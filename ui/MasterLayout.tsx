import { INavLink, INavLinkGroup, registerIcons } from "@fluentui/react";
import { ColumnLayout, Menu, TopNav } from "core";
import * as Icons from "heroicons-react";
import { useRouter } from "next/dist/client/router";
import React, { HTMLAttributes, useEffect } from "react";

registerIcons({
  icons: {
    "hero-HomeOutline": <Icons.HomeOutline className="h-full" />,
    "hero-TemplateOutline": <Icons.TemplateOutline className="h-full" />,
    "hero-ViewGridOutline": <Icons.ViewGridOutline className="h-full" />,
    "hero-ViewGridAddOutline": (
      <Icons.ViewGridAddOutline className="h-full" />
    ),
    "hero-DocumentTextOutline": (
      <Icons.DocumentTextOutline className="h-full" />
    ),
    "hero-DocumentDuplicateOutline": (
      <Icons.DocumentDuplicateOutline className="h-full" />
    ),
    "hero-PuzzleOutline": <Icons.PuzzleOutline className="h-full" />,
  },
});

export const MasterLayout = (props: HTMLAttributes<HTMLDivElement>) => {
  const router = useRouter();
  let navLinkGroups: INavLinkGroup[] = [
    {
      links: [
        {
          name: "Home",
          ariaLabel: "Welcome",
          url: "/",
          icon: "hero-HomeOutline",
        },
        {
          name: "Layout",
          url: "",
          icon: "hero-TemplateOutline",
          links: [
            {
              name: "Containers",
              url: "/docs/layout/containers",
            },
            {
              name: "Elements",
              url: "/docs/layout/elements",
            },
            {
              name: "Form",
              url: "/docs/layout/form",
            },
          ],
        },
        {
          name: "Base",
          url: "",
          icon: "hero-ViewGridAddOutline",
          links: [
            {
              name: "Navigations",
              url: "/docs/base/navigations",
            },
            {
              name: "Headers",
              url: "/docs/base/headers",
            },
            {
              name: "Content",
              url: "/docs/base/content",
            },
            {
              name: "Features",
              url: "/docs/base/features",
            },
            {
              name: "How it works",
              url: "/docs/base/how-it-works",
            },
            {
              name: "Testimonial",
              url: "/docs/base/testimonial",
            },
            {
              name: "Portofolio",
              url: "/docs/base/portofolio",
            },
            {
              name: "Team",
              url: "/docs/base/team",
            },
            {
              name: "Pricing",
              url: "/docs/base/pricing",
            },
            {
              name: "FAQ",
              url: "/docs/base/faq",
            },
            {
              name: "Contact",
              url: "/docs/base/contact",
            },
          ],
        },
        {
          name: "Pages",
          url: "",
          icon: "hero-DocumentTextOutline",
          links: [
            {
              name: "Sign in",
              url: "/docs/pages/sign-in",
            },
            {
              name: "Blog",
              url: "/docs/pages/blog",
            },
            {
              name: "Ecommerce",
              url: "/docs/pages/ecommerce",
            },
            {
              name: "Admin",
              url: "/docs/pages/admin",
            },
            {
              name: "HTTP Codes",
              url: "/docs/pages/http-code",
            },
          ],
        },
        {
          name: "Utils",
          url: "",
          icon: "hero-PuzzleOutline",
          links: [
            {
              name: "Cookies",
              url: "/docs/pages/cookies",
            },
            {
              name: "Call to action",
              url: "/docs/pages/http-code",
            },
            {
              name: "Gallery",
              url: "/docs/pages/gallery",
            },
            {
              name: "Content grid",
              url: "/docs/pages/content-grid",
            },
            {
              name: "Pagination",
              url: "/docs/pages/pagination",
            },
          ],
        },
        {
          name: "Sample",
          url: "",
          icon: "hero-DocumentDuplicateOutline",
          links: [
            {
              name: "Basic Form",
              url: "/docs/pages/http-code",
            },
            {
              name: "Centered Form",
              url: "/docs/pages/http-code",
            },
            {
              name: "Table",
              url: "/docs/pages/http-code",
            },
            {
              name: "Card",
              url: "/docs/pages/http-code",
            },
            {
              name: "Sidebar",
              url: "/docs/pages/http-code",
            },
          ],
        },
      ],
    },
  ];
  const rebuildNavLinkGroups = (nav: INavLink[]) => {
    let active = false;
    for (let x of nav) {
      if (!!x.url && !x.key) {
        x.key = x.url;
      }
      if (x.key === router.pathname) {
        active = true;
        break;
      }
      if (!!x.links?.length) {
        let active = rebuildNavLinkGroups(x.links);
        x.isExpanded = active;
        if (!!active) break;
      }
    }
    return active;
  };
  rebuildNavLinkGroups(navLinkGroups[0].links);
  const getTitleByPath = (nav: INavLink[]): string => {
    let title = "";
    for (let x of nav) {
      if (x.key == router.pathname) {
        title = x.ariaLabel || x.name;
        break;
      }
      if (!!x.links?.length) {
        title = getTitleByPath(x.links);
        if (!!title) break;
      }
    }
    return title;
  };
  let TitlePage = getTitleByPath(navLinkGroups[0].links);
  useEffect(() => {
    TitlePage = getTitleByPath(navLinkGroups[0].links);
  }, [router.pathname]);
  return (
    <ColumnLayout className="flex-row flex flex-1 h-screen overflow-hidden">
      <div
        className="flex flex-col bg-white border-r border-gray-300"
        style={{
          width: "260px",
        }}
      >
        <div className="flex items-center p-4 mb-4">
          <img
            className="h-8 w-auto rounded-lg overflow-hidden"
            src={"/images/kelava.png"}
            alt="Kelava"
          />
        </div>
        <div className="overflow-auto">
          <Menu
            groups={navLinkGroups}
            selectedKey={router.pathname}
            initialSelectedKey="/"
            onLinkClick={(e, item) => {
              if (!!item?.url) {
                router.push(item.url);
                e?.preventDefault();
              }
            }}
          />
        </div>
      </div>
      <div className={`flex flex-1 flex-col overflow-hidden`}>
        <TopNav>
          <div className="flex flex-1 text-gray-700 text-lg">{TitlePage}</div>
          {/* <div className="hidden md:flex items-center justify-end space-x-2 md:flex-1 lg:w-0">
            <span className="rounded-sm shadow-sm">
              <a
                href="#"
                className="whitespace-no-wrap h-full-flex items-center justify-center px-2 py-1 border border-transparent text-sm leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150"
              >
                Action
              </a>
            </span>
            <span className="rounded-sm shadow-sm s">
              <a
                href="#"
                className="whitespace-no-wrap h-full-flex items-center justify-center px-2 py-1 border border-transparent text-sm leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150"
              >
                Action 2
              </a>
            </span>
          </div> */}
        </TopNav>

        <div className={`flex flex-1 overflow-auto bg-gray-100`} {...props}>
          {props.children}
        </div>
      </div>
    </ColumnLayout>
  );
};
