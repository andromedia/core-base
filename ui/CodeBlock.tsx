import { ActionButton, registerIcons } from "@fluentui/react";
import { CardElement } from "core";
import * as Icons from "heroicons-react";
import { ReactChild, ReactNode } from "react";

export interface ICodeBlock {
  title?: string;
  clipboard: string;
  children: ReactNode;
}
registerIcons({
  icons: {
    "hero-Clipboard": <Icons.Clipboard className="w-full h-full" />,
  },
});

export const CodeBlock = (props: ICodeBlock) => {
  const copy = () => {
    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = props.clipboard;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
  };
  return (
    <CardElement className="flex flex-col bg-white rounded shadow-sm mb-6">
      <div className="px-6 py border-b border-gray-300 flex flex-row items-center">
        <div className="text-gray-800 text-sm flex flex-1">{props.title}</div>
        <div>
          <ActionButton
            iconProps={{
              iconName: "hero-Clipboard",
            }}
            onClick={copy}
          >
            Copy
          </ActionButton>
        </div>
      </div>
      <div className="px-6 py-3">{props.children}</div>
    </CardElement>
  );
};
