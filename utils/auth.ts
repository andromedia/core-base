import { initializeAuth } from "core/dist/src/server";

const dummyRole = { name: "public", features: ["use-app"] };
const dummyUser = {
  username: "dummy",
  roles: [dummyRole],
};

export const auth = initializeAuth({
  login: async (u, p) => {
    // return { success: false, msg: "Login Gagal" };
    return { success: true, msg: "Login Berhasil", user: dummyUser };
  },
  getAllRoles: async () => {
    return [dummyRole];
  },
});
