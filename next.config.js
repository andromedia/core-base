const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require("next/constants");

module.exports = (phase, { defaultConfig }) => {
  return {
    publicRuntimeConfig: {
      mode: phase === PHASE_DEVELOPMENT_SERVER ? "dev" : "prod",
    },
    devIndicators: {
      autoPrerender: false,
    },
    webpack: (config, { buildId, dev, isServer, defaultLoaders }) => {
      // XXX https://github.com/evanw/node-source-map-support/issues/155
      // config.node = {
      //   fs: "empty",
      //   module: "empty",
      // };
      // config.resolve.alias.string_decoder = "string_decoder";
      return config;
    },
  };
};
