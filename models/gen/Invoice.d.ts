import { Model, HasManyClass } from "core";
import { Customer } from "./Customer";
import { InvoiceLine } from "./InvoiceLine";
import { FindManyInvoiceArgs } from "@prisma/generated";
export declare class Invoice<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    InvoiceId: number;
    CustomerId: number;
    InvoiceDate: any;
    BillingAddress?: string;
    BillingCity?: string;
    BillingState?: string;
    BillingCountry?: string;
    BillingPostalCode?: string;
    Total: any;
    Customer: HasManyClass<Customer<Invoice>, Invoice>;
    InvoiceLine: HasManyClass<InvoiceLine<Invoice>, Invoice>;
    static findMany(q: FindManyInvoiceArgs, featureName?: string): Invoice[];
}
