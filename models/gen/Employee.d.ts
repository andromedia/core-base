import { Model, HasManyClass } from "core";
import { Employee } from "./Employee";
import { Customer } from "./Customer";
import { FindManyEmployeeArgs } from "@prisma/generated";
export declare class Employee<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    EmployeeId: number;
    LastName: string;
    FirstName: string;
    Title?: string;
    ReportsTo?: number;
    BirthDate?: any;
    HireDate?: any;
    Address?: string;
    City?: string;
    State?: string;
    Country?: string;
    PostalCode?: string;
    Phone?: string;
    Fax?: string;
    Email?: string;
    Employee: HasManyClass<Employee<Employee>, Employee>;
    Customer: HasManyClass<Customer<Employee>, Employee>;
    other_Employee: HasManyClass<Employee<Employee>, Employee>;
    static findMany(q: FindManyEmployeeArgs, featureName?: string): Employee[];
}
