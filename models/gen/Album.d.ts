import { Model, HasManyClass } from "core";
import { Artist } from "./Artist";
import { Track } from "./Track";
import { FindManyAlbumArgs } from "@prisma/generated";
export declare class Album<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    AlbumId: number;
    Title: string;
    ArtistId: number;
    Artist: HasManyClass<Artist<Album>, Album>;
    Track: HasManyClass<Track<Album>, Album>;
    static findMany(q: FindManyAlbumArgs, featureName?: string): Album[];
}
