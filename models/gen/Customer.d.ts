import { Model, HasManyClass } from "core";
import { Employee } from "./Employee";
import { Invoice } from "./Invoice";
import { FindManyCustomerArgs } from "@prisma/generated";
export declare class Customer<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    CustomerId: number;
    FirstName: string;
    LastName: string;
    Company?: string;
    Address?: string;
    City?: string;
    State?: string;
    Country?: string;
    PostalCode?: string;
    Phone?: string;
    Fax?: string;
    Email: string;
    SupportRepId?: number;
    Employee: HasManyClass<Employee<Customer>, Customer>;
    Invoice: HasManyClass<Invoice<Customer>, Customer>;
    static findMany(q: FindManyCustomerArgs, featureName?: string): Customer[];
}
