import { Model, HasManyClass } from "core";
import { Invoice } from "./Invoice";
import { Track } from "./Track";
import { FindManyInvoiceLineArgs } from "@prisma/generated";
export declare class InvoiceLine<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    InvoiceLineId: number;
    InvoiceId: number;
    TrackId: number;
    UnitPrice: any;
    Quantity: number;
    Invoice: HasManyClass<Invoice<InvoiceLine>, InvoiceLine>;
    Track: HasManyClass<Track<InvoiceLine>, InvoiceLine>;
    static findMany(q: FindManyInvoiceLineArgs, featureName?: string): InvoiceLine[];
}
