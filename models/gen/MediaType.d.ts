import { Model, HasManyClass } from "core";
import { Track } from "./Track";
import { FindManyMediaTypeArgs } from "@prisma/generated";
export declare class MediaType<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    MediaTypeId: number;
    Name?: string;
    Track: HasManyClass<Track<MediaType>, MediaType>;
    static findMany(q: FindManyMediaTypeArgs, featureName?: string): MediaType[];
}
