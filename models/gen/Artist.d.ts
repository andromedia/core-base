import { Model, HasManyClass } from "core";
import { Album } from "./Album";
import { FindManyArtistArgs } from "@prisma/generated";
export declare class Artist<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    ArtistId: number;
    Name?: string;
    Album: HasManyClass<Album<Artist>, Artist>;
    static findMany(q: FindManyArtistArgs, featureName?: string): Artist[];
}
