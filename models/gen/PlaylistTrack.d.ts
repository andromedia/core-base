import { Model, HasManyClass } from "core";
import { Playlist } from "./Playlist";
import { Track } from "./Track";
import { FindManyPlaylistTrackArgs } from "@prisma/generated";
export declare class PlaylistTrack<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    PlaylistId: number;
    TrackId: number;
    Playlist: HasManyClass<Playlist<PlaylistTrack>, PlaylistTrack>;
    Track: HasManyClass<Track<PlaylistTrack>, PlaylistTrack>;
    static findMany(q: FindManyPlaylistTrackArgs, featureName?: string): PlaylistTrack[];
}
