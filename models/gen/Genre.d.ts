import { Model, HasManyClass } from "core";
import { Track } from "./Track";
import { FindManyGenreArgs } from "@prisma/generated";
export declare class Genre<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    GenreId: number;
    Name?: string;
    Track: HasManyClass<Track<Genre>, Genre>;
    static findMany(q: FindManyGenreArgs, featureName?: string): Genre[];
}
