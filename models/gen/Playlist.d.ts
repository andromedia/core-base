import { Model, HasManyClass } from "core";
import { PlaylistTrack } from "./PlaylistTrack";
import { FindManyPlaylistArgs } from "@prisma/generated";
export declare class Playlist<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    PlaylistId: number;
    Name?: string;
    PlaylistTrack: HasManyClass<PlaylistTrack<Playlist>, Playlist>;
    static findMany(q: FindManyPlaylistArgs, featureName?: string): Playlist[];
}
