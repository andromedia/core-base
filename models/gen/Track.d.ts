import { Model, HasManyClass } from "core";
import { Album } from "./Album";
import { Genre } from "./Genre";
import { MediaType } from "./MediaType";
import { InvoiceLine } from "./InvoiceLine";
import { PlaylistTrack } from "./PlaylistTrack";
import { FindManyTrackArgs } from "@prisma/generated";
export declare class Track<T extends Model = any> extends Model<T> {
    private static _primaryKey;
    private static _modelName;
    private static _tableName;
    TrackId: number;
    Name: string;
    AlbumId?: number;
    MediaTypeId: number;
    GenreId?: number;
    Composer?: string;
    Milliseconds: number;
    Bytes?: number;
    UnitPrice: any;
    Album: HasManyClass<Album<Track>, Track>;
    Genre: HasManyClass<Genre<Track>, Track>;
    MediaType: HasManyClass<MediaType<Track>, Track>;
    InvoiceLine: HasManyClass<InvoiceLine<Track>, Track>;
    PlaylistTrack: HasManyClass<PlaylistTrack<Track>, Track>;
    static findMany(q: FindManyTrackArgs, featureName?: string): Track[];
}
