import { CardElement } from "core";
import { Album } from "models/gen/Album";
import { useRouter } from "next/dist/client/router";
import { useEffect } from "react";
import { MasterLayout } from "ui/MasterLayout";

const Page = () => {
  const router = useRouter();
  useEffect(() => {
    (async () => {
      const qb = await Album.findMany({
        select: {
          Artist: true,
        },
        take: 10,
      });
      console.log(qb);
    })(); 
  }, []);

  return (
    <MasterLayout>
      <div className="flex flex-col flex-1 overflow-auto p-4">
        <CardElement>
          <section className="py-12 px-8">
            <div className="flex flex-wrap items-center text-center lg:text-left -mx-2">
              <div className="lg:w-1/2 px-2 lg:pr-10 mt-10 lg:mt-0 order-1 lg:order-none">
                <span className="text-sm font-semibold">PLANSYS 3.0</span>
                <h2 className="text-5xl mt-4 mb-6 leading-tight font-heading">
                  The React Framework for Production
                </h2>
                <a
                  className="text-lg text-blue-700 hover:underline"
                  onClick={() => router.push("/docs/layout/containers")}
                >
                  Learn more »
                </a>
              </div>
              <div className="lg:w-1/2 px-2">
                <img
                  src="https://tailwind.build/placeholders/pictures/certificate.svg"
                  alt=""
                />
              </div>
            </div>
          </section>
        </CardElement>
      </div>
    </MasterLayout>
  );
};

export default Page;
