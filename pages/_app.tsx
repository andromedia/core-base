import type { AppProps } from "next/app";
import "public/styles/globals.css";
import "public/styles/tailwind.css";
import dynamic from "next/dynamic";
import React from "react";

const App = (props: AppProps) => {
  const { Component, pageProps } = props;

  // Put your master layout here...
  const FinalComponent = (props: any) => (
    <>
      <Component {...props} />
    </>
  );

  // Force nextjs to render client-side only when not in SSG/SSP
  if (props.__N_SSG || props.__N_SSP) {
    return <FinalComponent {...pageProps} />;
  }
  const ClientOnlyComponent = dynamic(() => Promise.resolve(FinalComponent), {
    ssr: false,
  });
  return <ClientOnlyComponent {...pageProps} />;
};

export default App;
