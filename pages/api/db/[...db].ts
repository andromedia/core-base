import { CoreDb } from "core/dist/src/server";
import { auth } from "utils/auth";
import nextConfig from "next/config";

export default (req: any, res: any) =>
  CoreDb(req, res, {
    auth,
    config: nextConfig(),
  });
