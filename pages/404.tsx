import { useRouter } from "next/router";

const ErrorPage = () => {
  const router = useRouter();
  return (
    <div className="absolute inset-0 flex flex-col justify-center items-center">
      <h1 className="text-2xl">Error - 404</h1>
      <div>Page Not Found</div>
    </div>
  );
};
export default ErrorPage;
