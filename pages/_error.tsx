import { useRouter } from "next/router";

const ErrorPage = () => {
  const router = useRouter();
  return (
    <div className="absolute inset-0 flex flex-col justify-center items-center">
      <h1 className="text-2xl">{router.query.title ?? "Error"}</h1>
      <div>{router.query.msg}</div>
    </div>
  );
};
export default ErrorPage;
