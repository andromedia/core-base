import { CodeBlock } from "ui/CodeBlock";
import { MasterLayout } from "ui/MasterLayout";

const Page = () => {
  return (
    <MasterLayout>
      <div className="flex flex-col flex-1 overflow-auto p-4">
        <CodeBlock
          title="Button"
          clipboard={`
          <button
            className="inline-block py-4 px-8 leading-none text-white bg-indigo-500 hover:bg-indigo-600 rounded shadow"
            type="submit"
          >
            Submit
          </button>
        `}
        >
          <div className="mb-4">
            <button
              className="inline-block py-2 px-3 leading-none text-xs text-white bg-indigo-500 hover:bg-indigo-600 rounded shadow"
              type="submit"
            >
              Submit
            </button>
            <button
              className="inline-block py-3 px-4 leading-none text-sm text-white bg-indigo-500 hover:bg-indigo-600 rounded shadow"
              type="submit"
            >
              Submit
            </button>
            <button
              className="inline-block py-4 px-8 leading-none text-white bg-indigo-500 hover:bg-indigo-600 rounded shadow"
              type="submit"
            >
              Submit
            </button>
          </div>
          <div className="">
            <button
              className="inline-block py-2 px-3 leading-none text-xs text-white bg-indigo-500 hover:bg-indigo-600 rounded shadow"
              type="submit"
            >
              Cancel
            </button>
            <button
              className="inline-block py-2 px-3 leading-none text-xs text-white bg-red-500 hover:bg-red-600 rounded shadow"
              type="submit"
            >
              Cancel
            </button>
            <button
              className="inline-block py-2 px-3 leading-none text-xs text-white bg-green-500 hover:bg-green-600 rounded shadow"
              type="submit"
            >
              Cancel
            </button>
            <button
              className="inline-block py-2 px-3 leading-none text-xs text-gray-700 bg-gray-300 hover:bg-gray-400 rounded shadow"
              type="submit"
            >
              Cancel
            </button>
          </div>
        </CodeBlock>
      </div>
    </MasterLayout>
  );
};

export default Page;
