import { ColumnLayout } from "core";
import { CodeBlock } from "ui/CodeBlock";
import { MasterLayout } from "ui/MasterLayout";

// ASK
// 1. Merge CSS Tailwind
// 2. ReactNode to string for copy to clipboard

const Page = () => {
  return (
    <MasterLayout>
      <div className="flex flex-col flex-1 overflow-auto p-4">
        <CodeBlock
          title="Two Column"
          clipboard={`
          <ColumnLayout>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        `}
        >
          <ColumnLayout>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        </CodeBlock>
        <CodeBlock
          title="Two Column Left Sidebar"
          clipboard={`
          <ColumnLayout>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-2/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        `}
        >
          <ColumnLayout>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-2/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        </CodeBlock>
        <CodeBlock
          title="Two Column Right Sidebar"
          clipboard={`
          <ColumnLayout>
            <div className="w-full md:w-2/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        `}
        >
          <ColumnLayout>
            <div className="w-full md:w-2/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        </CodeBlock>
        <CodeBlock
          title="Three Column"
          clipboard={`
          <ColumnLayout>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        `}
        >
          <ColumnLayout>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        </CodeBlock>

        <CodeBlock
          title="Four Column and wrap"
          clipboard={`
          <ColumnLayout>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        `}
        >
          <ColumnLayout>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
            <div className="w-full md:w-1/2 px-4 mb-4 md:mb-0">
              <div className="bg-gray-100 border border-dashed border-gray-400 rounded p-6 text-gray-500 text-center">
                Container
              </div>
            </div>
          </ColumnLayout>
        </CodeBlock>
      </div>
    </MasterLayout>
  );
};

export default Page;
