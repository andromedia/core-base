import { CodeBlock } from "ui/CodeBlock";
import { MasterLayout } from "ui/MasterLayout";
import { cx } from "core";

const Page = () => {
  return (
    <MasterLayout>
      <div className={cx(`flex flex-col flex-1  overflow-auto p-4`)}>
        <CodeBlock
          title="Heading"
          clipboard={`
          <h1 className="text-5xl mb-2 leading-tight font-heading">
            h1. Lorem, ipsum dolor.
          </h1>
          <h2 className="text-4xl mb-2 leading-tight font-heading">
            h2. Lorem, ipsum dolor.
          </h2>
          <h3 className="text-3xl mb-2 leading-tight font-heading">
            h3. Lorem, ipsum dolor.
          </h3>
          <h4 className="text-2xl mb-2 leading-tight font-heading">
            h4. Lorem, ipsum dolor.
          </h4>
          <h5 className="text-lg mb-2 leading-tight font-heading">
            h5. Lorem, ipsum dolor.
          </h5>
          <h6 className="mb-2 leading-tight font-heading">
            h6. Lorem, ipsum dolor.
          </h6>
        `}
        >
          <h1 className="text-5xl mb-2 leading-tight font-heading">
            h1. Lorem, ipsum dolor.
          </h1>
          <h2 className="text-4xl mb-2 leading-tight font-heading">
            h2. Lorem, ipsum dolor.
          </h2>
          <h3 className="text-3xl mb-2 leading-tight font-heading">
            h3. Lorem, ipsum dolor.
          </h3>
          <h4 className="text-2xl mb-2 leading-tight font-heading">
            h4. Lorem, ipsum dolor.
          </h4>
          <h5 className="text-lg mb-2 leading-tight font-heading">
            h5. Lorem, ipsum dolor.
          </h5>
          <h6 className="mb-2 leading-tight font-heading">
            h6. Lorem, ipsum dolor.
          </h6>
        </CodeBlock>
        <CodeBlock
          title="Paragraph"
          clipboard={`
            <p className="mb-2 text-lg text-gray-500 leading-snug">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea sit
              eaque totam aliquid veritatis assumenda temporibus harum unde!
            </p>
            <p className="mb-2 leading-snug">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea sit
              eaque totam aliquid veritatis assumenda temporibus harum unde!
            </p>
        `}
        >
          <p className="mb-2 text-lg text-gray-500 leading-snug">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea sit
            eaque totam aliquid veritatis assumenda temporibus harum unde!
          </p>
          <p className="mb-2 leading-snug">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea sit
            eaque totam aliquid veritatis assumenda temporibus harum unde!
          </p>
        </CodeBlock>
        <CodeBlock
          title="Link"
          clipboard={`
          <a
            className="inline-block py-3 px-6 leading-none text-white bg-indigo-500 hover:bg-indigo-600 rounded shadow"
            href="#"
          >
            Primary button
          </a>
          <a className="text-blue-700 hover:underline" href="#">
            Simple link
          </a>
        `}
        >
          <a className="text-blue-700 hover:underline m-1" href="#">
            Simple link
          </a>
          <a
            className="inline-block py-3 px-6 leading-none text-white bg-indigo-500 hover:bg-indigo-600 rounded shadow m-1 ml-4"
            href="#"
          >
            Primary button
          </a>
        </CodeBlock>
        <CodeBlock
          title="Image"
          clipboard={`
          <img
            src="https://tailwind.build/placeholders/pictures/new_ideas.svg"
            alt=""
          />
        `}
        >
          <img
            className="h-64 w-64"
            src="https://tailwind.build/placeholders/pictures/new_ideas.svg"
            alt=""
          />
        </CodeBlock>
      </div>
    </MasterLayout>
  );
};

export default Page;
