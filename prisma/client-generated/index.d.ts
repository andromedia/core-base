import {
  DMMF,
  DMMFClass,
  Engine,
  PrismaClientKnownRequestError,
  PrismaClientUnknownRequestError,
  PrismaClientRustPanicError,
  PrismaClientInitializationError,
  PrismaClientValidationError,
  sqltag as sql,
  empty,
  join,
  raw,
  Sql,
  Decimal,
} from './runtime';

export { PrismaClientKnownRequestError }
export { PrismaClientUnknownRequestError }
export { PrismaClientRustPanicError }
export { PrismaClientInitializationError }
export { PrismaClientValidationError }
export { Decimal }

/**
 * Re-export of sql-template-tag
 */
export { sql, empty, join, raw, Sql }

/**
 * Prisma Client JS version: 2.10.1
 * Query Engine version: 7d0087eadc7265e12d4b8d8c3516b02c4c965111
 */
export declare type PrismaVersion = {
  client: string
}

export declare const prismaVersion: PrismaVersion 

/**
 * Utility Types
 */

/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON object.
 * This type can be useful to enforce some input to be JSON-compatible or as a super-type to be extended from. 
 */
export declare type JsonObject = {[Key in string]?: JsonValue}
 
/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON array.
 */
export declare interface JsonArray extends Array<JsonValue> {}
 
/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches any valid JSON value.
 */
export declare type JsonValue = string | number | boolean | null | JsonObject | JsonArray

/**
 * Same as JsonObject, but allows undefined
 */
export declare type InputJsonObject = {[Key in string]?: JsonValue}
 
export declare interface InputJsonArray extends Array<JsonValue> {}
 
export declare type InputJsonValue = undefined |  string | number | boolean | null | InputJsonObject | InputJsonArray

declare type SelectAndInclude = {
  select: any
  include: any
}

declare type HasSelect = {
  select: any
}

declare type HasInclude = {
  include: any
}

declare type CheckSelect<T, S, U> = T extends SelectAndInclude
  ? 'Please either choose `select` or `include`'
  : T extends HasSelect
  ? U
  : T extends HasInclude
  ? U
  : S

/**
 * Get the type of the value, that the Promise holds.
 */
export declare type PromiseType<T extends PromiseLike<any>> = T extends PromiseLike<infer U> ? U : T;

/**
 * Get the return type of a function which returns a Promise.
 */
export declare type PromiseReturnType<T extends (...args: any) => Promise<any>> = PromiseType<ReturnType<T>>


export declare type Enumerable<T> = T | Array<T>;

export type RequiredKeys<T> = {
  [K in keyof T]-?: {} extends Pick<T, K> ? never : K
}[keyof T]

export declare type TruthyKeys<T> = {
  [key in keyof T]: T[key] extends false | undefined | null ? never : key
}[keyof T]

export declare type TrueKeys<T> = TruthyKeys<Pick<T, RequiredKeys<T>>>

/**
 * Subset
 * @desc From `T` pick properties that exist in `U`. Simple version of Intersection
 */
export declare type Subset<T, U> = {
  [key in keyof T]: key extends keyof U ? T[key] : never;
};
declare class PrismaClientFetcher {
  private readonly prisma;
  private readonly debug;
  private readonly hooks?;
  constructor(prisma: PrismaClient<any, any>, debug?: boolean, hooks?: Hooks | undefined);
  request<T>(document: any, dataPath?: string[], rootField?: string, typeName?: string, isList?: boolean, callsite?: string): Promise<T>;
  sanitizeMessage(message: string): string;
  protected unpack(document: any, data: any, path: string[], rootField?: string, isList?: boolean): any;
}


/**
 * Client
**/

export declare type Datasource = {
  url?: string
}

export type Datasources = {
  db?: Datasource
}

export type ErrorFormat = 'pretty' | 'colorless' | 'minimal'

export interface PrismaClientOptions {
  /**
   * Overwrites the datasource url from your prisma.schema file
   */
  datasources?: Datasources

  /**
   * @default "colorless"
   */
  errorFormat?: ErrorFormat

  /**
   * @example
   * ```
   * // Defaults to stdout
   * log: ['query', 'info', 'warn', 'error']
   * 
   * // Emit as events
   * log: [
   *  { emit: 'stdout', level: 'query' },
   *  { emit: 'stdout', level: 'info' },
   *  { emit: 'stdout', level: 'warn' }
   *  { emit: 'stdout', level: 'error' }
   * ]
   * ```
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/logging#the-log-option).
   */
  log?: Array<LogLevel | LogDefinition>
}

export type Hooks = {
  beforeRequest?: (options: {query: string, path: string[], rootField?: string, typeName?: string, document: any}) => any
}

/* Types for Logging */
export type LogLevel = 'info' | 'query' | 'warn' | 'error'
export type LogDefinition = {
  level: LogLevel
  emit: 'stdout' | 'event'
}

export type GetLogType<T extends LogLevel | LogDefinition> = T extends LogDefinition ? T['emit'] extends 'event' ? T['level'] : never : never
export type GetEvents<T extends any> = T extends Array<LogLevel | LogDefinition> ?
  GetLogType<T[0]> | GetLogType<T[1]> | GetLogType<T[2]> | GetLogType<T[3]>
  : never

export type QueryEvent = {
  timestamp: Date
  query: string
  params: string
  duration: number
  target: string
}

export type LogEvent = {
  timestamp: Date
  message: string
  target: string
}
/* End Types for Logging */


export type PrismaAction =
  | 'findOne'
  | 'findMany'
  | 'findFirst'
  | 'create'
  | 'update'
  | 'updateMany'
  | 'upsert'
  | 'delete'
  | 'deleteMany'
  | 'executeRaw'
  | 'queryRaw'
  | 'aggregate'

/**
 * These options are being passed in to the middleware as "params"
 */
export type MiddlewareParams = {
  model?: string
  action: PrismaAction
  args: any
  dataPath: string[]
  runInTransaction: boolean
}

/**
 * The `T` type makes sure, that the `return proceed` is not forgotten in the middleware implementation
 */
export type Middleware<T = any> = (
  params: MiddlewareParams,
  next: (params: MiddlewareParams) => Promise<T>,
) => Promise<T>

// tested in getLogLevel.test.ts
export declare function getLogLevel(log: Array<LogLevel | LogDefinition>): LogLevel | undefined;

/**
 * ##  Prisma Client ʲˢ
 * 
 * Type-safe database client for TypeScript & Node.js (ORM replacement)
 * @example
 * ```
 * const prisma = new PrismaClient()
 * // Fetch zero or more Albums
 * const albums = await prisma.album.findMany()
 * ```
 *
 * 
 * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
 */
export declare class PrismaClient<
  T extends PrismaClientOptions = PrismaClientOptions,
  U = 'log' extends keyof T ? T['log'] extends Array<LogLevel | LogDefinition> ? GetEvents<T['log']> : never : never
> {
  /**
   * @private
   */
  private fetcher;
  /**
   * @private
   */
  private readonly dmmf;
  /**
   * @private
   */
  private connectionPromise?;
  /**
   * @private
   */
  private disconnectionPromise?;
  /**
   * @private
   */
  private readonly engineConfig;
  /**
   * @private
   */
  private readonly measurePerformance;
  /**
   * @private
   */
  private engine: Engine;
  /**
   * @private
   */
  private errorFormat: ErrorFormat;

  /**
   * ##  Prisma Client ʲˢ
   * 
   * Type-safe database client for TypeScript & Node.js (ORM replacement)
   * @example
   * ```
   * const prisma = new PrismaClient()
   * // Fetch zero or more Albums
   * const albums = await prisma.album.findMany()
   * ```
   *
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
   */
  constructor(optionsArg?: T);
  $on<V extends U>(eventType: V, callback: (event: V extends 'query' ? QueryEvent : LogEvent) => void): void;
  /**
   * @deprecated renamed to `$on`
   */
  on<V extends U>(eventType: V, callback: (event: V extends 'query' ? QueryEvent : LogEvent) => void): void;
  /**
   * Connect with the database
   */
  $connect(): Promise<void>;
  /**
   * @deprecated renamed to `$connect`
   */
  connect(): Promise<void>;

  /**
   * Disconnect from the database
   */
  $disconnect(): Promise<any>;
  /**
   * @deprecated renamed to `$disconnect`
   */
  disconnect(): Promise<any>;

  /**
   * Add a middleware
   */
  $use(cb: Middleware): void

  /**
   * Executes a raw query and returns the number of affected rows
   * @example
   * ```
   * // With parameters use prisma.executeRaw``, values will be escaped automatically
   * const result = await prisma.executeRaw`UPDATE User SET cool = ${true} WHERE id = ${1};`
   * // Or
   * const result = await prisma.executeRaw('UPDATE User SET cool = $1 WHERE id = $2 ;', true, 1)
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $executeRaw<T = any>(query: string | TemplateStringsArray | Sql, ...values: any[]): Promise<number>;

  /**
   * @deprecated renamed to `$executeRaw`
   */
  executeRaw<T = any>(query: string | TemplateStringsArray | Sql, ...values: any[]): Promise<number>;

  /**
   * Performs a raw query and returns the SELECT data
   * @example
   * ```
   * // With parameters use prisma.queryRaw``, values will be escaped automatically
   * const result = await prisma.queryRaw`SELECT * FROM User WHERE id = ${1} OR email = ${'ema.il'};`
   * // Or
   * const result = await prisma.queryRaw('SELECT * FROM User WHERE id = $1 OR email = $2;', 1, 'ema.il')
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $queryRaw<T = any>(query: string | TemplateStringsArray | Sql, ...values: any[]): Promise<T>;
 
  /**
   * @deprecated renamed to `$queryRaw`
   */
  queryRaw<T = any>(query: string | TemplateStringsArray | Sql, ...values: any[]): Promise<T>;

  /**
   * `prisma.album`: Exposes CRUD operations for the **Album** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Albums
    * const albums = await prisma.album.findMany()
    * ```
    */
  get album(): AlbumDelegate;

  /**
   * `prisma.artist`: Exposes CRUD operations for the **Artist** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Artists
    * const artists = await prisma.artist.findMany()
    * ```
    */
  get artist(): ArtistDelegate;

  /**
   * `prisma.customer`: Exposes CRUD operations for the **Customer** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Customers
    * const customers = await prisma.customer.findMany()
    * ```
    */
  get customer(): CustomerDelegate;

  /**
   * `prisma.employee`: Exposes CRUD operations for the **Employee** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Employees
    * const employees = await prisma.employee.findMany()
    * ```
    */
  get employee(): EmployeeDelegate;

  /**
   * `prisma.genre`: Exposes CRUD operations for the **Genre** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Genres
    * const genres = await prisma.genre.findMany()
    * ```
    */
  get genre(): GenreDelegate;

  /**
   * `prisma.invoice`: Exposes CRUD operations for the **Invoice** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Invoices
    * const invoices = await prisma.invoice.findMany()
    * ```
    */
  get invoice(): InvoiceDelegate;

  /**
   * `prisma.invoiceLine`: Exposes CRUD operations for the **InvoiceLine** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more InvoiceLines
    * const invoiceLines = await prisma.invoiceLine.findMany()
    * ```
    */
  get invoiceLine(): InvoiceLineDelegate;

  /**
   * `prisma.mediaType`: Exposes CRUD operations for the **MediaType** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more MediaTypes
    * const mediaTypes = await prisma.mediaType.findMany()
    * ```
    */
  get mediaType(): MediaTypeDelegate;

  /**
   * `prisma.playlist`: Exposes CRUD operations for the **Playlist** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Playlists
    * const playlists = await prisma.playlist.findMany()
    * ```
    */
  get playlist(): PlaylistDelegate;

  /**
   * `prisma.playlistTrack`: Exposes CRUD operations for the **PlaylistTrack** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more PlaylistTracks
    * const playlistTracks = await prisma.playlistTrack.findMany()
    * ```
    */
  get playlistTrack(): PlaylistTrackDelegate;

  /**
   * `prisma.track`: Exposes CRUD operations for the **Track** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Tracks
    * const tracks = await prisma.track.findMany()
    * ```
    */
  get track(): TrackDelegate;
}



/**
 * Enums
 */

// Based on
// https://github.com/microsoft/TypeScript/issues/3192#issuecomment-261720275

export declare const AlbumDistinctFieldEnum: {
  AlbumId: 'AlbumId',
  Title: 'Title',
  ArtistId: 'ArtistId'
};

export declare type AlbumDistinctFieldEnum = (typeof AlbumDistinctFieldEnum)[keyof typeof AlbumDistinctFieldEnum]


export declare const ArtistDistinctFieldEnum: {
  ArtistId: 'ArtistId',
  Name: 'Name'
};

export declare type ArtistDistinctFieldEnum = (typeof ArtistDistinctFieldEnum)[keyof typeof ArtistDistinctFieldEnum]


export declare const CustomerDistinctFieldEnum: {
  CustomerId: 'CustomerId',
  FirstName: 'FirstName',
  LastName: 'LastName',
  Company: 'Company',
  Address: 'Address',
  City: 'City',
  State: 'State',
  Country: 'Country',
  PostalCode: 'PostalCode',
  Phone: 'Phone',
  Fax: 'Fax',
  Email: 'Email',
  SupportRepId: 'SupportRepId'
};

export declare type CustomerDistinctFieldEnum = (typeof CustomerDistinctFieldEnum)[keyof typeof CustomerDistinctFieldEnum]


export declare const EmployeeDistinctFieldEnum: {
  EmployeeId: 'EmployeeId',
  LastName: 'LastName',
  FirstName: 'FirstName',
  Title: 'Title',
  ReportsTo: 'ReportsTo',
  BirthDate: 'BirthDate',
  HireDate: 'HireDate',
  Address: 'Address',
  City: 'City',
  State: 'State',
  Country: 'Country',
  PostalCode: 'PostalCode',
  Phone: 'Phone',
  Fax: 'Fax',
  Email: 'Email'
};

export declare type EmployeeDistinctFieldEnum = (typeof EmployeeDistinctFieldEnum)[keyof typeof EmployeeDistinctFieldEnum]


export declare const GenreDistinctFieldEnum: {
  GenreId: 'GenreId',
  Name: 'Name'
};

export declare type GenreDistinctFieldEnum = (typeof GenreDistinctFieldEnum)[keyof typeof GenreDistinctFieldEnum]


export declare const InvoiceDistinctFieldEnum: {
  InvoiceId: 'InvoiceId',
  CustomerId: 'CustomerId',
  InvoiceDate: 'InvoiceDate',
  BillingAddress: 'BillingAddress',
  BillingCity: 'BillingCity',
  BillingState: 'BillingState',
  BillingCountry: 'BillingCountry',
  BillingPostalCode: 'BillingPostalCode',
  Total: 'Total'
};

export declare type InvoiceDistinctFieldEnum = (typeof InvoiceDistinctFieldEnum)[keyof typeof InvoiceDistinctFieldEnum]


export declare const InvoiceLineDistinctFieldEnum: {
  InvoiceLineId: 'InvoiceLineId',
  InvoiceId: 'InvoiceId',
  TrackId: 'TrackId',
  UnitPrice: 'UnitPrice',
  Quantity: 'Quantity'
};

export declare type InvoiceLineDistinctFieldEnum = (typeof InvoiceLineDistinctFieldEnum)[keyof typeof InvoiceLineDistinctFieldEnum]


export declare const MediaTypeDistinctFieldEnum: {
  MediaTypeId: 'MediaTypeId',
  Name: 'Name'
};

export declare type MediaTypeDistinctFieldEnum = (typeof MediaTypeDistinctFieldEnum)[keyof typeof MediaTypeDistinctFieldEnum]


export declare const PlaylistDistinctFieldEnum: {
  PlaylistId: 'PlaylistId',
  Name: 'Name'
};

export declare type PlaylistDistinctFieldEnum = (typeof PlaylistDistinctFieldEnum)[keyof typeof PlaylistDistinctFieldEnum]


export declare const PlaylistTrackDistinctFieldEnum: {
  PlaylistId: 'PlaylistId',
  TrackId: 'TrackId'
};

export declare type PlaylistTrackDistinctFieldEnum = (typeof PlaylistTrackDistinctFieldEnum)[keyof typeof PlaylistTrackDistinctFieldEnum]


export declare const TrackDistinctFieldEnum: {
  TrackId: 'TrackId',
  Name: 'Name',
  AlbumId: 'AlbumId',
  MediaTypeId: 'MediaTypeId',
  GenreId: 'GenreId',
  Composer: 'Composer',
  Milliseconds: 'Milliseconds',
  Bytes: 'Bytes',
  UnitPrice: 'UnitPrice'
};

export declare type TrackDistinctFieldEnum = (typeof TrackDistinctFieldEnum)[keyof typeof TrackDistinctFieldEnum]


export declare const SortOrder: {
  asc: 'asc',
  desc: 'desc'
};

export declare type SortOrder = (typeof SortOrder)[keyof typeof SortOrder]



/**
 * Model Album
 */

export type Album = {
  AlbumId: number
  Title: string
  ArtistId: number
}


export type AggregateAlbum = {
  count: number
  avg: AlbumAvgAggregateOutputType | null
  sum: AlbumSumAggregateOutputType | null
  min: AlbumMinAggregateOutputType | null
  max: AlbumMaxAggregateOutputType | null
}

export type AlbumAvgAggregateOutputType = {
  AlbumId: number
  ArtistId: number
}

export type AlbumSumAggregateOutputType = {
  AlbumId: number
  ArtistId: number
}

export type AlbumMinAggregateOutputType = {
  AlbumId: number
  ArtistId: number
}

export type AlbumMaxAggregateOutputType = {
  AlbumId: number
  ArtistId: number
}


export type AlbumAvgAggregateInputType = {
  AlbumId?: true
  ArtistId?: true
}

export type AlbumSumAggregateInputType = {
  AlbumId?: true
  ArtistId?: true
}

export type AlbumMinAggregateInputType = {
  AlbumId?: true
  ArtistId?: true
}

export type AlbumMaxAggregateInputType = {
  AlbumId?: true
  ArtistId?: true
}

export type AggregateAlbumArgs = {
  where?: AlbumWhereInput
  orderBy?: Enumerable<AlbumOrderByInput> | AlbumOrderByInput
  cursor?: AlbumWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<AlbumDistinctFieldEnum>
  count?: true
  avg?: AlbumAvgAggregateInputType
  sum?: AlbumSumAggregateInputType
  min?: AlbumMinAggregateInputType
  max?: AlbumMaxAggregateInputType
}

export type GetAlbumAggregateType<T extends AggregateAlbumArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetAlbumAggregateScalarType<T[P]>
}

export type GetAlbumAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof AlbumAvgAggregateOutputType ? AlbumAvgAggregateOutputType[P] : never
}
    
    

export type AlbumSelect = {
  AlbumId?: boolean
  Title?: boolean
  ArtistId?: boolean
  Artist?: boolean | ArtistArgs
  Track?: boolean | FindManyTrackArgs
}

export type AlbumInclude = {
  Artist?: boolean | ArtistArgs
  Track?: boolean | FindManyTrackArgs
}

export type AlbumGetPayload<
  S extends boolean | null | undefined | AlbumArgs,
  U = keyof S
> = S extends true
  ? Album
  : S extends undefined
  ? never
  : S extends AlbumArgs | FindManyAlbumArgs
  ? 'include' extends U
    ? Album  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Artist'
      ? ArtistGetPayload<S['include'][P]> :
      P extends 'Track'
      ? Array<TrackGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Album ? Album[P]
: 
      P extends 'Artist'
      ? ArtistGetPayload<S['select'][P]> :
      P extends 'Track'
      ? Array<TrackGetPayload<S['select'][P]>> : never
    }
  : Album
: Album


export interface AlbumDelegate {
  /**
   * Find zero or one Album that matches the filter.
   * @param {FindOneAlbumArgs} args - Arguments to find a Album
   * @example
   * // Get one Album
   * const album = await prisma.album.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneAlbumArgs>(
    args: Subset<T, FindOneAlbumArgs>
  ): CheckSelect<T, Prisma__AlbumClient<Album | null>, Prisma__AlbumClient<AlbumGetPayload<T> | null>>
  /**
   * Find the first Album that matches the filter.
   * @param {FindFirstAlbumArgs} args - Arguments to find a Album
   * @example
   * // Get one Album
   * const album = await prisma.album.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstAlbumArgs>(
    args?: Subset<T, FindFirstAlbumArgs>
  ): CheckSelect<T, Prisma__AlbumClient<Album | null>, Prisma__AlbumClient<AlbumGetPayload<T> | null>>
  /**
   * Find zero or more Albums that matches the filter.
   * @param {FindManyAlbumArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Albums
   * const albums = await prisma.album.findMany()
   * 
   * // Get first 10 Albums
   * const albums = await prisma.album.findMany({ take: 10 })
   * 
   * // Only select the `AlbumId`
   * const albumWithAlbumIdOnly = await prisma.album.findMany({ select: { AlbumId: true } })
   * 
  **/
  findMany<T extends FindManyAlbumArgs>(
    args?: Subset<T, FindManyAlbumArgs>
  ): CheckSelect<T, Promise<Array<Album>>, Promise<Array<AlbumGetPayload<T>>>>
  /**
   * Create a Album.
   * @param {AlbumCreateArgs} args - Arguments to create a Album.
   * @example
   * // Create one Album
   * const Album = await prisma.album.create({
   *   data: {
   *     // ... data to create a Album
   *   }
   * })
   * 
  **/
  create<T extends AlbumCreateArgs>(
    args: Subset<T, AlbumCreateArgs>
  ): CheckSelect<T, Prisma__AlbumClient<Album>, Prisma__AlbumClient<AlbumGetPayload<T>>>
  /**
   * Delete a Album.
   * @param {AlbumDeleteArgs} args - Arguments to delete one Album.
   * @example
   * // Delete one Album
   * const Album = await prisma.album.delete({
   *   where: {
   *     // ... filter to delete one Album
   *   }
   * })
   * 
  **/
  delete<T extends AlbumDeleteArgs>(
    args: Subset<T, AlbumDeleteArgs>
  ): CheckSelect<T, Prisma__AlbumClient<Album>, Prisma__AlbumClient<AlbumGetPayload<T>>>
  /**
   * Update one Album.
   * @param {AlbumUpdateArgs} args - Arguments to update one Album.
   * @example
   * // Update one Album
   * const album = await prisma.album.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends AlbumUpdateArgs>(
    args: Subset<T, AlbumUpdateArgs>
  ): CheckSelect<T, Prisma__AlbumClient<Album>, Prisma__AlbumClient<AlbumGetPayload<T>>>
  /**
   * Delete zero or more Albums.
   * @param {AlbumDeleteManyArgs} args - Arguments to filter Albums to delete.
   * @example
   * // Delete a few Albums
   * const { count } = await prisma.album.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends AlbumDeleteManyArgs>(
    args: Subset<T, AlbumDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Albums.
   * @param {AlbumUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Albums
   * const album = await prisma.album.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends AlbumUpdateManyArgs>(
    args: Subset<T, AlbumUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Album.
   * @param {AlbumUpsertArgs} args - Arguments to update or create a Album.
   * @example
   * // Update or create a Album
   * const album = await prisma.album.upsert({
   *   create: {
   *     // ... data to create a Album
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Album we want to update
   *   }
   * })
  **/
  upsert<T extends AlbumUpsertArgs>(
    args: Subset<T, AlbumUpsertArgs>
  ): CheckSelect<T, Prisma__AlbumClient<Album>, Prisma__AlbumClient<AlbumGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyAlbumArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateAlbumArgs>(args: Subset<T, AggregateAlbumArgs>): Promise<GetAlbumAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Album.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__AlbumClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Artist<T extends ArtistArgs = {}>(args?: Subset<T, ArtistArgs>): CheckSelect<T, Prisma__ArtistClient<Artist | null>, Prisma__ArtistClient<ArtistGetPayload<T> | null>>;

  Track<T extends FindManyTrackArgs = {}>(args?: Subset<T, FindManyTrackArgs>): CheckSelect<T, Promise<Array<Track>>, Promise<Array<TrackGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Album findOne
 */
export type FindOneAlbumArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
  /**
   * Filter, which Album to fetch.
  **/
  where: AlbumWhereUniqueInput
}


/**
 * Album findFirst
 */
export type FindFirstAlbumArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
  /**
   * Filter, which Album to fetch.
  **/
  where?: AlbumWhereInput
  orderBy?: Enumerable<AlbumOrderByInput> | AlbumOrderByInput
  cursor?: AlbumWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<AlbumDistinctFieldEnum>
}


/**
 * Album findMany
 */
export type FindManyAlbumArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
  /**
   * Filter, which Albums to fetch.
  **/
  where?: AlbumWhereInput
  /**
   * Determine the order of the Albums to fetch.
  **/
  orderBy?: Enumerable<AlbumOrderByInput> | AlbumOrderByInput
  /**
   * Sets the position for listing Albums.
  **/
  cursor?: AlbumWhereUniqueInput
  /**
   * The number of Albums to fetch. If negative number, it will take Albums before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Albums.
  **/
  skip?: number
  distinct?: Enumerable<AlbumDistinctFieldEnum>
}


/**
 * Album create
 */
export type AlbumCreateArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
  /**
   * The data needed to create a Album.
  **/
  data: AlbumCreateInput
}


/**
 * Album update
 */
export type AlbumUpdateArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
  /**
   * The data needed to update a Album.
  **/
  data: AlbumUpdateInput
  /**
   * Choose, which Album to update.
  **/
  where: AlbumWhereUniqueInput
}


/**
 * Album updateMany
 */
export type AlbumUpdateManyArgs = {
  data: AlbumUpdateManyMutationInput
  where?: AlbumWhereInput
}


/**
 * Album upsert
 */
export type AlbumUpsertArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
  /**
   * The filter to search for the Album to update in case it exists.
  **/
  where: AlbumWhereUniqueInput
  /**
   * In case the Album found by the `where` argument doesn't exist, create a new Album with this data.
  **/
  create: AlbumCreateInput
  /**
   * In case the Album was found with the provided `where` argument, update it with this data.
  **/
  update: AlbumUpdateInput
}


/**
 * Album delete
 */
export type AlbumDeleteArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
  /**
   * Filter which Album to delete.
  **/
  where: AlbumWhereUniqueInput
}


/**
 * Album deleteMany
 */
export type AlbumDeleteManyArgs = {
  where?: AlbumWhereInput
}


/**
 * Album without action
 */
export type AlbumArgs = {
  /**
   * Select specific fields to fetch from the Album
  **/
  select?: AlbumSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AlbumInclude | null
}



/**
 * Model Artist
 */

export type Artist = {
  ArtistId: number
  Name: string | null
}


export type AggregateArtist = {
  count: number
  avg: ArtistAvgAggregateOutputType | null
  sum: ArtistSumAggregateOutputType | null
  min: ArtistMinAggregateOutputType | null
  max: ArtistMaxAggregateOutputType | null
}

export type ArtistAvgAggregateOutputType = {
  ArtistId: number
}

export type ArtistSumAggregateOutputType = {
  ArtistId: number
}

export type ArtistMinAggregateOutputType = {
  ArtistId: number
}

export type ArtistMaxAggregateOutputType = {
  ArtistId: number
}


export type ArtistAvgAggregateInputType = {
  ArtistId?: true
}

export type ArtistSumAggregateInputType = {
  ArtistId?: true
}

export type ArtistMinAggregateInputType = {
  ArtistId?: true
}

export type ArtistMaxAggregateInputType = {
  ArtistId?: true
}

export type AggregateArtistArgs = {
  where?: ArtistWhereInput
  orderBy?: Enumerable<ArtistOrderByInput> | ArtistOrderByInput
  cursor?: ArtistWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<ArtistDistinctFieldEnum>
  count?: true
  avg?: ArtistAvgAggregateInputType
  sum?: ArtistSumAggregateInputType
  min?: ArtistMinAggregateInputType
  max?: ArtistMaxAggregateInputType
}

export type GetArtistAggregateType<T extends AggregateArtistArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetArtistAggregateScalarType<T[P]>
}

export type GetArtistAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof ArtistAvgAggregateOutputType ? ArtistAvgAggregateOutputType[P] : never
}
    
    

export type ArtistSelect = {
  ArtistId?: boolean
  Name?: boolean
  Album?: boolean | FindManyAlbumArgs
}

export type ArtistInclude = {
  Album?: boolean | FindManyAlbumArgs
}

export type ArtistGetPayload<
  S extends boolean | null | undefined | ArtistArgs,
  U = keyof S
> = S extends true
  ? Artist
  : S extends undefined
  ? never
  : S extends ArtistArgs | FindManyArtistArgs
  ? 'include' extends U
    ? Artist  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Album'
      ? Array<AlbumGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Artist ? Artist[P]
: 
      P extends 'Album'
      ? Array<AlbumGetPayload<S['select'][P]>> : never
    }
  : Artist
: Artist


export interface ArtistDelegate {
  /**
   * Find zero or one Artist that matches the filter.
   * @param {FindOneArtistArgs} args - Arguments to find a Artist
   * @example
   * // Get one Artist
   * const artist = await prisma.artist.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneArtistArgs>(
    args: Subset<T, FindOneArtistArgs>
  ): CheckSelect<T, Prisma__ArtistClient<Artist | null>, Prisma__ArtistClient<ArtistGetPayload<T> | null>>
  /**
   * Find the first Artist that matches the filter.
   * @param {FindFirstArtistArgs} args - Arguments to find a Artist
   * @example
   * // Get one Artist
   * const artist = await prisma.artist.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstArtistArgs>(
    args?: Subset<T, FindFirstArtistArgs>
  ): CheckSelect<T, Prisma__ArtistClient<Artist | null>, Prisma__ArtistClient<ArtistGetPayload<T> | null>>
  /**
   * Find zero or more Artists that matches the filter.
   * @param {FindManyArtistArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Artists
   * const artists = await prisma.artist.findMany()
   * 
   * // Get first 10 Artists
   * const artists = await prisma.artist.findMany({ take: 10 })
   * 
   * // Only select the `ArtistId`
   * const artistWithArtistIdOnly = await prisma.artist.findMany({ select: { ArtistId: true } })
   * 
  **/
  findMany<T extends FindManyArtistArgs>(
    args?: Subset<T, FindManyArtistArgs>
  ): CheckSelect<T, Promise<Array<Artist>>, Promise<Array<ArtistGetPayload<T>>>>
  /**
   * Create a Artist.
   * @param {ArtistCreateArgs} args - Arguments to create a Artist.
   * @example
   * // Create one Artist
   * const Artist = await prisma.artist.create({
   *   data: {
   *     // ... data to create a Artist
   *   }
   * })
   * 
  **/
  create<T extends ArtistCreateArgs>(
    args: Subset<T, ArtistCreateArgs>
  ): CheckSelect<T, Prisma__ArtistClient<Artist>, Prisma__ArtistClient<ArtistGetPayload<T>>>
  /**
   * Delete a Artist.
   * @param {ArtistDeleteArgs} args - Arguments to delete one Artist.
   * @example
   * // Delete one Artist
   * const Artist = await prisma.artist.delete({
   *   where: {
   *     // ... filter to delete one Artist
   *   }
   * })
   * 
  **/
  delete<T extends ArtistDeleteArgs>(
    args: Subset<T, ArtistDeleteArgs>
  ): CheckSelect<T, Prisma__ArtistClient<Artist>, Prisma__ArtistClient<ArtistGetPayload<T>>>
  /**
   * Update one Artist.
   * @param {ArtistUpdateArgs} args - Arguments to update one Artist.
   * @example
   * // Update one Artist
   * const artist = await prisma.artist.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends ArtistUpdateArgs>(
    args: Subset<T, ArtistUpdateArgs>
  ): CheckSelect<T, Prisma__ArtistClient<Artist>, Prisma__ArtistClient<ArtistGetPayload<T>>>
  /**
   * Delete zero or more Artists.
   * @param {ArtistDeleteManyArgs} args - Arguments to filter Artists to delete.
   * @example
   * // Delete a few Artists
   * const { count } = await prisma.artist.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends ArtistDeleteManyArgs>(
    args: Subset<T, ArtistDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Artists.
   * @param {ArtistUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Artists
   * const artist = await prisma.artist.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends ArtistUpdateManyArgs>(
    args: Subset<T, ArtistUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Artist.
   * @param {ArtistUpsertArgs} args - Arguments to update or create a Artist.
   * @example
   * // Update or create a Artist
   * const artist = await prisma.artist.upsert({
   *   create: {
   *     // ... data to create a Artist
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Artist we want to update
   *   }
   * })
  **/
  upsert<T extends ArtistUpsertArgs>(
    args: Subset<T, ArtistUpsertArgs>
  ): CheckSelect<T, Prisma__ArtistClient<Artist>, Prisma__ArtistClient<ArtistGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyArtistArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateArtistArgs>(args: Subset<T, AggregateArtistArgs>): Promise<GetArtistAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Artist.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__ArtistClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Album<T extends FindManyAlbumArgs = {}>(args?: Subset<T, FindManyAlbumArgs>): CheckSelect<T, Promise<Array<Album>>, Promise<Array<AlbumGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Artist findOne
 */
export type FindOneArtistArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
  /**
   * Filter, which Artist to fetch.
  **/
  where: ArtistWhereUniqueInput
}


/**
 * Artist findFirst
 */
export type FindFirstArtistArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
  /**
   * Filter, which Artist to fetch.
  **/
  where?: ArtistWhereInput
  orderBy?: Enumerable<ArtistOrderByInput> | ArtistOrderByInput
  cursor?: ArtistWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<ArtistDistinctFieldEnum>
}


/**
 * Artist findMany
 */
export type FindManyArtistArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
  /**
   * Filter, which Artists to fetch.
  **/
  where?: ArtistWhereInput
  /**
   * Determine the order of the Artists to fetch.
  **/
  orderBy?: Enumerable<ArtistOrderByInput> | ArtistOrderByInput
  /**
   * Sets the position for listing Artists.
  **/
  cursor?: ArtistWhereUniqueInput
  /**
   * The number of Artists to fetch. If negative number, it will take Artists before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Artists.
  **/
  skip?: number
  distinct?: Enumerable<ArtistDistinctFieldEnum>
}


/**
 * Artist create
 */
export type ArtistCreateArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
  /**
   * The data needed to create a Artist.
  **/
  data: ArtistCreateInput
}


/**
 * Artist update
 */
export type ArtistUpdateArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
  /**
   * The data needed to update a Artist.
  **/
  data: ArtistUpdateInput
  /**
   * Choose, which Artist to update.
  **/
  where: ArtistWhereUniqueInput
}


/**
 * Artist updateMany
 */
export type ArtistUpdateManyArgs = {
  data: ArtistUpdateManyMutationInput
  where?: ArtistWhereInput
}


/**
 * Artist upsert
 */
export type ArtistUpsertArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
  /**
   * The filter to search for the Artist to update in case it exists.
  **/
  where: ArtistWhereUniqueInput
  /**
   * In case the Artist found by the `where` argument doesn't exist, create a new Artist with this data.
  **/
  create: ArtistCreateInput
  /**
   * In case the Artist was found with the provided `where` argument, update it with this data.
  **/
  update: ArtistUpdateInput
}


/**
 * Artist delete
 */
export type ArtistDeleteArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
  /**
   * Filter which Artist to delete.
  **/
  where: ArtistWhereUniqueInput
}


/**
 * Artist deleteMany
 */
export type ArtistDeleteManyArgs = {
  where?: ArtistWhereInput
}


/**
 * Artist without action
 */
export type ArtistArgs = {
  /**
   * Select specific fields to fetch from the Artist
  **/
  select?: ArtistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ArtistInclude | null
}



/**
 * Model Customer
 */

export type Customer = {
  CustomerId: number
  FirstName: string
  LastName: string
  Company: string | null
  Address: string | null
  City: string | null
  State: string | null
  Country: string | null
  PostalCode: string | null
  Phone: string | null
  Fax: string | null
  Email: string
  SupportRepId: number | null
}


export type AggregateCustomer = {
  count: number
  avg: CustomerAvgAggregateOutputType | null
  sum: CustomerSumAggregateOutputType | null
  min: CustomerMinAggregateOutputType | null
  max: CustomerMaxAggregateOutputType | null
}

export type CustomerAvgAggregateOutputType = {
  CustomerId: number
  SupportRepId: number | null
}

export type CustomerSumAggregateOutputType = {
  CustomerId: number
  SupportRepId: number | null
}

export type CustomerMinAggregateOutputType = {
  CustomerId: number
  SupportRepId: number | null
}

export type CustomerMaxAggregateOutputType = {
  CustomerId: number
  SupportRepId: number | null
}


export type CustomerAvgAggregateInputType = {
  CustomerId?: true
  SupportRepId?: true
}

export type CustomerSumAggregateInputType = {
  CustomerId?: true
  SupportRepId?: true
}

export type CustomerMinAggregateInputType = {
  CustomerId?: true
  SupportRepId?: true
}

export type CustomerMaxAggregateInputType = {
  CustomerId?: true
  SupportRepId?: true
}

export type AggregateCustomerArgs = {
  where?: CustomerWhereInput
  orderBy?: Enumerable<CustomerOrderByInput> | CustomerOrderByInput
  cursor?: CustomerWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<CustomerDistinctFieldEnum>
  count?: true
  avg?: CustomerAvgAggregateInputType
  sum?: CustomerSumAggregateInputType
  min?: CustomerMinAggregateInputType
  max?: CustomerMaxAggregateInputType
}

export type GetCustomerAggregateType<T extends AggregateCustomerArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetCustomerAggregateScalarType<T[P]>
}

export type GetCustomerAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof CustomerAvgAggregateOutputType ? CustomerAvgAggregateOutputType[P] : never
}
    
    

export type CustomerSelect = {
  CustomerId?: boolean
  FirstName?: boolean
  LastName?: boolean
  Company?: boolean
  Address?: boolean
  City?: boolean
  State?: boolean
  Country?: boolean
  PostalCode?: boolean
  Phone?: boolean
  Fax?: boolean
  Email?: boolean
  SupportRepId?: boolean
  Employee?: boolean | EmployeeArgs
  Invoice?: boolean | FindManyInvoiceArgs
}

export type CustomerInclude = {
  Employee?: boolean | EmployeeArgs
  Invoice?: boolean | FindManyInvoiceArgs
}

export type CustomerGetPayload<
  S extends boolean | null | undefined | CustomerArgs,
  U = keyof S
> = S extends true
  ? Customer
  : S extends undefined
  ? never
  : S extends CustomerArgs | FindManyCustomerArgs
  ? 'include' extends U
    ? Customer  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Employee'
      ? EmployeeGetPayload<S['include'][P]> | null :
      P extends 'Invoice'
      ? Array<InvoiceGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Customer ? Customer[P]
: 
      P extends 'Employee'
      ? EmployeeGetPayload<S['select'][P]> | null :
      P extends 'Invoice'
      ? Array<InvoiceGetPayload<S['select'][P]>> : never
    }
  : Customer
: Customer


export interface CustomerDelegate {
  /**
   * Find zero or one Customer that matches the filter.
   * @param {FindOneCustomerArgs} args - Arguments to find a Customer
   * @example
   * // Get one Customer
   * const customer = await prisma.customer.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneCustomerArgs>(
    args: Subset<T, FindOneCustomerArgs>
  ): CheckSelect<T, Prisma__CustomerClient<Customer | null>, Prisma__CustomerClient<CustomerGetPayload<T> | null>>
  /**
   * Find the first Customer that matches the filter.
   * @param {FindFirstCustomerArgs} args - Arguments to find a Customer
   * @example
   * // Get one Customer
   * const customer = await prisma.customer.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstCustomerArgs>(
    args?: Subset<T, FindFirstCustomerArgs>
  ): CheckSelect<T, Prisma__CustomerClient<Customer | null>, Prisma__CustomerClient<CustomerGetPayload<T> | null>>
  /**
   * Find zero or more Customers that matches the filter.
   * @param {FindManyCustomerArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Customers
   * const customers = await prisma.customer.findMany()
   * 
   * // Get first 10 Customers
   * const customers = await prisma.customer.findMany({ take: 10 })
   * 
   * // Only select the `CustomerId`
   * const customerWithCustomerIdOnly = await prisma.customer.findMany({ select: { CustomerId: true } })
   * 
  **/
  findMany<T extends FindManyCustomerArgs>(
    args?: Subset<T, FindManyCustomerArgs>
  ): CheckSelect<T, Promise<Array<Customer>>, Promise<Array<CustomerGetPayload<T>>>>
  /**
   * Create a Customer.
   * @param {CustomerCreateArgs} args - Arguments to create a Customer.
   * @example
   * // Create one Customer
   * const Customer = await prisma.customer.create({
   *   data: {
   *     // ... data to create a Customer
   *   }
   * })
   * 
  **/
  create<T extends CustomerCreateArgs>(
    args: Subset<T, CustomerCreateArgs>
  ): CheckSelect<T, Prisma__CustomerClient<Customer>, Prisma__CustomerClient<CustomerGetPayload<T>>>
  /**
   * Delete a Customer.
   * @param {CustomerDeleteArgs} args - Arguments to delete one Customer.
   * @example
   * // Delete one Customer
   * const Customer = await prisma.customer.delete({
   *   where: {
   *     // ... filter to delete one Customer
   *   }
   * })
   * 
  **/
  delete<T extends CustomerDeleteArgs>(
    args: Subset<T, CustomerDeleteArgs>
  ): CheckSelect<T, Prisma__CustomerClient<Customer>, Prisma__CustomerClient<CustomerGetPayload<T>>>
  /**
   * Update one Customer.
   * @param {CustomerUpdateArgs} args - Arguments to update one Customer.
   * @example
   * // Update one Customer
   * const customer = await prisma.customer.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends CustomerUpdateArgs>(
    args: Subset<T, CustomerUpdateArgs>
  ): CheckSelect<T, Prisma__CustomerClient<Customer>, Prisma__CustomerClient<CustomerGetPayload<T>>>
  /**
   * Delete zero or more Customers.
   * @param {CustomerDeleteManyArgs} args - Arguments to filter Customers to delete.
   * @example
   * // Delete a few Customers
   * const { count } = await prisma.customer.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends CustomerDeleteManyArgs>(
    args: Subset<T, CustomerDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Customers.
   * @param {CustomerUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Customers
   * const customer = await prisma.customer.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends CustomerUpdateManyArgs>(
    args: Subset<T, CustomerUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Customer.
   * @param {CustomerUpsertArgs} args - Arguments to update or create a Customer.
   * @example
   * // Update or create a Customer
   * const customer = await prisma.customer.upsert({
   *   create: {
   *     // ... data to create a Customer
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Customer we want to update
   *   }
   * })
  **/
  upsert<T extends CustomerUpsertArgs>(
    args: Subset<T, CustomerUpsertArgs>
  ): CheckSelect<T, Prisma__CustomerClient<Customer>, Prisma__CustomerClient<CustomerGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyCustomerArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateCustomerArgs>(args: Subset<T, AggregateCustomerArgs>): Promise<GetCustomerAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Customer.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__CustomerClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Employee<T extends EmployeeArgs = {}>(args?: Subset<T, EmployeeArgs>): CheckSelect<T, Prisma__EmployeeClient<Employee | null>, Prisma__EmployeeClient<EmployeeGetPayload<T> | null>>;

  Invoice<T extends FindManyInvoiceArgs = {}>(args?: Subset<T, FindManyInvoiceArgs>): CheckSelect<T, Promise<Array<Invoice>>, Promise<Array<InvoiceGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Customer findOne
 */
export type FindOneCustomerArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
  /**
   * Filter, which Customer to fetch.
  **/
  where: CustomerWhereUniqueInput
}


/**
 * Customer findFirst
 */
export type FindFirstCustomerArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
  /**
   * Filter, which Customer to fetch.
  **/
  where?: CustomerWhereInput
  orderBy?: Enumerable<CustomerOrderByInput> | CustomerOrderByInput
  cursor?: CustomerWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<CustomerDistinctFieldEnum>
}


/**
 * Customer findMany
 */
export type FindManyCustomerArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
  /**
   * Filter, which Customers to fetch.
  **/
  where?: CustomerWhereInput
  /**
   * Determine the order of the Customers to fetch.
  **/
  orderBy?: Enumerable<CustomerOrderByInput> | CustomerOrderByInput
  /**
   * Sets the position for listing Customers.
  **/
  cursor?: CustomerWhereUniqueInput
  /**
   * The number of Customers to fetch. If negative number, it will take Customers before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Customers.
  **/
  skip?: number
  distinct?: Enumerable<CustomerDistinctFieldEnum>
}


/**
 * Customer create
 */
export type CustomerCreateArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
  /**
   * The data needed to create a Customer.
  **/
  data: CustomerCreateInput
}


/**
 * Customer update
 */
export type CustomerUpdateArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
  /**
   * The data needed to update a Customer.
  **/
  data: CustomerUpdateInput
  /**
   * Choose, which Customer to update.
  **/
  where: CustomerWhereUniqueInput
}


/**
 * Customer updateMany
 */
export type CustomerUpdateManyArgs = {
  data: CustomerUpdateManyMutationInput
  where?: CustomerWhereInput
}


/**
 * Customer upsert
 */
export type CustomerUpsertArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
  /**
   * The filter to search for the Customer to update in case it exists.
  **/
  where: CustomerWhereUniqueInput
  /**
   * In case the Customer found by the `where` argument doesn't exist, create a new Customer with this data.
  **/
  create: CustomerCreateInput
  /**
   * In case the Customer was found with the provided `where` argument, update it with this data.
  **/
  update: CustomerUpdateInput
}


/**
 * Customer delete
 */
export type CustomerDeleteArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
  /**
   * Filter which Customer to delete.
  **/
  where: CustomerWhereUniqueInput
}


/**
 * Customer deleteMany
 */
export type CustomerDeleteManyArgs = {
  where?: CustomerWhereInput
}


/**
 * Customer without action
 */
export type CustomerArgs = {
  /**
   * Select specific fields to fetch from the Customer
  **/
  select?: CustomerSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: CustomerInclude | null
}



/**
 * Model Employee
 */

export type Employee = {
  EmployeeId: number
  LastName: string
  FirstName: string
  Title: string | null
  ReportsTo: number | null
  BirthDate: Date | null
  HireDate: Date | null
  Address: string | null
  City: string | null
  State: string | null
  Country: string | null
  PostalCode: string | null
  Phone: string | null
  Fax: string | null
  Email: string | null
}


export type AggregateEmployee = {
  count: number
  avg: EmployeeAvgAggregateOutputType | null
  sum: EmployeeSumAggregateOutputType | null
  min: EmployeeMinAggregateOutputType | null
  max: EmployeeMaxAggregateOutputType | null
}

export type EmployeeAvgAggregateOutputType = {
  EmployeeId: number
  ReportsTo: number | null
}

export type EmployeeSumAggregateOutputType = {
  EmployeeId: number
  ReportsTo: number | null
}

export type EmployeeMinAggregateOutputType = {
  EmployeeId: number
  ReportsTo: number | null
}

export type EmployeeMaxAggregateOutputType = {
  EmployeeId: number
  ReportsTo: number | null
}


export type EmployeeAvgAggregateInputType = {
  EmployeeId?: true
  ReportsTo?: true
}

export type EmployeeSumAggregateInputType = {
  EmployeeId?: true
  ReportsTo?: true
}

export type EmployeeMinAggregateInputType = {
  EmployeeId?: true
  ReportsTo?: true
}

export type EmployeeMaxAggregateInputType = {
  EmployeeId?: true
  ReportsTo?: true
}

export type AggregateEmployeeArgs = {
  where?: EmployeeWhereInput
  orderBy?: Enumerable<EmployeeOrderByInput> | EmployeeOrderByInput
  cursor?: EmployeeWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<EmployeeDistinctFieldEnum>
  count?: true
  avg?: EmployeeAvgAggregateInputType
  sum?: EmployeeSumAggregateInputType
  min?: EmployeeMinAggregateInputType
  max?: EmployeeMaxAggregateInputType
}

export type GetEmployeeAggregateType<T extends AggregateEmployeeArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetEmployeeAggregateScalarType<T[P]>
}

export type GetEmployeeAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof EmployeeAvgAggregateOutputType ? EmployeeAvgAggregateOutputType[P] : never
}
    
    

export type EmployeeSelect = {
  EmployeeId?: boolean
  LastName?: boolean
  FirstName?: boolean
  Title?: boolean
  ReportsTo?: boolean
  BirthDate?: boolean
  HireDate?: boolean
  Address?: boolean
  City?: boolean
  State?: boolean
  Country?: boolean
  PostalCode?: boolean
  Phone?: boolean
  Fax?: boolean
  Email?: boolean
  Employee?: boolean | EmployeeArgs
  Customer?: boolean | FindManyCustomerArgs
  other_Employee?: boolean | FindManyEmployeeArgs
}

export type EmployeeInclude = {
  Employee?: boolean | EmployeeArgs
  Customer?: boolean | FindManyCustomerArgs
  other_Employee?: boolean | FindManyEmployeeArgs
}

export type EmployeeGetPayload<
  S extends boolean | null | undefined | EmployeeArgs,
  U = keyof S
> = S extends true
  ? Employee
  : S extends undefined
  ? never
  : S extends EmployeeArgs | FindManyEmployeeArgs
  ? 'include' extends U
    ? Employee  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Employee'
      ? EmployeeGetPayload<S['include'][P]> | null :
      P extends 'Customer'
      ? Array<CustomerGetPayload<S['include'][P]>> :
      P extends 'other_Employee'
      ? Array<EmployeeGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Employee ? Employee[P]
: 
      P extends 'Employee'
      ? EmployeeGetPayload<S['select'][P]> | null :
      P extends 'Customer'
      ? Array<CustomerGetPayload<S['select'][P]>> :
      P extends 'other_Employee'
      ? Array<EmployeeGetPayload<S['select'][P]>> : never
    }
  : Employee
: Employee


export interface EmployeeDelegate {
  /**
   * Find zero or one Employee that matches the filter.
   * @param {FindOneEmployeeArgs} args - Arguments to find a Employee
   * @example
   * // Get one Employee
   * const employee = await prisma.employee.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneEmployeeArgs>(
    args: Subset<T, FindOneEmployeeArgs>
  ): CheckSelect<T, Prisma__EmployeeClient<Employee | null>, Prisma__EmployeeClient<EmployeeGetPayload<T> | null>>
  /**
   * Find the first Employee that matches the filter.
   * @param {FindFirstEmployeeArgs} args - Arguments to find a Employee
   * @example
   * // Get one Employee
   * const employee = await prisma.employee.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstEmployeeArgs>(
    args?: Subset<T, FindFirstEmployeeArgs>
  ): CheckSelect<T, Prisma__EmployeeClient<Employee | null>, Prisma__EmployeeClient<EmployeeGetPayload<T> | null>>
  /**
   * Find zero or more Employees that matches the filter.
   * @param {FindManyEmployeeArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Employees
   * const employees = await prisma.employee.findMany()
   * 
   * // Get first 10 Employees
   * const employees = await prisma.employee.findMany({ take: 10 })
   * 
   * // Only select the `EmployeeId`
   * const employeeWithEmployeeIdOnly = await prisma.employee.findMany({ select: { EmployeeId: true } })
   * 
  **/
  findMany<T extends FindManyEmployeeArgs>(
    args?: Subset<T, FindManyEmployeeArgs>
  ): CheckSelect<T, Promise<Array<Employee>>, Promise<Array<EmployeeGetPayload<T>>>>
  /**
   * Create a Employee.
   * @param {EmployeeCreateArgs} args - Arguments to create a Employee.
   * @example
   * // Create one Employee
   * const Employee = await prisma.employee.create({
   *   data: {
   *     // ... data to create a Employee
   *   }
   * })
   * 
  **/
  create<T extends EmployeeCreateArgs>(
    args: Subset<T, EmployeeCreateArgs>
  ): CheckSelect<T, Prisma__EmployeeClient<Employee>, Prisma__EmployeeClient<EmployeeGetPayload<T>>>
  /**
   * Delete a Employee.
   * @param {EmployeeDeleteArgs} args - Arguments to delete one Employee.
   * @example
   * // Delete one Employee
   * const Employee = await prisma.employee.delete({
   *   where: {
   *     // ... filter to delete one Employee
   *   }
   * })
   * 
  **/
  delete<T extends EmployeeDeleteArgs>(
    args: Subset<T, EmployeeDeleteArgs>
  ): CheckSelect<T, Prisma__EmployeeClient<Employee>, Prisma__EmployeeClient<EmployeeGetPayload<T>>>
  /**
   * Update one Employee.
   * @param {EmployeeUpdateArgs} args - Arguments to update one Employee.
   * @example
   * // Update one Employee
   * const employee = await prisma.employee.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends EmployeeUpdateArgs>(
    args: Subset<T, EmployeeUpdateArgs>
  ): CheckSelect<T, Prisma__EmployeeClient<Employee>, Prisma__EmployeeClient<EmployeeGetPayload<T>>>
  /**
   * Delete zero or more Employees.
   * @param {EmployeeDeleteManyArgs} args - Arguments to filter Employees to delete.
   * @example
   * // Delete a few Employees
   * const { count } = await prisma.employee.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends EmployeeDeleteManyArgs>(
    args: Subset<T, EmployeeDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Employees.
   * @param {EmployeeUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Employees
   * const employee = await prisma.employee.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends EmployeeUpdateManyArgs>(
    args: Subset<T, EmployeeUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Employee.
   * @param {EmployeeUpsertArgs} args - Arguments to update or create a Employee.
   * @example
   * // Update or create a Employee
   * const employee = await prisma.employee.upsert({
   *   create: {
   *     // ... data to create a Employee
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Employee we want to update
   *   }
   * })
  **/
  upsert<T extends EmployeeUpsertArgs>(
    args: Subset<T, EmployeeUpsertArgs>
  ): CheckSelect<T, Prisma__EmployeeClient<Employee>, Prisma__EmployeeClient<EmployeeGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyEmployeeArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateEmployeeArgs>(args: Subset<T, AggregateEmployeeArgs>): Promise<GetEmployeeAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Employee.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__EmployeeClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Employee<T extends EmployeeArgs = {}>(args?: Subset<T, EmployeeArgs>): CheckSelect<T, Prisma__EmployeeClient<Employee | null>, Prisma__EmployeeClient<EmployeeGetPayload<T> | null>>;

  Customer<T extends FindManyCustomerArgs = {}>(args?: Subset<T, FindManyCustomerArgs>): CheckSelect<T, Promise<Array<Customer>>, Promise<Array<CustomerGetPayload<T>>>>;

  other_Employee<T extends FindManyEmployeeArgs = {}>(args?: Subset<T, FindManyEmployeeArgs>): CheckSelect<T, Promise<Array<Employee>>, Promise<Array<EmployeeGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Employee findOne
 */
export type FindOneEmployeeArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
  /**
   * Filter, which Employee to fetch.
  **/
  where: EmployeeWhereUniqueInput
}


/**
 * Employee findFirst
 */
export type FindFirstEmployeeArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
  /**
   * Filter, which Employee to fetch.
  **/
  where?: EmployeeWhereInput
  orderBy?: Enumerable<EmployeeOrderByInput> | EmployeeOrderByInput
  cursor?: EmployeeWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<EmployeeDistinctFieldEnum>
}


/**
 * Employee findMany
 */
export type FindManyEmployeeArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
  /**
   * Filter, which Employees to fetch.
  **/
  where?: EmployeeWhereInput
  /**
   * Determine the order of the Employees to fetch.
  **/
  orderBy?: Enumerable<EmployeeOrderByInput> | EmployeeOrderByInput
  /**
   * Sets the position for listing Employees.
  **/
  cursor?: EmployeeWhereUniqueInput
  /**
   * The number of Employees to fetch. If negative number, it will take Employees before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Employees.
  **/
  skip?: number
  distinct?: Enumerable<EmployeeDistinctFieldEnum>
}


/**
 * Employee create
 */
export type EmployeeCreateArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
  /**
   * The data needed to create a Employee.
  **/
  data: EmployeeCreateInput
}


/**
 * Employee update
 */
export type EmployeeUpdateArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
  /**
   * The data needed to update a Employee.
  **/
  data: EmployeeUpdateInput
  /**
   * Choose, which Employee to update.
  **/
  where: EmployeeWhereUniqueInput
}


/**
 * Employee updateMany
 */
export type EmployeeUpdateManyArgs = {
  data: EmployeeUpdateManyMutationInput
  where?: EmployeeWhereInput
}


/**
 * Employee upsert
 */
export type EmployeeUpsertArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
  /**
   * The filter to search for the Employee to update in case it exists.
  **/
  where: EmployeeWhereUniqueInput
  /**
   * In case the Employee found by the `where` argument doesn't exist, create a new Employee with this data.
  **/
  create: EmployeeCreateInput
  /**
   * In case the Employee was found with the provided `where` argument, update it with this data.
  **/
  update: EmployeeUpdateInput
}


/**
 * Employee delete
 */
export type EmployeeDeleteArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
  /**
   * Filter which Employee to delete.
  **/
  where: EmployeeWhereUniqueInput
}


/**
 * Employee deleteMany
 */
export type EmployeeDeleteManyArgs = {
  where?: EmployeeWhereInput
}


/**
 * Employee without action
 */
export type EmployeeArgs = {
  /**
   * Select specific fields to fetch from the Employee
  **/
  select?: EmployeeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: EmployeeInclude | null
}



/**
 * Model Genre
 */

export type Genre = {
  GenreId: number
  Name: string | null
}


export type AggregateGenre = {
  count: number
  avg: GenreAvgAggregateOutputType | null
  sum: GenreSumAggregateOutputType | null
  min: GenreMinAggregateOutputType | null
  max: GenreMaxAggregateOutputType | null
}

export type GenreAvgAggregateOutputType = {
  GenreId: number
}

export type GenreSumAggregateOutputType = {
  GenreId: number
}

export type GenreMinAggregateOutputType = {
  GenreId: number
}

export type GenreMaxAggregateOutputType = {
  GenreId: number
}


export type GenreAvgAggregateInputType = {
  GenreId?: true
}

export type GenreSumAggregateInputType = {
  GenreId?: true
}

export type GenreMinAggregateInputType = {
  GenreId?: true
}

export type GenreMaxAggregateInputType = {
  GenreId?: true
}

export type AggregateGenreArgs = {
  where?: GenreWhereInput
  orderBy?: Enumerable<GenreOrderByInput> | GenreOrderByInput
  cursor?: GenreWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<GenreDistinctFieldEnum>
  count?: true
  avg?: GenreAvgAggregateInputType
  sum?: GenreSumAggregateInputType
  min?: GenreMinAggregateInputType
  max?: GenreMaxAggregateInputType
}

export type GetGenreAggregateType<T extends AggregateGenreArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetGenreAggregateScalarType<T[P]>
}

export type GetGenreAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof GenreAvgAggregateOutputType ? GenreAvgAggregateOutputType[P] : never
}
    
    

export type GenreSelect = {
  GenreId?: boolean
  Name?: boolean
  Track?: boolean | FindManyTrackArgs
}

export type GenreInclude = {
  Track?: boolean | FindManyTrackArgs
}

export type GenreGetPayload<
  S extends boolean | null | undefined | GenreArgs,
  U = keyof S
> = S extends true
  ? Genre
  : S extends undefined
  ? never
  : S extends GenreArgs | FindManyGenreArgs
  ? 'include' extends U
    ? Genre  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Track'
      ? Array<TrackGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Genre ? Genre[P]
: 
      P extends 'Track'
      ? Array<TrackGetPayload<S['select'][P]>> : never
    }
  : Genre
: Genre


export interface GenreDelegate {
  /**
   * Find zero or one Genre that matches the filter.
   * @param {FindOneGenreArgs} args - Arguments to find a Genre
   * @example
   * // Get one Genre
   * const genre = await prisma.genre.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneGenreArgs>(
    args: Subset<T, FindOneGenreArgs>
  ): CheckSelect<T, Prisma__GenreClient<Genre | null>, Prisma__GenreClient<GenreGetPayload<T> | null>>
  /**
   * Find the first Genre that matches the filter.
   * @param {FindFirstGenreArgs} args - Arguments to find a Genre
   * @example
   * // Get one Genre
   * const genre = await prisma.genre.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstGenreArgs>(
    args?: Subset<T, FindFirstGenreArgs>
  ): CheckSelect<T, Prisma__GenreClient<Genre | null>, Prisma__GenreClient<GenreGetPayload<T> | null>>
  /**
   * Find zero or more Genres that matches the filter.
   * @param {FindManyGenreArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Genres
   * const genres = await prisma.genre.findMany()
   * 
   * // Get first 10 Genres
   * const genres = await prisma.genre.findMany({ take: 10 })
   * 
   * // Only select the `GenreId`
   * const genreWithGenreIdOnly = await prisma.genre.findMany({ select: { GenreId: true } })
   * 
  **/
  findMany<T extends FindManyGenreArgs>(
    args?: Subset<T, FindManyGenreArgs>
  ): CheckSelect<T, Promise<Array<Genre>>, Promise<Array<GenreGetPayload<T>>>>
  /**
   * Create a Genre.
   * @param {GenreCreateArgs} args - Arguments to create a Genre.
   * @example
   * // Create one Genre
   * const Genre = await prisma.genre.create({
   *   data: {
   *     // ... data to create a Genre
   *   }
   * })
   * 
  **/
  create<T extends GenreCreateArgs>(
    args: Subset<T, GenreCreateArgs>
  ): CheckSelect<T, Prisma__GenreClient<Genre>, Prisma__GenreClient<GenreGetPayload<T>>>
  /**
   * Delete a Genre.
   * @param {GenreDeleteArgs} args - Arguments to delete one Genre.
   * @example
   * // Delete one Genre
   * const Genre = await prisma.genre.delete({
   *   where: {
   *     // ... filter to delete one Genre
   *   }
   * })
   * 
  **/
  delete<T extends GenreDeleteArgs>(
    args: Subset<T, GenreDeleteArgs>
  ): CheckSelect<T, Prisma__GenreClient<Genre>, Prisma__GenreClient<GenreGetPayload<T>>>
  /**
   * Update one Genre.
   * @param {GenreUpdateArgs} args - Arguments to update one Genre.
   * @example
   * // Update one Genre
   * const genre = await prisma.genre.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends GenreUpdateArgs>(
    args: Subset<T, GenreUpdateArgs>
  ): CheckSelect<T, Prisma__GenreClient<Genre>, Prisma__GenreClient<GenreGetPayload<T>>>
  /**
   * Delete zero or more Genres.
   * @param {GenreDeleteManyArgs} args - Arguments to filter Genres to delete.
   * @example
   * // Delete a few Genres
   * const { count } = await prisma.genre.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends GenreDeleteManyArgs>(
    args: Subset<T, GenreDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Genres.
   * @param {GenreUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Genres
   * const genre = await prisma.genre.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends GenreUpdateManyArgs>(
    args: Subset<T, GenreUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Genre.
   * @param {GenreUpsertArgs} args - Arguments to update or create a Genre.
   * @example
   * // Update or create a Genre
   * const genre = await prisma.genre.upsert({
   *   create: {
   *     // ... data to create a Genre
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Genre we want to update
   *   }
   * })
  **/
  upsert<T extends GenreUpsertArgs>(
    args: Subset<T, GenreUpsertArgs>
  ): CheckSelect<T, Prisma__GenreClient<Genre>, Prisma__GenreClient<GenreGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyGenreArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateGenreArgs>(args: Subset<T, AggregateGenreArgs>): Promise<GetGenreAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Genre.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__GenreClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Track<T extends FindManyTrackArgs = {}>(args?: Subset<T, FindManyTrackArgs>): CheckSelect<T, Promise<Array<Track>>, Promise<Array<TrackGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Genre findOne
 */
export type FindOneGenreArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
  /**
   * Filter, which Genre to fetch.
  **/
  where: GenreWhereUniqueInput
}


/**
 * Genre findFirst
 */
export type FindFirstGenreArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
  /**
   * Filter, which Genre to fetch.
  **/
  where?: GenreWhereInput
  orderBy?: Enumerable<GenreOrderByInput> | GenreOrderByInput
  cursor?: GenreWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<GenreDistinctFieldEnum>
}


/**
 * Genre findMany
 */
export type FindManyGenreArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
  /**
   * Filter, which Genres to fetch.
  **/
  where?: GenreWhereInput
  /**
   * Determine the order of the Genres to fetch.
  **/
  orderBy?: Enumerable<GenreOrderByInput> | GenreOrderByInput
  /**
   * Sets the position for listing Genres.
  **/
  cursor?: GenreWhereUniqueInput
  /**
   * The number of Genres to fetch. If negative number, it will take Genres before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Genres.
  **/
  skip?: number
  distinct?: Enumerable<GenreDistinctFieldEnum>
}


/**
 * Genre create
 */
export type GenreCreateArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
  /**
   * The data needed to create a Genre.
  **/
  data: GenreCreateInput
}


/**
 * Genre update
 */
export type GenreUpdateArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
  /**
   * The data needed to update a Genre.
  **/
  data: GenreUpdateInput
  /**
   * Choose, which Genre to update.
  **/
  where: GenreWhereUniqueInput
}


/**
 * Genre updateMany
 */
export type GenreUpdateManyArgs = {
  data: GenreUpdateManyMutationInput
  where?: GenreWhereInput
}


/**
 * Genre upsert
 */
export type GenreUpsertArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
  /**
   * The filter to search for the Genre to update in case it exists.
  **/
  where: GenreWhereUniqueInput
  /**
   * In case the Genre found by the `where` argument doesn't exist, create a new Genre with this data.
  **/
  create: GenreCreateInput
  /**
   * In case the Genre was found with the provided `where` argument, update it with this data.
  **/
  update: GenreUpdateInput
}


/**
 * Genre delete
 */
export type GenreDeleteArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
  /**
   * Filter which Genre to delete.
  **/
  where: GenreWhereUniqueInput
}


/**
 * Genre deleteMany
 */
export type GenreDeleteManyArgs = {
  where?: GenreWhereInput
}


/**
 * Genre without action
 */
export type GenreArgs = {
  /**
   * Select specific fields to fetch from the Genre
  **/
  select?: GenreSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: GenreInclude | null
}



/**
 * Model Invoice
 */

export type Invoice = {
  InvoiceId: number
  CustomerId: number
  InvoiceDate: Date
  BillingAddress: string | null
  BillingCity: string | null
  BillingState: string | null
  BillingCountry: string | null
  BillingPostalCode: string | null
  Total: number
}


export type AggregateInvoice = {
  count: number
  avg: InvoiceAvgAggregateOutputType | null
  sum: InvoiceSumAggregateOutputType | null
  min: InvoiceMinAggregateOutputType | null
  max: InvoiceMaxAggregateOutputType | null
}

export type InvoiceAvgAggregateOutputType = {
  InvoiceId: number
  CustomerId: number
  Total: number
}

export type InvoiceSumAggregateOutputType = {
  InvoiceId: number
  CustomerId: number
  Total: number
}

export type InvoiceMinAggregateOutputType = {
  InvoiceId: number
  CustomerId: number
  Total: number
}

export type InvoiceMaxAggregateOutputType = {
  InvoiceId: number
  CustomerId: number
  Total: number
}


export type InvoiceAvgAggregateInputType = {
  InvoiceId?: true
  CustomerId?: true
  Total?: true
}

export type InvoiceSumAggregateInputType = {
  InvoiceId?: true
  CustomerId?: true
  Total?: true
}

export type InvoiceMinAggregateInputType = {
  InvoiceId?: true
  CustomerId?: true
  Total?: true
}

export type InvoiceMaxAggregateInputType = {
  InvoiceId?: true
  CustomerId?: true
  Total?: true
}

export type AggregateInvoiceArgs = {
  where?: InvoiceWhereInput
  orderBy?: Enumerable<InvoiceOrderByInput> | InvoiceOrderByInput
  cursor?: InvoiceWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<InvoiceDistinctFieldEnum>
  count?: true
  avg?: InvoiceAvgAggregateInputType
  sum?: InvoiceSumAggregateInputType
  min?: InvoiceMinAggregateInputType
  max?: InvoiceMaxAggregateInputType
}

export type GetInvoiceAggregateType<T extends AggregateInvoiceArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetInvoiceAggregateScalarType<T[P]>
}

export type GetInvoiceAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof InvoiceAvgAggregateOutputType ? InvoiceAvgAggregateOutputType[P] : never
}
    
    

export type InvoiceSelect = {
  InvoiceId?: boolean
  CustomerId?: boolean
  InvoiceDate?: boolean
  BillingAddress?: boolean
  BillingCity?: boolean
  BillingState?: boolean
  BillingCountry?: boolean
  BillingPostalCode?: boolean
  Total?: boolean
  Customer?: boolean | CustomerArgs
  InvoiceLine?: boolean | FindManyInvoiceLineArgs
}

export type InvoiceInclude = {
  Customer?: boolean | CustomerArgs
  InvoiceLine?: boolean | FindManyInvoiceLineArgs
}

export type InvoiceGetPayload<
  S extends boolean | null | undefined | InvoiceArgs,
  U = keyof S
> = S extends true
  ? Invoice
  : S extends undefined
  ? never
  : S extends InvoiceArgs | FindManyInvoiceArgs
  ? 'include' extends U
    ? Invoice  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Customer'
      ? CustomerGetPayload<S['include'][P]> :
      P extends 'InvoiceLine'
      ? Array<InvoiceLineGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Invoice ? Invoice[P]
: 
      P extends 'Customer'
      ? CustomerGetPayload<S['select'][P]> :
      P extends 'InvoiceLine'
      ? Array<InvoiceLineGetPayload<S['select'][P]>> : never
    }
  : Invoice
: Invoice


export interface InvoiceDelegate {
  /**
   * Find zero or one Invoice that matches the filter.
   * @param {FindOneInvoiceArgs} args - Arguments to find a Invoice
   * @example
   * // Get one Invoice
   * const invoice = await prisma.invoice.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneInvoiceArgs>(
    args: Subset<T, FindOneInvoiceArgs>
  ): CheckSelect<T, Prisma__InvoiceClient<Invoice | null>, Prisma__InvoiceClient<InvoiceGetPayload<T> | null>>
  /**
   * Find the first Invoice that matches the filter.
   * @param {FindFirstInvoiceArgs} args - Arguments to find a Invoice
   * @example
   * // Get one Invoice
   * const invoice = await prisma.invoice.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstInvoiceArgs>(
    args?: Subset<T, FindFirstInvoiceArgs>
  ): CheckSelect<T, Prisma__InvoiceClient<Invoice | null>, Prisma__InvoiceClient<InvoiceGetPayload<T> | null>>
  /**
   * Find zero or more Invoices that matches the filter.
   * @param {FindManyInvoiceArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Invoices
   * const invoices = await prisma.invoice.findMany()
   * 
   * // Get first 10 Invoices
   * const invoices = await prisma.invoice.findMany({ take: 10 })
   * 
   * // Only select the `InvoiceId`
   * const invoiceWithInvoiceIdOnly = await prisma.invoice.findMany({ select: { InvoiceId: true } })
   * 
  **/
  findMany<T extends FindManyInvoiceArgs>(
    args?: Subset<T, FindManyInvoiceArgs>
  ): CheckSelect<T, Promise<Array<Invoice>>, Promise<Array<InvoiceGetPayload<T>>>>
  /**
   * Create a Invoice.
   * @param {InvoiceCreateArgs} args - Arguments to create a Invoice.
   * @example
   * // Create one Invoice
   * const Invoice = await prisma.invoice.create({
   *   data: {
   *     // ... data to create a Invoice
   *   }
   * })
   * 
  **/
  create<T extends InvoiceCreateArgs>(
    args: Subset<T, InvoiceCreateArgs>
  ): CheckSelect<T, Prisma__InvoiceClient<Invoice>, Prisma__InvoiceClient<InvoiceGetPayload<T>>>
  /**
   * Delete a Invoice.
   * @param {InvoiceDeleteArgs} args - Arguments to delete one Invoice.
   * @example
   * // Delete one Invoice
   * const Invoice = await prisma.invoice.delete({
   *   where: {
   *     // ... filter to delete one Invoice
   *   }
   * })
   * 
  **/
  delete<T extends InvoiceDeleteArgs>(
    args: Subset<T, InvoiceDeleteArgs>
  ): CheckSelect<T, Prisma__InvoiceClient<Invoice>, Prisma__InvoiceClient<InvoiceGetPayload<T>>>
  /**
   * Update one Invoice.
   * @param {InvoiceUpdateArgs} args - Arguments to update one Invoice.
   * @example
   * // Update one Invoice
   * const invoice = await prisma.invoice.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends InvoiceUpdateArgs>(
    args: Subset<T, InvoiceUpdateArgs>
  ): CheckSelect<T, Prisma__InvoiceClient<Invoice>, Prisma__InvoiceClient<InvoiceGetPayload<T>>>
  /**
   * Delete zero or more Invoices.
   * @param {InvoiceDeleteManyArgs} args - Arguments to filter Invoices to delete.
   * @example
   * // Delete a few Invoices
   * const { count } = await prisma.invoice.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends InvoiceDeleteManyArgs>(
    args: Subset<T, InvoiceDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Invoices.
   * @param {InvoiceUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Invoices
   * const invoice = await prisma.invoice.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends InvoiceUpdateManyArgs>(
    args: Subset<T, InvoiceUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Invoice.
   * @param {InvoiceUpsertArgs} args - Arguments to update or create a Invoice.
   * @example
   * // Update or create a Invoice
   * const invoice = await prisma.invoice.upsert({
   *   create: {
   *     // ... data to create a Invoice
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Invoice we want to update
   *   }
   * })
  **/
  upsert<T extends InvoiceUpsertArgs>(
    args: Subset<T, InvoiceUpsertArgs>
  ): CheckSelect<T, Prisma__InvoiceClient<Invoice>, Prisma__InvoiceClient<InvoiceGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyInvoiceArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateInvoiceArgs>(args: Subset<T, AggregateInvoiceArgs>): Promise<GetInvoiceAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Invoice.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__InvoiceClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Customer<T extends CustomerArgs = {}>(args?: Subset<T, CustomerArgs>): CheckSelect<T, Prisma__CustomerClient<Customer | null>, Prisma__CustomerClient<CustomerGetPayload<T> | null>>;

  InvoiceLine<T extends FindManyInvoiceLineArgs = {}>(args?: Subset<T, FindManyInvoiceLineArgs>): CheckSelect<T, Promise<Array<InvoiceLine>>, Promise<Array<InvoiceLineGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Invoice findOne
 */
export type FindOneInvoiceArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
  /**
   * Filter, which Invoice to fetch.
  **/
  where: InvoiceWhereUniqueInput
}


/**
 * Invoice findFirst
 */
export type FindFirstInvoiceArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
  /**
   * Filter, which Invoice to fetch.
  **/
  where?: InvoiceWhereInput
  orderBy?: Enumerable<InvoiceOrderByInput> | InvoiceOrderByInput
  cursor?: InvoiceWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<InvoiceDistinctFieldEnum>
}


/**
 * Invoice findMany
 */
export type FindManyInvoiceArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
  /**
   * Filter, which Invoices to fetch.
  **/
  where?: InvoiceWhereInput
  /**
   * Determine the order of the Invoices to fetch.
  **/
  orderBy?: Enumerable<InvoiceOrderByInput> | InvoiceOrderByInput
  /**
   * Sets the position for listing Invoices.
  **/
  cursor?: InvoiceWhereUniqueInput
  /**
   * The number of Invoices to fetch. If negative number, it will take Invoices before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Invoices.
  **/
  skip?: number
  distinct?: Enumerable<InvoiceDistinctFieldEnum>
}


/**
 * Invoice create
 */
export type InvoiceCreateArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
  /**
   * The data needed to create a Invoice.
  **/
  data: InvoiceCreateInput
}


/**
 * Invoice update
 */
export type InvoiceUpdateArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
  /**
   * The data needed to update a Invoice.
  **/
  data: InvoiceUpdateInput
  /**
   * Choose, which Invoice to update.
  **/
  where: InvoiceWhereUniqueInput
}


/**
 * Invoice updateMany
 */
export type InvoiceUpdateManyArgs = {
  data: InvoiceUpdateManyMutationInput
  where?: InvoiceWhereInput
}


/**
 * Invoice upsert
 */
export type InvoiceUpsertArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
  /**
   * The filter to search for the Invoice to update in case it exists.
  **/
  where: InvoiceWhereUniqueInput
  /**
   * In case the Invoice found by the `where` argument doesn't exist, create a new Invoice with this data.
  **/
  create: InvoiceCreateInput
  /**
   * In case the Invoice was found with the provided `where` argument, update it with this data.
  **/
  update: InvoiceUpdateInput
}


/**
 * Invoice delete
 */
export type InvoiceDeleteArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
  /**
   * Filter which Invoice to delete.
  **/
  where: InvoiceWhereUniqueInput
}


/**
 * Invoice deleteMany
 */
export type InvoiceDeleteManyArgs = {
  where?: InvoiceWhereInput
}


/**
 * Invoice without action
 */
export type InvoiceArgs = {
  /**
   * Select specific fields to fetch from the Invoice
  **/
  select?: InvoiceSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceInclude | null
}



/**
 * Model InvoiceLine
 */

export type InvoiceLine = {
  InvoiceLineId: number
  InvoiceId: number
  TrackId: number
  UnitPrice: number
  Quantity: number
}


export type AggregateInvoiceLine = {
  count: number
  avg: InvoiceLineAvgAggregateOutputType | null
  sum: InvoiceLineSumAggregateOutputType | null
  min: InvoiceLineMinAggregateOutputType | null
  max: InvoiceLineMaxAggregateOutputType | null
}

export type InvoiceLineAvgAggregateOutputType = {
  InvoiceLineId: number
  InvoiceId: number
  TrackId: number
  UnitPrice: number
  Quantity: number
}

export type InvoiceLineSumAggregateOutputType = {
  InvoiceLineId: number
  InvoiceId: number
  TrackId: number
  UnitPrice: number
  Quantity: number
}

export type InvoiceLineMinAggregateOutputType = {
  InvoiceLineId: number
  InvoiceId: number
  TrackId: number
  UnitPrice: number
  Quantity: number
}

export type InvoiceLineMaxAggregateOutputType = {
  InvoiceLineId: number
  InvoiceId: number
  TrackId: number
  UnitPrice: number
  Quantity: number
}


export type InvoiceLineAvgAggregateInputType = {
  InvoiceLineId?: true
  InvoiceId?: true
  TrackId?: true
  UnitPrice?: true
  Quantity?: true
}

export type InvoiceLineSumAggregateInputType = {
  InvoiceLineId?: true
  InvoiceId?: true
  TrackId?: true
  UnitPrice?: true
  Quantity?: true
}

export type InvoiceLineMinAggregateInputType = {
  InvoiceLineId?: true
  InvoiceId?: true
  TrackId?: true
  UnitPrice?: true
  Quantity?: true
}

export type InvoiceLineMaxAggregateInputType = {
  InvoiceLineId?: true
  InvoiceId?: true
  TrackId?: true
  UnitPrice?: true
  Quantity?: true
}

export type AggregateInvoiceLineArgs = {
  where?: InvoiceLineWhereInput
  orderBy?: Enumerable<InvoiceLineOrderByInput> | InvoiceLineOrderByInput
  cursor?: InvoiceLineWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<InvoiceLineDistinctFieldEnum>
  count?: true
  avg?: InvoiceLineAvgAggregateInputType
  sum?: InvoiceLineSumAggregateInputType
  min?: InvoiceLineMinAggregateInputType
  max?: InvoiceLineMaxAggregateInputType
}

export type GetInvoiceLineAggregateType<T extends AggregateInvoiceLineArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetInvoiceLineAggregateScalarType<T[P]>
}

export type GetInvoiceLineAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof InvoiceLineAvgAggregateOutputType ? InvoiceLineAvgAggregateOutputType[P] : never
}
    
    

export type InvoiceLineSelect = {
  InvoiceLineId?: boolean
  InvoiceId?: boolean
  TrackId?: boolean
  UnitPrice?: boolean
  Quantity?: boolean
  Invoice?: boolean | InvoiceArgs
  Track?: boolean | TrackArgs
}

export type InvoiceLineInclude = {
  Invoice?: boolean | InvoiceArgs
  Track?: boolean | TrackArgs
}

export type InvoiceLineGetPayload<
  S extends boolean | null | undefined | InvoiceLineArgs,
  U = keyof S
> = S extends true
  ? InvoiceLine
  : S extends undefined
  ? never
  : S extends InvoiceLineArgs | FindManyInvoiceLineArgs
  ? 'include' extends U
    ? InvoiceLine  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Invoice'
      ? InvoiceGetPayload<S['include'][P]> :
      P extends 'Track'
      ? TrackGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof InvoiceLine ? InvoiceLine[P]
: 
      P extends 'Invoice'
      ? InvoiceGetPayload<S['select'][P]> :
      P extends 'Track'
      ? TrackGetPayload<S['select'][P]> : never
    }
  : InvoiceLine
: InvoiceLine


export interface InvoiceLineDelegate {
  /**
   * Find zero or one InvoiceLine that matches the filter.
   * @param {FindOneInvoiceLineArgs} args - Arguments to find a InvoiceLine
   * @example
   * // Get one InvoiceLine
   * const invoiceLine = await prisma.invoiceLine.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneInvoiceLineArgs>(
    args: Subset<T, FindOneInvoiceLineArgs>
  ): CheckSelect<T, Prisma__InvoiceLineClient<InvoiceLine | null>, Prisma__InvoiceLineClient<InvoiceLineGetPayload<T> | null>>
  /**
   * Find the first InvoiceLine that matches the filter.
   * @param {FindFirstInvoiceLineArgs} args - Arguments to find a InvoiceLine
   * @example
   * // Get one InvoiceLine
   * const invoiceLine = await prisma.invoiceLine.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstInvoiceLineArgs>(
    args?: Subset<T, FindFirstInvoiceLineArgs>
  ): CheckSelect<T, Prisma__InvoiceLineClient<InvoiceLine | null>, Prisma__InvoiceLineClient<InvoiceLineGetPayload<T> | null>>
  /**
   * Find zero or more InvoiceLines that matches the filter.
   * @param {FindManyInvoiceLineArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all InvoiceLines
   * const invoiceLines = await prisma.invoiceLine.findMany()
   * 
   * // Get first 10 InvoiceLines
   * const invoiceLines = await prisma.invoiceLine.findMany({ take: 10 })
   * 
   * // Only select the `InvoiceLineId`
   * const invoiceLineWithInvoiceLineIdOnly = await prisma.invoiceLine.findMany({ select: { InvoiceLineId: true } })
   * 
  **/
  findMany<T extends FindManyInvoiceLineArgs>(
    args?: Subset<T, FindManyInvoiceLineArgs>
  ): CheckSelect<T, Promise<Array<InvoiceLine>>, Promise<Array<InvoiceLineGetPayload<T>>>>
  /**
   * Create a InvoiceLine.
   * @param {InvoiceLineCreateArgs} args - Arguments to create a InvoiceLine.
   * @example
   * // Create one InvoiceLine
   * const InvoiceLine = await prisma.invoiceLine.create({
   *   data: {
   *     // ... data to create a InvoiceLine
   *   }
   * })
   * 
  **/
  create<T extends InvoiceLineCreateArgs>(
    args: Subset<T, InvoiceLineCreateArgs>
  ): CheckSelect<T, Prisma__InvoiceLineClient<InvoiceLine>, Prisma__InvoiceLineClient<InvoiceLineGetPayload<T>>>
  /**
   * Delete a InvoiceLine.
   * @param {InvoiceLineDeleteArgs} args - Arguments to delete one InvoiceLine.
   * @example
   * // Delete one InvoiceLine
   * const InvoiceLine = await prisma.invoiceLine.delete({
   *   where: {
   *     // ... filter to delete one InvoiceLine
   *   }
   * })
   * 
  **/
  delete<T extends InvoiceLineDeleteArgs>(
    args: Subset<T, InvoiceLineDeleteArgs>
  ): CheckSelect<T, Prisma__InvoiceLineClient<InvoiceLine>, Prisma__InvoiceLineClient<InvoiceLineGetPayload<T>>>
  /**
   * Update one InvoiceLine.
   * @param {InvoiceLineUpdateArgs} args - Arguments to update one InvoiceLine.
   * @example
   * // Update one InvoiceLine
   * const invoiceLine = await prisma.invoiceLine.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends InvoiceLineUpdateArgs>(
    args: Subset<T, InvoiceLineUpdateArgs>
  ): CheckSelect<T, Prisma__InvoiceLineClient<InvoiceLine>, Prisma__InvoiceLineClient<InvoiceLineGetPayload<T>>>
  /**
   * Delete zero or more InvoiceLines.
   * @param {InvoiceLineDeleteManyArgs} args - Arguments to filter InvoiceLines to delete.
   * @example
   * // Delete a few InvoiceLines
   * const { count } = await prisma.invoiceLine.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends InvoiceLineDeleteManyArgs>(
    args: Subset<T, InvoiceLineDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more InvoiceLines.
   * @param {InvoiceLineUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many InvoiceLines
   * const invoiceLine = await prisma.invoiceLine.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends InvoiceLineUpdateManyArgs>(
    args: Subset<T, InvoiceLineUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one InvoiceLine.
   * @param {InvoiceLineUpsertArgs} args - Arguments to update or create a InvoiceLine.
   * @example
   * // Update or create a InvoiceLine
   * const invoiceLine = await prisma.invoiceLine.upsert({
   *   create: {
   *     // ... data to create a InvoiceLine
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the InvoiceLine we want to update
   *   }
   * })
  **/
  upsert<T extends InvoiceLineUpsertArgs>(
    args: Subset<T, InvoiceLineUpsertArgs>
  ): CheckSelect<T, Prisma__InvoiceLineClient<InvoiceLine>, Prisma__InvoiceLineClient<InvoiceLineGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyInvoiceLineArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateInvoiceLineArgs>(args: Subset<T, AggregateInvoiceLineArgs>): Promise<GetInvoiceLineAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for InvoiceLine.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__InvoiceLineClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Invoice<T extends InvoiceArgs = {}>(args?: Subset<T, InvoiceArgs>): CheckSelect<T, Prisma__InvoiceClient<Invoice | null>, Prisma__InvoiceClient<InvoiceGetPayload<T> | null>>;

  Track<T extends TrackArgs = {}>(args?: Subset<T, TrackArgs>): CheckSelect<T, Prisma__TrackClient<Track | null>, Prisma__TrackClient<TrackGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * InvoiceLine findOne
 */
export type FindOneInvoiceLineArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
  /**
   * Filter, which InvoiceLine to fetch.
  **/
  where: InvoiceLineWhereUniqueInput
}


/**
 * InvoiceLine findFirst
 */
export type FindFirstInvoiceLineArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
  /**
   * Filter, which InvoiceLine to fetch.
  **/
  where?: InvoiceLineWhereInput
  orderBy?: Enumerable<InvoiceLineOrderByInput> | InvoiceLineOrderByInput
  cursor?: InvoiceLineWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<InvoiceLineDistinctFieldEnum>
}


/**
 * InvoiceLine findMany
 */
export type FindManyInvoiceLineArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
  /**
   * Filter, which InvoiceLines to fetch.
  **/
  where?: InvoiceLineWhereInput
  /**
   * Determine the order of the InvoiceLines to fetch.
  **/
  orderBy?: Enumerable<InvoiceLineOrderByInput> | InvoiceLineOrderByInput
  /**
   * Sets the position for listing InvoiceLines.
  **/
  cursor?: InvoiceLineWhereUniqueInput
  /**
   * The number of InvoiceLines to fetch. If negative number, it will take InvoiceLines before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` InvoiceLines.
  **/
  skip?: number
  distinct?: Enumerable<InvoiceLineDistinctFieldEnum>
}


/**
 * InvoiceLine create
 */
export type InvoiceLineCreateArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
  /**
   * The data needed to create a InvoiceLine.
  **/
  data: InvoiceLineCreateInput
}


/**
 * InvoiceLine update
 */
export type InvoiceLineUpdateArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
  /**
   * The data needed to update a InvoiceLine.
  **/
  data: InvoiceLineUpdateInput
  /**
   * Choose, which InvoiceLine to update.
  **/
  where: InvoiceLineWhereUniqueInput
}


/**
 * InvoiceLine updateMany
 */
export type InvoiceLineUpdateManyArgs = {
  data: InvoiceLineUpdateManyMutationInput
  where?: InvoiceLineWhereInput
}


/**
 * InvoiceLine upsert
 */
export type InvoiceLineUpsertArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
  /**
   * The filter to search for the InvoiceLine to update in case it exists.
  **/
  where: InvoiceLineWhereUniqueInput
  /**
   * In case the InvoiceLine found by the `where` argument doesn't exist, create a new InvoiceLine with this data.
  **/
  create: InvoiceLineCreateInput
  /**
   * In case the InvoiceLine was found with the provided `where` argument, update it with this data.
  **/
  update: InvoiceLineUpdateInput
}


/**
 * InvoiceLine delete
 */
export type InvoiceLineDeleteArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
  /**
   * Filter which InvoiceLine to delete.
  **/
  where: InvoiceLineWhereUniqueInput
}


/**
 * InvoiceLine deleteMany
 */
export type InvoiceLineDeleteManyArgs = {
  where?: InvoiceLineWhereInput
}


/**
 * InvoiceLine without action
 */
export type InvoiceLineArgs = {
  /**
   * Select specific fields to fetch from the InvoiceLine
  **/
  select?: InvoiceLineSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: InvoiceLineInclude | null
}



/**
 * Model MediaType
 */

export type MediaType = {
  MediaTypeId: number
  Name: string | null
}


export type AggregateMediaType = {
  count: number
  avg: MediaTypeAvgAggregateOutputType | null
  sum: MediaTypeSumAggregateOutputType | null
  min: MediaTypeMinAggregateOutputType | null
  max: MediaTypeMaxAggregateOutputType | null
}

export type MediaTypeAvgAggregateOutputType = {
  MediaTypeId: number
}

export type MediaTypeSumAggregateOutputType = {
  MediaTypeId: number
}

export type MediaTypeMinAggregateOutputType = {
  MediaTypeId: number
}

export type MediaTypeMaxAggregateOutputType = {
  MediaTypeId: number
}


export type MediaTypeAvgAggregateInputType = {
  MediaTypeId?: true
}

export type MediaTypeSumAggregateInputType = {
  MediaTypeId?: true
}

export type MediaTypeMinAggregateInputType = {
  MediaTypeId?: true
}

export type MediaTypeMaxAggregateInputType = {
  MediaTypeId?: true
}

export type AggregateMediaTypeArgs = {
  where?: MediaTypeWhereInput
  orderBy?: Enumerable<MediaTypeOrderByInput> | MediaTypeOrderByInput
  cursor?: MediaTypeWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<MediaTypeDistinctFieldEnum>
  count?: true
  avg?: MediaTypeAvgAggregateInputType
  sum?: MediaTypeSumAggregateInputType
  min?: MediaTypeMinAggregateInputType
  max?: MediaTypeMaxAggregateInputType
}

export type GetMediaTypeAggregateType<T extends AggregateMediaTypeArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetMediaTypeAggregateScalarType<T[P]>
}

export type GetMediaTypeAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof MediaTypeAvgAggregateOutputType ? MediaTypeAvgAggregateOutputType[P] : never
}
    
    

export type MediaTypeSelect = {
  MediaTypeId?: boolean
  Name?: boolean
  Track?: boolean | FindManyTrackArgs
}

export type MediaTypeInclude = {
  Track?: boolean | FindManyTrackArgs
}

export type MediaTypeGetPayload<
  S extends boolean | null | undefined | MediaTypeArgs,
  U = keyof S
> = S extends true
  ? MediaType
  : S extends undefined
  ? never
  : S extends MediaTypeArgs | FindManyMediaTypeArgs
  ? 'include' extends U
    ? MediaType  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Track'
      ? Array<TrackGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof MediaType ? MediaType[P]
: 
      P extends 'Track'
      ? Array<TrackGetPayload<S['select'][P]>> : never
    }
  : MediaType
: MediaType


export interface MediaTypeDelegate {
  /**
   * Find zero or one MediaType that matches the filter.
   * @param {FindOneMediaTypeArgs} args - Arguments to find a MediaType
   * @example
   * // Get one MediaType
   * const mediaType = await prisma.mediaType.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneMediaTypeArgs>(
    args: Subset<T, FindOneMediaTypeArgs>
  ): CheckSelect<T, Prisma__MediaTypeClient<MediaType | null>, Prisma__MediaTypeClient<MediaTypeGetPayload<T> | null>>
  /**
   * Find the first MediaType that matches the filter.
   * @param {FindFirstMediaTypeArgs} args - Arguments to find a MediaType
   * @example
   * // Get one MediaType
   * const mediaType = await prisma.mediaType.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstMediaTypeArgs>(
    args?: Subset<T, FindFirstMediaTypeArgs>
  ): CheckSelect<T, Prisma__MediaTypeClient<MediaType | null>, Prisma__MediaTypeClient<MediaTypeGetPayload<T> | null>>
  /**
   * Find zero or more MediaTypes that matches the filter.
   * @param {FindManyMediaTypeArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all MediaTypes
   * const mediaTypes = await prisma.mediaType.findMany()
   * 
   * // Get first 10 MediaTypes
   * const mediaTypes = await prisma.mediaType.findMany({ take: 10 })
   * 
   * // Only select the `MediaTypeId`
   * const mediaTypeWithMediaTypeIdOnly = await prisma.mediaType.findMany({ select: { MediaTypeId: true } })
   * 
  **/
  findMany<T extends FindManyMediaTypeArgs>(
    args?: Subset<T, FindManyMediaTypeArgs>
  ): CheckSelect<T, Promise<Array<MediaType>>, Promise<Array<MediaTypeGetPayload<T>>>>
  /**
   * Create a MediaType.
   * @param {MediaTypeCreateArgs} args - Arguments to create a MediaType.
   * @example
   * // Create one MediaType
   * const MediaType = await prisma.mediaType.create({
   *   data: {
   *     // ... data to create a MediaType
   *   }
   * })
   * 
  **/
  create<T extends MediaTypeCreateArgs>(
    args: Subset<T, MediaTypeCreateArgs>
  ): CheckSelect<T, Prisma__MediaTypeClient<MediaType>, Prisma__MediaTypeClient<MediaTypeGetPayload<T>>>
  /**
   * Delete a MediaType.
   * @param {MediaTypeDeleteArgs} args - Arguments to delete one MediaType.
   * @example
   * // Delete one MediaType
   * const MediaType = await prisma.mediaType.delete({
   *   where: {
   *     // ... filter to delete one MediaType
   *   }
   * })
   * 
  **/
  delete<T extends MediaTypeDeleteArgs>(
    args: Subset<T, MediaTypeDeleteArgs>
  ): CheckSelect<T, Prisma__MediaTypeClient<MediaType>, Prisma__MediaTypeClient<MediaTypeGetPayload<T>>>
  /**
   * Update one MediaType.
   * @param {MediaTypeUpdateArgs} args - Arguments to update one MediaType.
   * @example
   * // Update one MediaType
   * const mediaType = await prisma.mediaType.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends MediaTypeUpdateArgs>(
    args: Subset<T, MediaTypeUpdateArgs>
  ): CheckSelect<T, Prisma__MediaTypeClient<MediaType>, Prisma__MediaTypeClient<MediaTypeGetPayload<T>>>
  /**
   * Delete zero or more MediaTypes.
   * @param {MediaTypeDeleteManyArgs} args - Arguments to filter MediaTypes to delete.
   * @example
   * // Delete a few MediaTypes
   * const { count } = await prisma.mediaType.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends MediaTypeDeleteManyArgs>(
    args: Subset<T, MediaTypeDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more MediaTypes.
   * @param {MediaTypeUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many MediaTypes
   * const mediaType = await prisma.mediaType.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends MediaTypeUpdateManyArgs>(
    args: Subset<T, MediaTypeUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one MediaType.
   * @param {MediaTypeUpsertArgs} args - Arguments to update or create a MediaType.
   * @example
   * // Update or create a MediaType
   * const mediaType = await prisma.mediaType.upsert({
   *   create: {
   *     // ... data to create a MediaType
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the MediaType we want to update
   *   }
   * })
  **/
  upsert<T extends MediaTypeUpsertArgs>(
    args: Subset<T, MediaTypeUpsertArgs>
  ): CheckSelect<T, Prisma__MediaTypeClient<MediaType>, Prisma__MediaTypeClient<MediaTypeGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyMediaTypeArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateMediaTypeArgs>(args: Subset<T, AggregateMediaTypeArgs>): Promise<GetMediaTypeAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for MediaType.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__MediaTypeClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Track<T extends FindManyTrackArgs = {}>(args?: Subset<T, FindManyTrackArgs>): CheckSelect<T, Promise<Array<Track>>, Promise<Array<TrackGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * MediaType findOne
 */
export type FindOneMediaTypeArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
  /**
   * Filter, which MediaType to fetch.
  **/
  where: MediaTypeWhereUniqueInput
}


/**
 * MediaType findFirst
 */
export type FindFirstMediaTypeArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
  /**
   * Filter, which MediaType to fetch.
  **/
  where?: MediaTypeWhereInput
  orderBy?: Enumerable<MediaTypeOrderByInput> | MediaTypeOrderByInput
  cursor?: MediaTypeWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<MediaTypeDistinctFieldEnum>
}


/**
 * MediaType findMany
 */
export type FindManyMediaTypeArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
  /**
   * Filter, which MediaTypes to fetch.
  **/
  where?: MediaTypeWhereInput
  /**
   * Determine the order of the MediaTypes to fetch.
  **/
  orderBy?: Enumerable<MediaTypeOrderByInput> | MediaTypeOrderByInput
  /**
   * Sets the position for listing MediaTypes.
  **/
  cursor?: MediaTypeWhereUniqueInput
  /**
   * The number of MediaTypes to fetch. If negative number, it will take MediaTypes before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` MediaTypes.
  **/
  skip?: number
  distinct?: Enumerable<MediaTypeDistinctFieldEnum>
}


/**
 * MediaType create
 */
export type MediaTypeCreateArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
  /**
   * The data needed to create a MediaType.
  **/
  data: MediaTypeCreateInput
}


/**
 * MediaType update
 */
export type MediaTypeUpdateArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
  /**
   * The data needed to update a MediaType.
  **/
  data: MediaTypeUpdateInput
  /**
   * Choose, which MediaType to update.
  **/
  where: MediaTypeWhereUniqueInput
}


/**
 * MediaType updateMany
 */
export type MediaTypeUpdateManyArgs = {
  data: MediaTypeUpdateManyMutationInput
  where?: MediaTypeWhereInput
}


/**
 * MediaType upsert
 */
export type MediaTypeUpsertArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
  /**
   * The filter to search for the MediaType to update in case it exists.
  **/
  where: MediaTypeWhereUniqueInput
  /**
   * In case the MediaType found by the `where` argument doesn't exist, create a new MediaType with this data.
  **/
  create: MediaTypeCreateInput
  /**
   * In case the MediaType was found with the provided `where` argument, update it with this data.
  **/
  update: MediaTypeUpdateInput
}


/**
 * MediaType delete
 */
export type MediaTypeDeleteArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
  /**
   * Filter which MediaType to delete.
  **/
  where: MediaTypeWhereUniqueInput
}


/**
 * MediaType deleteMany
 */
export type MediaTypeDeleteManyArgs = {
  where?: MediaTypeWhereInput
}


/**
 * MediaType without action
 */
export type MediaTypeArgs = {
  /**
   * Select specific fields to fetch from the MediaType
  **/
  select?: MediaTypeSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: MediaTypeInclude | null
}



/**
 * Model Playlist
 */

export type Playlist = {
  PlaylistId: number
  Name: string | null
}


export type AggregatePlaylist = {
  count: number
  avg: PlaylistAvgAggregateOutputType | null
  sum: PlaylistSumAggregateOutputType | null
  min: PlaylistMinAggregateOutputType | null
  max: PlaylistMaxAggregateOutputType | null
}

export type PlaylistAvgAggregateOutputType = {
  PlaylistId: number
}

export type PlaylistSumAggregateOutputType = {
  PlaylistId: number
}

export type PlaylistMinAggregateOutputType = {
  PlaylistId: number
}

export type PlaylistMaxAggregateOutputType = {
  PlaylistId: number
}


export type PlaylistAvgAggregateInputType = {
  PlaylistId?: true
}

export type PlaylistSumAggregateInputType = {
  PlaylistId?: true
}

export type PlaylistMinAggregateInputType = {
  PlaylistId?: true
}

export type PlaylistMaxAggregateInputType = {
  PlaylistId?: true
}

export type AggregatePlaylistArgs = {
  where?: PlaylistWhereInput
  orderBy?: Enumerable<PlaylistOrderByInput> | PlaylistOrderByInput
  cursor?: PlaylistWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<PlaylistDistinctFieldEnum>
  count?: true
  avg?: PlaylistAvgAggregateInputType
  sum?: PlaylistSumAggregateInputType
  min?: PlaylistMinAggregateInputType
  max?: PlaylistMaxAggregateInputType
}

export type GetPlaylistAggregateType<T extends AggregatePlaylistArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetPlaylistAggregateScalarType<T[P]>
}

export type GetPlaylistAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof PlaylistAvgAggregateOutputType ? PlaylistAvgAggregateOutputType[P] : never
}
    
    

export type PlaylistSelect = {
  PlaylistId?: boolean
  Name?: boolean
  PlaylistTrack?: boolean | FindManyPlaylistTrackArgs
}

export type PlaylistInclude = {
  PlaylistTrack?: boolean | FindManyPlaylistTrackArgs
}

export type PlaylistGetPayload<
  S extends boolean | null | undefined | PlaylistArgs,
  U = keyof S
> = S extends true
  ? Playlist
  : S extends undefined
  ? never
  : S extends PlaylistArgs | FindManyPlaylistArgs
  ? 'include' extends U
    ? Playlist  & {
      [P in TrueKeys<S['include']>]:
      P extends 'PlaylistTrack'
      ? Array<PlaylistTrackGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Playlist ? Playlist[P]
: 
      P extends 'PlaylistTrack'
      ? Array<PlaylistTrackGetPayload<S['select'][P]>> : never
    }
  : Playlist
: Playlist


export interface PlaylistDelegate {
  /**
   * Find zero or one Playlist that matches the filter.
   * @param {FindOnePlaylistArgs} args - Arguments to find a Playlist
   * @example
   * // Get one Playlist
   * const playlist = await prisma.playlist.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnePlaylistArgs>(
    args: Subset<T, FindOnePlaylistArgs>
  ): CheckSelect<T, Prisma__PlaylistClient<Playlist | null>, Prisma__PlaylistClient<PlaylistGetPayload<T> | null>>
  /**
   * Find the first Playlist that matches the filter.
   * @param {FindFirstPlaylistArgs} args - Arguments to find a Playlist
   * @example
   * // Get one Playlist
   * const playlist = await prisma.playlist.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstPlaylistArgs>(
    args?: Subset<T, FindFirstPlaylistArgs>
  ): CheckSelect<T, Prisma__PlaylistClient<Playlist | null>, Prisma__PlaylistClient<PlaylistGetPayload<T> | null>>
  /**
   * Find zero or more Playlists that matches the filter.
   * @param {FindManyPlaylistArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Playlists
   * const playlists = await prisma.playlist.findMany()
   * 
   * // Get first 10 Playlists
   * const playlists = await prisma.playlist.findMany({ take: 10 })
   * 
   * // Only select the `PlaylistId`
   * const playlistWithPlaylistIdOnly = await prisma.playlist.findMany({ select: { PlaylistId: true } })
   * 
  **/
  findMany<T extends FindManyPlaylistArgs>(
    args?: Subset<T, FindManyPlaylistArgs>
  ): CheckSelect<T, Promise<Array<Playlist>>, Promise<Array<PlaylistGetPayload<T>>>>
  /**
   * Create a Playlist.
   * @param {PlaylistCreateArgs} args - Arguments to create a Playlist.
   * @example
   * // Create one Playlist
   * const Playlist = await prisma.playlist.create({
   *   data: {
   *     // ... data to create a Playlist
   *   }
   * })
   * 
  **/
  create<T extends PlaylistCreateArgs>(
    args: Subset<T, PlaylistCreateArgs>
  ): CheckSelect<T, Prisma__PlaylistClient<Playlist>, Prisma__PlaylistClient<PlaylistGetPayload<T>>>
  /**
   * Delete a Playlist.
   * @param {PlaylistDeleteArgs} args - Arguments to delete one Playlist.
   * @example
   * // Delete one Playlist
   * const Playlist = await prisma.playlist.delete({
   *   where: {
   *     // ... filter to delete one Playlist
   *   }
   * })
   * 
  **/
  delete<T extends PlaylistDeleteArgs>(
    args: Subset<T, PlaylistDeleteArgs>
  ): CheckSelect<T, Prisma__PlaylistClient<Playlist>, Prisma__PlaylistClient<PlaylistGetPayload<T>>>
  /**
   * Update one Playlist.
   * @param {PlaylistUpdateArgs} args - Arguments to update one Playlist.
   * @example
   * // Update one Playlist
   * const playlist = await prisma.playlist.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends PlaylistUpdateArgs>(
    args: Subset<T, PlaylistUpdateArgs>
  ): CheckSelect<T, Prisma__PlaylistClient<Playlist>, Prisma__PlaylistClient<PlaylistGetPayload<T>>>
  /**
   * Delete zero or more Playlists.
   * @param {PlaylistDeleteManyArgs} args - Arguments to filter Playlists to delete.
   * @example
   * // Delete a few Playlists
   * const { count } = await prisma.playlist.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends PlaylistDeleteManyArgs>(
    args: Subset<T, PlaylistDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Playlists.
   * @param {PlaylistUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Playlists
   * const playlist = await prisma.playlist.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends PlaylistUpdateManyArgs>(
    args: Subset<T, PlaylistUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Playlist.
   * @param {PlaylistUpsertArgs} args - Arguments to update or create a Playlist.
   * @example
   * // Update or create a Playlist
   * const playlist = await prisma.playlist.upsert({
   *   create: {
   *     // ... data to create a Playlist
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Playlist we want to update
   *   }
   * })
  **/
  upsert<T extends PlaylistUpsertArgs>(
    args: Subset<T, PlaylistUpsertArgs>
  ): CheckSelect<T, Prisma__PlaylistClient<Playlist>, Prisma__PlaylistClient<PlaylistGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyPlaylistArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregatePlaylistArgs>(args: Subset<T, AggregatePlaylistArgs>): Promise<GetPlaylistAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Playlist.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__PlaylistClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  PlaylistTrack<T extends FindManyPlaylistTrackArgs = {}>(args?: Subset<T, FindManyPlaylistTrackArgs>): CheckSelect<T, Promise<Array<PlaylistTrack>>, Promise<Array<PlaylistTrackGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Playlist findOne
 */
export type FindOnePlaylistArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
  /**
   * Filter, which Playlist to fetch.
  **/
  where: PlaylistWhereUniqueInput
}


/**
 * Playlist findFirst
 */
export type FindFirstPlaylistArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
  /**
   * Filter, which Playlist to fetch.
  **/
  where?: PlaylistWhereInput
  orderBy?: Enumerable<PlaylistOrderByInput> | PlaylistOrderByInput
  cursor?: PlaylistWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<PlaylistDistinctFieldEnum>
}


/**
 * Playlist findMany
 */
export type FindManyPlaylistArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
  /**
   * Filter, which Playlists to fetch.
  **/
  where?: PlaylistWhereInput
  /**
   * Determine the order of the Playlists to fetch.
  **/
  orderBy?: Enumerable<PlaylistOrderByInput> | PlaylistOrderByInput
  /**
   * Sets the position for listing Playlists.
  **/
  cursor?: PlaylistWhereUniqueInput
  /**
   * The number of Playlists to fetch. If negative number, it will take Playlists before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Playlists.
  **/
  skip?: number
  distinct?: Enumerable<PlaylistDistinctFieldEnum>
}


/**
 * Playlist create
 */
export type PlaylistCreateArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
  /**
   * The data needed to create a Playlist.
  **/
  data: PlaylistCreateInput
}


/**
 * Playlist update
 */
export type PlaylistUpdateArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
  /**
   * The data needed to update a Playlist.
  **/
  data: PlaylistUpdateInput
  /**
   * Choose, which Playlist to update.
  **/
  where: PlaylistWhereUniqueInput
}


/**
 * Playlist updateMany
 */
export type PlaylistUpdateManyArgs = {
  data: PlaylistUpdateManyMutationInput
  where?: PlaylistWhereInput
}


/**
 * Playlist upsert
 */
export type PlaylistUpsertArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
  /**
   * The filter to search for the Playlist to update in case it exists.
  **/
  where: PlaylistWhereUniqueInput
  /**
   * In case the Playlist found by the `where` argument doesn't exist, create a new Playlist with this data.
  **/
  create: PlaylistCreateInput
  /**
   * In case the Playlist was found with the provided `where` argument, update it with this data.
  **/
  update: PlaylistUpdateInput
}


/**
 * Playlist delete
 */
export type PlaylistDeleteArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
  /**
   * Filter which Playlist to delete.
  **/
  where: PlaylistWhereUniqueInput
}


/**
 * Playlist deleteMany
 */
export type PlaylistDeleteManyArgs = {
  where?: PlaylistWhereInput
}


/**
 * Playlist without action
 */
export type PlaylistArgs = {
  /**
   * Select specific fields to fetch from the Playlist
  **/
  select?: PlaylistSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistInclude | null
}



/**
 * Model PlaylistTrack
 */

export type PlaylistTrack = {
  PlaylistId: number
  TrackId: number
}


export type AggregatePlaylistTrack = {
  count: number
  avg: PlaylistTrackAvgAggregateOutputType | null
  sum: PlaylistTrackSumAggregateOutputType | null
  min: PlaylistTrackMinAggregateOutputType | null
  max: PlaylistTrackMaxAggregateOutputType | null
}

export type PlaylistTrackAvgAggregateOutputType = {
  PlaylistId: number
  TrackId: number
}

export type PlaylistTrackSumAggregateOutputType = {
  PlaylistId: number
  TrackId: number
}

export type PlaylistTrackMinAggregateOutputType = {
  PlaylistId: number
  TrackId: number
}

export type PlaylistTrackMaxAggregateOutputType = {
  PlaylistId: number
  TrackId: number
}


export type PlaylistTrackAvgAggregateInputType = {
  PlaylistId?: true
  TrackId?: true
}

export type PlaylistTrackSumAggregateInputType = {
  PlaylistId?: true
  TrackId?: true
}

export type PlaylistTrackMinAggregateInputType = {
  PlaylistId?: true
  TrackId?: true
}

export type PlaylistTrackMaxAggregateInputType = {
  PlaylistId?: true
  TrackId?: true
}

export type AggregatePlaylistTrackArgs = {
  where?: PlaylistTrackWhereInput
  orderBy?: Enumerable<PlaylistTrackOrderByInput> | PlaylistTrackOrderByInput
  cursor?: PlaylistTrackWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<PlaylistTrackDistinctFieldEnum>
  count?: true
  avg?: PlaylistTrackAvgAggregateInputType
  sum?: PlaylistTrackSumAggregateInputType
  min?: PlaylistTrackMinAggregateInputType
  max?: PlaylistTrackMaxAggregateInputType
}

export type GetPlaylistTrackAggregateType<T extends AggregatePlaylistTrackArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetPlaylistTrackAggregateScalarType<T[P]>
}

export type GetPlaylistTrackAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof PlaylistTrackAvgAggregateOutputType ? PlaylistTrackAvgAggregateOutputType[P] : never
}
    
    

export type PlaylistTrackSelect = {
  PlaylistId?: boolean
  TrackId?: boolean
  Playlist?: boolean | PlaylistArgs
  Track?: boolean | TrackArgs
}

export type PlaylistTrackInclude = {
  Playlist?: boolean | PlaylistArgs
  Track?: boolean | TrackArgs
}

export type PlaylistTrackGetPayload<
  S extends boolean | null | undefined | PlaylistTrackArgs,
  U = keyof S
> = S extends true
  ? PlaylistTrack
  : S extends undefined
  ? never
  : S extends PlaylistTrackArgs | FindManyPlaylistTrackArgs
  ? 'include' extends U
    ? PlaylistTrack  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Playlist'
      ? PlaylistGetPayload<S['include'][P]> :
      P extends 'Track'
      ? TrackGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof PlaylistTrack ? PlaylistTrack[P]
: 
      P extends 'Playlist'
      ? PlaylistGetPayload<S['select'][P]> :
      P extends 'Track'
      ? TrackGetPayload<S['select'][P]> : never
    }
  : PlaylistTrack
: PlaylistTrack


export interface PlaylistTrackDelegate {
  /**
   * Find zero or one PlaylistTrack that matches the filter.
   * @param {FindOnePlaylistTrackArgs} args - Arguments to find a PlaylistTrack
   * @example
   * // Get one PlaylistTrack
   * const playlistTrack = await prisma.playlistTrack.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnePlaylistTrackArgs>(
    args: Subset<T, FindOnePlaylistTrackArgs>
  ): CheckSelect<T, Prisma__PlaylistTrackClient<PlaylistTrack | null>, Prisma__PlaylistTrackClient<PlaylistTrackGetPayload<T> | null>>
  /**
   * Find the first PlaylistTrack that matches the filter.
   * @param {FindFirstPlaylistTrackArgs} args - Arguments to find a PlaylistTrack
   * @example
   * // Get one PlaylistTrack
   * const playlistTrack = await prisma.playlistTrack.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstPlaylistTrackArgs>(
    args?: Subset<T, FindFirstPlaylistTrackArgs>
  ): CheckSelect<T, Prisma__PlaylistTrackClient<PlaylistTrack | null>, Prisma__PlaylistTrackClient<PlaylistTrackGetPayload<T> | null>>
  /**
   * Find zero or more PlaylistTracks that matches the filter.
   * @param {FindManyPlaylistTrackArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all PlaylistTracks
   * const playlistTracks = await prisma.playlistTrack.findMany()
   * 
   * // Get first 10 PlaylistTracks
   * const playlistTracks = await prisma.playlistTrack.findMany({ take: 10 })
   * 
   * // Only select the `PlaylistId`
   * const playlistTrackWithPlaylistIdOnly = await prisma.playlistTrack.findMany({ select: { PlaylistId: true } })
   * 
  **/
  findMany<T extends FindManyPlaylistTrackArgs>(
    args?: Subset<T, FindManyPlaylistTrackArgs>
  ): CheckSelect<T, Promise<Array<PlaylistTrack>>, Promise<Array<PlaylistTrackGetPayload<T>>>>
  /**
   * Create a PlaylistTrack.
   * @param {PlaylistTrackCreateArgs} args - Arguments to create a PlaylistTrack.
   * @example
   * // Create one PlaylistTrack
   * const PlaylistTrack = await prisma.playlistTrack.create({
   *   data: {
   *     // ... data to create a PlaylistTrack
   *   }
   * })
   * 
  **/
  create<T extends PlaylistTrackCreateArgs>(
    args: Subset<T, PlaylistTrackCreateArgs>
  ): CheckSelect<T, Prisma__PlaylistTrackClient<PlaylistTrack>, Prisma__PlaylistTrackClient<PlaylistTrackGetPayload<T>>>
  /**
   * Delete a PlaylistTrack.
   * @param {PlaylistTrackDeleteArgs} args - Arguments to delete one PlaylistTrack.
   * @example
   * // Delete one PlaylistTrack
   * const PlaylistTrack = await prisma.playlistTrack.delete({
   *   where: {
   *     // ... filter to delete one PlaylistTrack
   *   }
   * })
   * 
  **/
  delete<T extends PlaylistTrackDeleteArgs>(
    args: Subset<T, PlaylistTrackDeleteArgs>
  ): CheckSelect<T, Prisma__PlaylistTrackClient<PlaylistTrack>, Prisma__PlaylistTrackClient<PlaylistTrackGetPayload<T>>>
  /**
   * Update one PlaylistTrack.
   * @param {PlaylistTrackUpdateArgs} args - Arguments to update one PlaylistTrack.
   * @example
   * // Update one PlaylistTrack
   * const playlistTrack = await prisma.playlistTrack.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends PlaylistTrackUpdateArgs>(
    args: Subset<T, PlaylistTrackUpdateArgs>
  ): CheckSelect<T, Prisma__PlaylistTrackClient<PlaylistTrack>, Prisma__PlaylistTrackClient<PlaylistTrackGetPayload<T>>>
  /**
   * Delete zero or more PlaylistTracks.
   * @param {PlaylistTrackDeleteManyArgs} args - Arguments to filter PlaylistTracks to delete.
   * @example
   * // Delete a few PlaylistTracks
   * const { count } = await prisma.playlistTrack.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends PlaylistTrackDeleteManyArgs>(
    args: Subset<T, PlaylistTrackDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more PlaylistTracks.
   * @param {PlaylistTrackUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many PlaylistTracks
   * const playlistTrack = await prisma.playlistTrack.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends PlaylistTrackUpdateManyArgs>(
    args: Subset<T, PlaylistTrackUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one PlaylistTrack.
   * @param {PlaylistTrackUpsertArgs} args - Arguments to update or create a PlaylistTrack.
   * @example
   * // Update or create a PlaylistTrack
   * const playlistTrack = await prisma.playlistTrack.upsert({
   *   create: {
   *     // ... data to create a PlaylistTrack
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the PlaylistTrack we want to update
   *   }
   * })
  **/
  upsert<T extends PlaylistTrackUpsertArgs>(
    args: Subset<T, PlaylistTrackUpsertArgs>
  ): CheckSelect<T, Prisma__PlaylistTrackClient<PlaylistTrack>, Prisma__PlaylistTrackClient<PlaylistTrackGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyPlaylistTrackArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregatePlaylistTrackArgs>(args: Subset<T, AggregatePlaylistTrackArgs>): Promise<GetPlaylistTrackAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for PlaylistTrack.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__PlaylistTrackClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Playlist<T extends PlaylistArgs = {}>(args?: Subset<T, PlaylistArgs>): CheckSelect<T, Prisma__PlaylistClient<Playlist | null>, Prisma__PlaylistClient<PlaylistGetPayload<T> | null>>;

  Track<T extends TrackArgs = {}>(args?: Subset<T, TrackArgs>): CheckSelect<T, Prisma__TrackClient<Track | null>, Prisma__TrackClient<TrackGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * PlaylistTrack findOne
 */
export type FindOnePlaylistTrackArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
  /**
   * Filter, which PlaylistTrack to fetch.
  **/
  where: PlaylistTrackWhereUniqueInput
}


/**
 * PlaylistTrack findFirst
 */
export type FindFirstPlaylistTrackArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
  /**
   * Filter, which PlaylistTrack to fetch.
  **/
  where?: PlaylistTrackWhereInput
  orderBy?: Enumerable<PlaylistTrackOrderByInput> | PlaylistTrackOrderByInput
  cursor?: PlaylistTrackWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<PlaylistTrackDistinctFieldEnum>
}


/**
 * PlaylistTrack findMany
 */
export type FindManyPlaylistTrackArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
  /**
   * Filter, which PlaylistTracks to fetch.
  **/
  where?: PlaylistTrackWhereInput
  /**
   * Determine the order of the PlaylistTracks to fetch.
  **/
  orderBy?: Enumerable<PlaylistTrackOrderByInput> | PlaylistTrackOrderByInput
  /**
   * Sets the position for listing PlaylistTracks.
  **/
  cursor?: PlaylistTrackWhereUniqueInput
  /**
   * The number of PlaylistTracks to fetch. If negative number, it will take PlaylistTracks before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` PlaylistTracks.
  **/
  skip?: number
  distinct?: Enumerable<PlaylistTrackDistinctFieldEnum>
}


/**
 * PlaylistTrack create
 */
export type PlaylistTrackCreateArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
  /**
   * The data needed to create a PlaylistTrack.
  **/
  data: PlaylistTrackCreateInput
}


/**
 * PlaylistTrack update
 */
export type PlaylistTrackUpdateArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
  /**
   * The data needed to update a PlaylistTrack.
  **/
  data: PlaylistTrackUpdateInput
  /**
   * Choose, which PlaylistTrack to update.
  **/
  where: PlaylistTrackWhereUniqueInput
}


/**
 * PlaylistTrack updateMany
 */
export type PlaylistTrackUpdateManyArgs = {
  data: PlaylistTrackUpdateManyMutationInput
  where?: PlaylistTrackWhereInput
}


/**
 * PlaylistTrack upsert
 */
export type PlaylistTrackUpsertArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
  /**
   * The filter to search for the PlaylistTrack to update in case it exists.
  **/
  where: PlaylistTrackWhereUniqueInput
  /**
   * In case the PlaylistTrack found by the `where` argument doesn't exist, create a new PlaylistTrack with this data.
  **/
  create: PlaylistTrackCreateInput
  /**
   * In case the PlaylistTrack was found with the provided `where` argument, update it with this data.
  **/
  update: PlaylistTrackUpdateInput
}


/**
 * PlaylistTrack delete
 */
export type PlaylistTrackDeleteArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
  /**
   * Filter which PlaylistTrack to delete.
  **/
  where: PlaylistTrackWhereUniqueInput
}


/**
 * PlaylistTrack deleteMany
 */
export type PlaylistTrackDeleteManyArgs = {
  where?: PlaylistTrackWhereInput
}


/**
 * PlaylistTrack without action
 */
export type PlaylistTrackArgs = {
  /**
   * Select specific fields to fetch from the PlaylistTrack
  **/
  select?: PlaylistTrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: PlaylistTrackInclude | null
}



/**
 * Model Track
 */

export type Track = {
  TrackId: number
  Name: string
  AlbumId: number | null
  MediaTypeId: number
  GenreId: number | null
  Composer: string | null
  Milliseconds: number
  Bytes: number | null
  UnitPrice: number
}


export type AggregateTrack = {
  count: number
  avg: TrackAvgAggregateOutputType | null
  sum: TrackSumAggregateOutputType | null
  min: TrackMinAggregateOutputType | null
  max: TrackMaxAggregateOutputType | null
}

export type TrackAvgAggregateOutputType = {
  TrackId: number
  AlbumId: number | null
  MediaTypeId: number
  GenreId: number | null
  Milliseconds: number
  Bytes: number | null
  UnitPrice: number
}

export type TrackSumAggregateOutputType = {
  TrackId: number
  AlbumId: number | null
  MediaTypeId: number
  GenreId: number | null
  Milliseconds: number
  Bytes: number | null
  UnitPrice: number
}

export type TrackMinAggregateOutputType = {
  TrackId: number
  AlbumId: number | null
  MediaTypeId: number
  GenreId: number | null
  Milliseconds: number
  Bytes: number | null
  UnitPrice: number
}

export type TrackMaxAggregateOutputType = {
  TrackId: number
  AlbumId: number | null
  MediaTypeId: number
  GenreId: number | null
  Milliseconds: number
  Bytes: number | null
  UnitPrice: number
}


export type TrackAvgAggregateInputType = {
  TrackId?: true
  AlbumId?: true
  MediaTypeId?: true
  GenreId?: true
  Milliseconds?: true
  Bytes?: true
  UnitPrice?: true
}

export type TrackSumAggregateInputType = {
  TrackId?: true
  AlbumId?: true
  MediaTypeId?: true
  GenreId?: true
  Milliseconds?: true
  Bytes?: true
  UnitPrice?: true
}

export type TrackMinAggregateInputType = {
  TrackId?: true
  AlbumId?: true
  MediaTypeId?: true
  GenreId?: true
  Milliseconds?: true
  Bytes?: true
  UnitPrice?: true
}

export type TrackMaxAggregateInputType = {
  TrackId?: true
  AlbumId?: true
  MediaTypeId?: true
  GenreId?: true
  Milliseconds?: true
  Bytes?: true
  UnitPrice?: true
}

export type AggregateTrackArgs = {
  where?: TrackWhereInput
  orderBy?: Enumerable<TrackOrderByInput> | TrackOrderByInput
  cursor?: TrackWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<TrackDistinctFieldEnum>
  count?: true
  avg?: TrackAvgAggregateInputType
  sum?: TrackSumAggregateInputType
  min?: TrackMinAggregateInputType
  max?: TrackMaxAggregateInputType
}

export type GetTrackAggregateType<T extends AggregateTrackArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetTrackAggregateScalarType<T[P]>
}

export type GetTrackAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof TrackAvgAggregateOutputType ? TrackAvgAggregateOutputType[P] : never
}
    
    

export type TrackSelect = {
  TrackId?: boolean
  Name?: boolean
  AlbumId?: boolean
  MediaTypeId?: boolean
  GenreId?: boolean
  Composer?: boolean
  Milliseconds?: boolean
  Bytes?: boolean
  UnitPrice?: boolean
  Album?: boolean | AlbumArgs
  Genre?: boolean | GenreArgs
  MediaType?: boolean | MediaTypeArgs
  InvoiceLine?: boolean | FindManyInvoiceLineArgs
  PlaylistTrack?: boolean | FindManyPlaylistTrackArgs
}

export type TrackInclude = {
  Album?: boolean | AlbumArgs
  Genre?: boolean | GenreArgs
  MediaType?: boolean | MediaTypeArgs
  InvoiceLine?: boolean | FindManyInvoiceLineArgs
  PlaylistTrack?: boolean | FindManyPlaylistTrackArgs
}

export type TrackGetPayload<
  S extends boolean | null | undefined | TrackArgs,
  U = keyof S
> = S extends true
  ? Track
  : S extends undefined
  ? never
  : S extends TrackArgs | FindManyTrackArgs
  ? 'include' extends U
    ? Track  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Album'
      ? AlbumGetPayload<S['include'][P]> | null :
      P extends 'Genre'
      ? GenreGetPayload<S['include'][P]> | null :
      P extends 'MediaType'
      ? MediaTypeGetPayload<S['include'][P]> :
      P extends 'InvoiceLine'
      ? Array<InvoiceLineGetPayload<S['include'][P]>> :
      P extends 'PlaylistTrack'
      ? Array<PlaylistTrackGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Track ? Track[P]
: 
      P extends 'Album'
      ? AlbumGetPayload<S['select'][P]> | null :
      P extends 'Genre'
      ? GenreGetPayload<S['select'][P]> | null :
      P extends 'MediaType'
      ? MediaTypeGetPayload<S['select'][P]> :
      P extends 'InvoiceLine'
      ? Array<InvoiceLineGetPayload<S['select'][P]>> :
      P extends 'PlaylistTrack'
      ? Array<PlaylistTrackGetPayload<S['select'][P]>> : never
    }
  : Track
: Track


export interface TrackDelegate {
  /**
   * Find zero or one Track that matches the filter.
   * @param {FindOneTrackArgs} args - Arguments to find a Track
   * @example
   * // Get one Track
   * const track = await prisma.track.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneTrackArgs>(
    args: Subset<T, FindOneTrackArgs>
  ): CheckSelect<T, Prisma__TrackClient<Track | null>, Prisma__TrackClient<TrackGetPayload<T> | null>>
  /**
   * Find the first Track that matches the filter.
   * @param {FindFirstTrackArgs} args - Arguments to find a Track
   * @example
   * // Get one Track
   * const track = await prisma.track.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstTrackArgs>(
    args?: Subset<T, FindFirstTrackArgs>
  ): CheckSelect<T, Prisma__TrackClient<Track | null>, Prisma__TrackClient<TrackGetPayload<T> | null>>
  /**
   * Find zero or more Tracks that matches the filter.
   * @param {FindManyTrackArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Tracks
   * const tracks = await prisma.track.findMany()
   * 
   * // Get first 10 Tracks
   * const tracks = await prisma.track.findMany({ take: 10 })
   * 
   * // Only select the `TrackId`
   * const trackWithTrackIdOnly = await prisma.track.findMany({ select: { TrackId: true } })
   * 
  **/
  findMany<T extends FindManyTrackArgs>(
    args?: Subset<T, FindManyTrackArgs>
  ): CheckSelect<T, Promise<Array<Track>>, Promise<Array<TrackGetPayload<T>>>>
  /**
   * Create a Track.
   * @param {TrackCreateArgs} args - Arguments to create a Track.
   * @example
   * // Create one Track
   * const Track = await prisma.track.create({
   *   data: {
   *     // ... data to create a Track
   *   }
   * })
   * 
  **/
  create<T extends TrackCreateArgs>(
    args: Subset<T, TrackCreateArgs>
  ): CheckSelect<T, Prisma__TrackClient<Track>, Prisma__TrackClient<TrackGetPayload<T>>>
  /**
   * Delete a Track.
   * @param {TrackDeleteArgs} args - Arguments to delete one Track.
   * @example
   * // Delete one Track
   * const Track = await prisma.track.delete({
   *   where: {
   *     // ... filter to delete one Track
   *   }
   * })
   * 
  **/
  delete<T extends TrackDeleteArgs>(
    args: Subset<T, TrackDeleteArgs>
  ): CheckSelect<T, Prisma__TrackClient<Track>, Prisma__TrackClient<TrackGetPayload<T>>>
  /**
   * Update one Track.
   * @param {TrackUpdateArgs} args - Arguments to update one Track.
   * @example
   * // Update one Track
   * const track = await prisma.track.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends TrackUpdateArgs>(
    args: Subset<T, TrackUpdateArgs>
  ): CheckSelect<T, Prisma__TrackClient<Track>, Prisma__TrackClient<TrackGetPayload<T>>>
  /**
   * Delete zero or more Tracks.
   * @param {TrackDeleteManyArgs} args - Arguments to filter Tracks to delete.
   * @example
   * // Delete a few Tracks
   * const { count } = await prisma.track.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends TrackDeleteManyArgs>(
    args: Subset<T, TrackDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Tracks.
   * @param {TrackUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Tracks
   * const track = await prisma.track.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends TrackUpdateManyArgs>(
    args: Subset<T, TrackUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Track.
   * @param {TrackUpsertArgs} args - Arguments to update or create a Track.
   * @example
   * // Update or create a Track
   * const track = await prisma.track.upsert({
   *   create: {
   *     // ... data to create a Track
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Track we want to update
   *   }
   * })
  **/
  upsert<T extends TrackUpsertArgs>(
    args: Subset<T, TrackUpsertArgs>
  ): CheckSelect<T, Prisma__TrackClient<Track>, Prisma__TrackClient<TrackGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyTrackArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateTrackArgs>(args: Subset<T, AggregateTrackArgs>): Promise<GetTrackAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Track.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__TrackClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Album<T extends AlbumArgs = {}>(args?: Subset<T, AlbumArgs>): CheckSelect<T, Prisma__AlbumClient<Album | null>, Prisma__AlbumClient<AlbumGetPayload<T> | null>>;

  Genre<T extends GenreArgs = {}>(args?: Subset<T, GenreArgs>): CheckSelect<T, Prisma__GenreClient<Genre | null>, Prisma__GenreClient<GenreGetPayload<T> | null>>;

  MediaType<T extends MediaTypeArgs = {}>(args?: Subset<T, MediaTypeArgs>): CheckSelect<T, Prisma__MediaTypeClient<MediaType | null>, Prisma__MediaTypeClient<MediaTypeGetPayload<T> | null>>;

  InvoiceLine<T extends FindManyInvoiceLineArgs = {}>(args?: Subset<T, FindManyInvoiceLineArgs>): CheckSelect<T, Promise<Array<InvoiceLine>>, Promise<Array<InvoiceLineGetPayload<T>>>>;

  PlaylistTrack<T extends FindManyPlaylistTrackArgs = {}>(args?: Subset<T, FindManyPlaylistTrackArgs>): CheckSelect<T, Promise<Array<PlaylistTrack>>, Promise<Array<PlaylistTrackGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Track findOne
 */
export type FindOneTrackArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
  /**
   * Filter, which Track to fetch.
  **/
  where: TrackWhereUniqueInput
}


/**
 * Track findFirst
 */
export type FindFirstTrackArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
  /**
   * Filter, which Track to fetch.
  **/
  where?: TrackWhereInput
  orderBy?: Enumerable<TrackOrderByInput> | TrackOrderByInput
  cursor?: TrackWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<TrackDistinctFieldEnum>
}


/**
 * Track findMany
 */
export type FindManyTrackArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
  /**
   * Filter, which Tracks to fetch.
  **/
  where?: TrackWhereInput
  /**
   * Determine the order of the Tracks to fetch.
  **/
  orderBy?: Enumerable<TrackOrderByInput> | TrackOrderByInput
  /**
   * Sets the position for listing Tracks.
  **/
  cursor?: TrackWhereUniqueInput
  /**
   * The number of Tracks to fetch. If negative number, it will take Tracks before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Tracks.
  **/
  skip?: number
  distinct?: Enumerable<TrackDistinctFieldEnum>
}


/**
 * Track create
 */
export type TrackCreateArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
  /**
   * The data needed to create a Track.
  **/
  data: TrackCreateInput
}


/**
 * Track update
 */
export type TrackUpdateArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
  /**
   * The data needed to update a Track.
  **/
  data: TrackUpdateInput
  /**
   * Choose, which Track to update.
  **/
  where: TrackWhereUniqueInput
}


/**
 * Track updateMany
 */
export type TrackUpdateManyArgs = {
  data: TrackUpdateManyMutationInput
  where?: TrackWhereInput
}


/**
 * Track upsert
 */
export type TrackUpsertArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
  /**
   * The filter to search for the Track to update in case it exists.
  **/
  where: TrackWhereUniqueInput
  /**
   * In case the Track found by the `where` argument doesn't exist, create a new Track with this data.
  **/
  create: TrackCreateInput
  /**
   * In case the Track was found with the provided `where` argument, update it with this data.
  **/
  update: TrackUpdateInput
}


/**
 * Track delete
 */
export type TrackDeleteArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
  /**
   * Filter which Track to delete.
  **/
  where: TrackWhereUniqueInput
}


/**
 * Track deleteMany
 */
export type TrackDeleteManyArgs = {
  where?: TrackWhereInput
}


/**
 * Track without action
 */
export type TrackArgs = {
  /**
   * Select specific fields to fetch from the Track
  **/
  select?: TrackSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: TrackInclude | null
}



/**
 * Deep Input Types
 */


export type AlbumWhereInput = {
  AND?: AlbumWhereInput | Enumerable<AlbumWhereInput>
  OR?: AlbumWhereInput | Enumerable<AlbumWhereInput>
  NOT?: AlbumWhereInput | Enumerable<AlbumWhereInput>
  AlbumId?: IntFilter | number
  Title?: StringFilter | string
  ArtistId?: IntFilter | number
  Artist?: ArtistRelationFilter | ArtistWhereInput
  Track?: TrackListRelationFilter
}

export type AlbumOrderByInput = {
  AlbumId?: SortOrder
  Title?: SortOrder
  ArtistId?: SortOrder
}

export type AlbumWhereUniqueInput = {
  AlbumId?: number
}

export type ArtistWhereInput = {
  AND?: ArtistWhereInput | Enumerable<ArtistWhereInput>
  OR?: ArtistWhereInput | Enumerable<ArtistWhereInput>
  NOT?: ArtistWhereInput | Enumerable<ArtistWhereInput>
  ArtistId?: IntFilter | number
  Name?: StringNullableFilter | string | null
  Album?: AlbumListRelationFilter
}

export type ArtistOrderByInput = {
  ArtistId?: SortOrder
  Name?: SortOrder
}

export type ArtistWhereUniqueInput = {
  ArtistId?: number
}

export type CustomerWhereInput = {
  AND?: CustomerWhereInput | Enumerable<CustomerWhereInput>
  OR?: CustomerWhereInput | Enumerable<CustomerWhereInput>
  NOT?: CustomerWhereInput | Enumerable<CustomerWhereInput>
  CustomerId?: IntFilter | number
  FirstName?: StringFilter | string
  LastName?: StringFilter | string
  Company?: StringNullableFilter | string | null
  Address?: StringNullableFilter | string | null
  City?: StringNullableFilter | string | null
  State?: StringNullableFilter | string | null
  Country?: StringNullableFilter | string | null
  PostalCode?: StringNullableFilter | string | null
  Phone?: StringNullableFilter | string | null
  Fax?: StringNullableFilter | string | null
  Email?: StringFilter | string
  SupportRepId?: IntNullableFilter | number | null
  Employee?: EmployeeRelationFilter | EmployeeWhereInput | null
  Invoice?: InvoiceListRelationFilter
}

export type CustomerOrderByInput = {
  CustomerId?: SortOrder
  FirstName?: SortOrder
  LastName?: SortOrder
  Company?: SortOrder
  Address?: SortOrder
  City?: SortOrder
  State?: SortOrder
  Country?: SortOrder
  PostalCode?: SortOrder
  Phone?: SortOrder
  Fax?: SortOrder
  Email?: SortOrder
  SupportRepId?: SortOrder
}

export type CustomerWhereUniqueInput = {
  CustomerId?: number
}

export type EmployeeWhereInput = {
  AND?: EmployeeWhereInput | Enumerable<EmployeeWhereInput>
  OR?: EmployeeWhereInput | Enumerable<EmployeeWhereInput>
  NOT?: EmployeeWhereInput | Enumerable<EmployeeWhereInput>
  EmployeeId?: IntFilter | number
  LastName?: StringFilter | string
  FirstName?: StringFilter | string
  Title?: StringNullableFilter | string | null
  ReportsTo?: IntNullableFilter | number | null
  BirthDate?: DateTimeNullableFilter | Date | string | null
  HireDate?: DateTimeNullableFilter | Date | string | null
  Address?: StringNullableFilter | string | null
  City?: StringNullableFilter | string | null
  State?: StringNullableFilter | string | null
  Country?: StringNullableFilter | string | null
  PostalCode?: StringNullableFilter | string | null
  Phone?: StringNullableFilter | string | null
  Fax?: StringNullableFilter | string | null
  Email?: StringNullableFilter | string | null
  Employee?: EmployeeRelationFilter | EmployeeWhereInput | null
  Customer?: CustomerListRelationFilter
  other_Employee?: EmployeeListRelationFilter
}

export type EmployeeOrderByInput = {
  EmployeeId?: SortOrder
  LastName?: SortOrder
  FirstName?: SortOrder
  Title?: SortOrder
  ReportsTo?: SortOrder
  BirthDate?: SortOrder
  HireDate?: SortOrder
  Address?: SortOrder
  City?: SortOrder
  State?: SortOrder
  Country?: SortOrder
  PostalCode?: SortOrder
  Phone?: SortOrder
  Fax?: SortOrder
  Email?: SortOrder
}

export type EmployeeWhereUniqueInput = {
  EmployeeId?: number
}

export type GenreWhereInput = {
  AND?: GenreWhereInput | Enumerable<GenreWhereInput>
  OR?: GenreWhereInput | Enumerable<GenreWhereInput>
  NOT?: GenreWhereInput | Enumerable<GenreWhereInput>
  GenreId?: IntFilter | number
  Name?: StringNullableFilter | string | null
  Track?: TrackListRelationFilter
}

export type GenreOrderByInput = {
  GenreId?: SortOrder
  Name?: SortOrder
}

export type GenreWhereUniqueInput = {
  GenreId?: number
}

export type InvoiceWhereInput = {
  AND?: InvoiceWhereInput | Enumerable<InvoiceWhereInput>
  OR?: InvoiceWhereInput | Enumerable<InvoiceWhereInput>
  NOT?: InvoiceWhereInput | Enumerable<InvoiceWhereInput>
  InvoiceId?: IntFilter | number
  CustomerId?: IntFilter | number
  InvoiceDate?: DateTimeFilter | Date | string
  BillingAddress?: StringNullableFilter | string | null
  BillingCity?: StringNullableFilter | string | null
  BillingState?: StringNullableFilter | string | null
  BillingCountry?: StringNullableFilter | string | null
  BillingPostalCode?: StringNullableFilter | string | null
  Total?: FloatFilter | number
  Customer?: CustomerRelationFilter | CustomerWhereInput
  InvoiceLine?: InvoiceLineListRelationFilter
}

export type InvoiceOrderByInput = {
  InvoiceId?: SortOrder
  CustomerId?: SortOrder
  InvoiceDate?: SortOrder
  BillingAddress?: SortOrder
  BillingCity?: SortOrder
  BillingState?: SortOrder
  BillingCountry?: SortOrder
  BillingPostalCode?: SortOrder
  Total?: SortOrder
}

export type InvoiceWhereUniqueInput = {
  InvoiceId?: number
}

export type InvoiceLineWhereInput = {
  AND?: InvoiceLineWhereInput | Enumerable<InvoiceLineWhereInput>
  OR?: InvoiceLineWhereInput | Enumerable<InvoiceLineWhereInput>
  NOT?: InvoiceLineWhereInput | Enumerable<InvoiceLineWhereInput>
  InvoiceLineId?: IntFilter | number
  InvoiceId?: IntFilter | number
  TrackId?: IntFilter | number
  UnitPrice?: FloatFilter | number
  Quantity?: IntFilter | number
  Invoice?: InvoiceRelationFilter | InvoiceWhereInput
  Track?: TrackRelationFilter | TrackWhereInput
}

export type InvoiceLineOrderByInput = {
  InvoiceLineId?: SortOrder
  InvoiceId?: SortOrder
  TrackId?: SortOrder
  UnitPrice?: SortOrder
  Quantity?: SortOrder
}

export type InvoiceLineWhereUniqueInput = {
  InvoiceLineId?: number
}

export type MediaTypeWhereInput = {
  AND?: MediaTypeWhereInput | Enumerable<MediaTypeWhereInput>
  OR?: MediaTypeWhereInput | Enumerable<MediaTypeWhereInput>
  NOT?: MediaTypeWhereInput | Enumerable<MediaTypeWhereInput>
  MediaTypeId?: IntFilter | number
  Name?: StringNullableFilter | string | null
  Track?: TrackListRelationFilter
}

export type MediaTypeOrderByInput = {
  MediaTypeId?: SortOrder
  Name?: SortOrder
}

export type MediaTypeWhereUniqueInput = {
  MediaTypeId?: number
}

export type PlaylistWhereInput = {
  AND?: PlaylistWhereInput | Enumerable<PlaylistWhereInput>
  OR?: PlaylistWhereInput | Enumerable<PlaylistWhereInput>
  NOT?: PlaylistWhereInput | Enumerable<PlaylistWhereInput>
  PlaylistId?: IntFilter | number
  Name?: StringNullableFilter | string | null
  PlaylistTrack?: PlaylistTrackListRelationFilter
}

export type PlaylistOrderByInput = {
  PlaylistId?: SortOrder
  Name?: SortOrder
}

export type PlaylistWhereUniqueInput = {
  PlaylistId?: number
}

export type PlaylistTrackWhereInput = {
  AND?: PlaylistTrackWhereInput | Enumerable<PlaylistTrackWhereInput>
  OR?: PlaylistTrackWhereInput | Enumerable<PlaylistTrackWhereInput>
  NOT?: PlaylistTrackWhereInput | Enumerable<PlaylistTrackWhereInput>
  PlaylistId?: IntFilter | number
  TrackId?: IntFilter | number
  Playlist?: PlaylistRelationFilter | PlaylistWhereInput
  Track?: TrackRelationFilter | TrackWhereInput
}

export type PlaylistTrackOrderByInput = {
  PlaylistId?: SortOrder
  TrackId?: SortOrder
}

export type PlaylistTrackWhereUniqueInput = {
  PlaylistId_TrackId?: PlaylistIdTrackIdCompoundUniqueInput
}

export type TrackWhereInput = {
  AND?: TrackWhereInput | Enumerable<TrackWhereInput>
  OR?: TrackWhereInput | Enumerable<TrackWhereInput>
  NOT?: TrackWhereInput | Enumerable<TrackWhereInput>
  TrackId?: IntFilter | number
  Name?: StringFilter | string
  AlbumId?: IntNullableFilter | number | null
  MediaTypeId?: IntFilter | number
  GenreId?: IntNullableFilter | number | null
  Composer?: StringNullableFilter | string | null
  Milliseconds?: IntFilter | number
  Bytes?: IntNullableFilter | number | null
  UnitPrice?: FloatFilter | number
  Album?: AlbumRelationFilter | AlbumWhereInput | null
  Genre?: GenreRelationFilter | GenreWhereInput | null
  MediaType?: MediaTypeRelationFilter | MediaTypeWhereInput
  InvoiceLine?: InvoiceLineListRelationFilter
  PlaylistTrack?: PlaylistTrackListRelationFilter
}

export type TrackOrderByInput = {
  TrackId?: SortOrder
  Name?: SortOrder
  AlbumId?: SortOrder
  MediaTypeId?: SortOrder
  GenreId?: SortOrder
  Composer?: SortOrder
  Milliseconds?: SortOrder
  Bytes?: SortOrder
  UnitPrice?: SortOrder
}

export type TrackWhereUniqueInput = {
  TrackId?: number
}

export type AlbumCreateInput = {
  Title: string
  Artist: ArtistCreateOneWithoutAlbumInput
  Track?: TrackCreateManyWithoutAlbumInput
}

export type AlbumUpdateInput = {
  Title?: string | StringFieldUpdateOperationsInput
  Artist?: ArtistUpdateOneRequiredWithoutAlbumInput
  Track?: TrackUpdateManyWithoutAlbumInput
}

export type AlbumUpdateManyMutationInput = {
  Title?: string | StringFieldUpdateOperationsInput
}

export type ArtistCreateInput = {
  Name?: string | null
  Album?: AlbumCreateManyWithoutArtistInput
}

export type ArtistUpdateInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
  Album?: AlbumUpdateManyWithoutArtistInput
}

export type ArtistUpdateManyMutationInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type CustomerCreateInput = {
  FirstName: string
  LastName: string
  Company?: string | null
  Address?: string | null
  City?: string | null
  State?: string | null
  Country?: string | null
  PostalCode?: string | null
  Phone?: string | null
  Fax?: string | null
  Email: string
  Employee?: EmployeeCreateOneWithoutCustomerInput
  Invoice?: InvoiceCreateManyWithoutCustomerInput
}

export type CustomerUpdateInput = {
  FirstName?: string | StringFieldUpdateOperationsInput
  LastName?: string | StringFieldUpdateOperationsInput
  Company?: string | NullableStringFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | StringFieldUpdateOperationsInput
  Employee?: EmployeeUpdateOneWithoutCustomerInput
  Invoice?: InvoiceUpdateManyWithoutCustomerInput
}

export type CustomerUpdateManyMutationInput = {
  FirstName?: string | StringFieldUpdateOperationsInput
  LastName?: string | StringFieldUpdateOperationsInput
  Company?: string | NullableStringFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | StringFieldUpdateOperationsInput
}

export type EmployeeCreateInput = {
  LastName: string
  FirstName: string
  Title?: string | null
  BirthDate?: Date | string | null
  HireDate?: Date | string | null
  Address?: string | null
  City?: string | null
  State?: string | null
  Country?: string | null
  PostalCode?: string | null
  Phone?: string | null
  Fax?: string | null
  Email?: string | null
  Employee?: EmployeeCreateOneWithoutOther_EmployeeInput
  Customer?: CustomerCreateManyWithoutEmployeeInput
  other_Employee?: EmployeeCreateManyWithoutEmployeeInput
}

export type EmployeeUpdateInput = {
  LastName?: string | StringFieldUpdateOperationsInput
  FirstName?: string | StringFieldUpdateOperationsInput
  Title?: string | NullableStringFieldUpdateOperationsInput | null
  BirthDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  HireDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | NullableStringFieldUpdateOperationsInput | null
  Employee?: EmployeeUpdateOneWithoutOther_EmployeeInput
  Customer?: CustomerUpdateManyWithoutEmployeeInput
  other_Employee?: EmployeeUpdateManyWithoutEmployeeInput
}

export type EmployeeUpdateManyMutationInput = {
  LastName?: string | StringFieldUpdateOperationsInput
  FirstName?: string | StringFieldUpdateOperationsInput
  Title?: string | NullableStringFieldUpdateOperationsInput | null
  BirthDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  HireDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | NullableStringFieldUpdateOperationsInput | null
}

export type GenreCreateInput = {
  Name?: string | null
  Track?: TrackCreateManyWithoutGenreInput
}

export type GenreUpdateInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
  Track?: TrackUpdateManyWithoutGenreInput
}

export type GenreUpdateManyMutationInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type InvoiceCreateInput = {
  InvoiceDate: Date | string
  BillingAddress?: string | null
  BillingCity?: string | null
  BillingState?: string | null
  BillingCountry?: string | null
  BillingPostalCode?: string | null
  Total: number
  Customer: CustomerCreateOneWithoutInvoiceInput
  InvoiceLine?: InvoiceLineCreateManyWithoutInvoiceInput
}

export type InvoiceUpdateInput = {
  InvoiceDate?: Date | string | DateTimeFieldUpdateOperationsInput
  BillingAddress?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCity?: string | NullableStringFieldUpdateOperationsInput | null
  BillingState?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCountry?: string | NullableStringFieldUpdateOperationsInput | null
  BillingPostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Total?: number | FloatFieldUpdateOperationsInput
  Customer?: CustomerUpdateOneRequiredWithoutInvoiceInput
  InvoiceLine?: InvoiceLineUpdateManyWithoutInvoiceInput
}

export type InvoiceUpdateManyMutationInput = {
  InvoiceDate?: Date | string | DateTimeFieldUpdateOperationsInput
  BillingAddress?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCity?: string | NullableStringFieldUpdateOperationsInput | null
  BillingState?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCountry?: string | NullableStringFieldUpdateOperationsInput | null
  BillingPostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Total?: number | FloatFieldUpdateOperationsInput
}

export type InvoiceLineCreateInput = {
  UnitPrice: number
  Quantity: number
  Invoice: InvoiceCreateOneWithoutInvoiceLineInput
  Track: TrackCreateOneWithoutInvoiceLineInput
}

export type InvoiceLineUpdateInput = {
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Quantity?: number | IntFieldUpdateOperationsInput
  Invoice?: InvoiceUpdateOneRequiredWithoutInvoiceLineInput
  Track?: TrackUpdateOneRequiredWithoutInvoiceLineInput
}

export type InvoiceLineUpdateManyMutationInput = {
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Quantity?: number | IntFieldUpdateOperationsInput
}

export type MediaTypeCreateInput = {
  Name?: string | null
  Track?: TrackCreateManyWithoutMediaTypeInput
}

export type MediaTypeUpdateInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
  Track?: TrackUpdateManyWithoutMediaTypeInput
}

export type MediaTypeUpdateManyMutationInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type PlaylistCreateInput = {
  Name?: string | null
  PlaylistTrack?: PlaylistTrackCreateManyWithoutPlaylistInput
}

export type PlaylistUpdateInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
  PlaylistTrack?: PlaylistTrackUpdateManyWithoutPlaylistInput
}

export type PlaylistUpdateManyMutationInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type PlaylistTrackCreateInput = {
  Playlist: PlaylistCreateOneWithoutPlaylistTrackInput
  Track: TrackCreateOneWithoutPlaylistTrackInput
}

export type PlaylistTrackUpdateInput = {
  Playlist?: PlaylistUpdateOneRequiredWithoutPlaylistTrackInput
  Track?: TrackUpdateOneRequiredWithoutPlaylistTrackInput
}

export type PlaylistTrackUpdateManyMutationInput = {

}

export type TrackCreateInput = {
  Name: string
  Composer?: string | null
  Milliseconds: number
  Bytes?: number | null
  UnitPrice: number
  Album?: AlbumCreateOneWithoutTrackInput
  Genre?: GenreCreateOneWithoutTrackInput
  MediaType: MediaTypeCreateOneWithoutTrackInput
  InvoiceLine?: InvoiceLineCreateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackCreateManyWithoutTrackInput
}

export type TrackUpdateInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Album?: AlbumUpdateOneWithoutTrackInput
  Genre?: GenreUpdateOneWithoutTrackInput
  MediaType?: MediaTypeUpdateOneRequiredWithoutTrackInput
  InvoiceLine?: InvoiceLineUpdateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackUpdateManyWithoutTrackInput
}

export type TrackUpdateManyMutationInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
}

export type IntFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntFilter
}

export type StringFilter = {
  equals?: string
  in?: Enumerable<string>
  notIn?: Enumerable<string>
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: string | NestedStringFilter
}

export type ArtistRelationFilter = {
  is?: ArtistWhereInput
  isNot?: ArtistWhereInput
}

export type TrackListRelationFilter = {
  every?: TrackWhereInput
  some?: TrackWhereInput
  none?: TrackWhereInput
}

export type StringNullableFilter = {
  equals?: string | null
  in?: Enumerable<string> | null
  notIn?: Enumerable<string> | null
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: string | NestedStringNullableFilter | null
}

export type AlbumListRelationFilter = {
  every?: AlbumWhereInput
  some?: AlbumWhereInput
  none?: AlbumWhereInput
}

export type IntNullableFilter = {
  equals?: number | null
  in?: Enumerable<number> | null
  notIn?: Enumerable<number> | null
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntNullableFilter | null
}

export type EmployeeRelationFilter = {
  is?: EmployeeWhereInput | null
  isNot?: EmployeeWhereInput | null
}

export type InvoiceListRelationFilter = {
  every?: InvoiceWhereInput
  some?: InvoiceWhereInput
  none?: InvoiceWhereInput
}

export type DateTimeNullableFilter = {
  equals?: Date | string | null
  in?: Enumerable<Date> | Enumerable<string> | null
  notIn?: Enumerable<Date> | Enumerable<string> | null
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeNullableFilter | null
}

export type CustomerListRelationFilter = {
  every?: CustomerWhereInput
  some?: CustomerWhereInput
  none?: CustomerWhereInput
}

export type EmployeeListRelationFilter = {
  every?: EmployeeWhereInput
  some?: EmployeeWhereInput
  none?: EmployeeWhereInput
}

export type DateTimeFilter = {
  equals?: Date | string
  in?: Enumerable<Date> | Enumerable<string>
  notIn?: Enumerable<Date> | Enumerable<string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeFilter
}

export type FloatFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedFloatFilter
}

export type CustomerRelationFilter = {
  is?: CustomerWhereInput
  isNot?: CustomerWhereInput
}

export type InvoiceLineListRelationFilter = {
  every?: InvoiceLineWhereInput
  some?: InvoiceLineWhereInput
  none?: InvoiceLineWhereInput
}

export type InvoiceRelationFilter = {
  is?: InvoiceWhereInput
  isNot?: InvoiceWhereInput
}

export type TrackRelationFilter = {
  is?: TrackWhereInput
  isNot?: TrackWhereInput
}

export type PlaylistTrackListRelationFilter = {
  every?: PlaylistTrackWhereInput
  some?: PlaylistTrackWhereInput
  none?: PlaylistTrackWhereInput
}

export type PlaylistRelationFilter = {
  is?: PlaylistWhereInput
  isNot?: PlaylistWhereInput
}

export type PlaylistIdTrackIdCompoundUniqueInput = {
  PlaylistId: number
  TrackId: number
}

export type AlbumRelationFilter = {
  is?: AlbumWhereInput | null
  isNot?: AlbumWhereInput | null
}

export type GenreRelationFilter = {
  is?: GenreWhereInput | null
  isNot?: GenreWhereInput | null
}

export type MediaTypeRelationFilter = {
  is?: MediaTypeWhereInput
  isNot?: MediaTypeWhereInput
}

export type ArtistCreateOneWithoutAlbumInput = {
  create?: ArtistCreateWithoutAlbumInput
  connect?: ArtistWhereUniqueInput
}

export type TrackCreateManyWithoutAlbumInput = {
  create?: TrackCreateWithoutAlbumInput | Enumerable<TrackCreateWithoutAlbumInput>
  connect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
}

export type StringFieldUpdateOperationsInput = {
  set?: string
}

export type ArtistUpdateOneRequiredWithoutAlbumInput = {
  create?: ArtistCreateWithoutAlbumInput
  connect?: ArtistWhereUniqueInput
  update?: ArtistUpdateWithoutAlbumDataInput
  upsert?: ArtistUpsertWithoutAlbumInput
}

export type TrackUpdateManyWithoutAlbumInput = {
  create?: TrackCreateWithoutAlbumInput | Enumerable<TrackCreateWithoutAlbumInput>
  connect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  set?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  disconnect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  delete?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  update?: TrackUpdateWithWhereUniqueWithoutAlbumInput | Enumerable<TrackUpdateWithWhereUniqueWithoutAlbumInput>
  updateMany?: TrackUpdateManyWithWhereNestedInput | Enumerable<TrackUpdateManyWithWhereNestedInput>
  deleteMany?: TrackScalarWhereInput | Enumerable<TrackScalarWhereInput>
  upsert?: TrackUpsertWithWhereUniqueWithoutAlbumInput | Enumerable<TrackUpsertWithWhereUniqueWithoutAlbumInput>
}

export type AlbumCreateManyWithoutArtistInput = {
  create?: AlbumCreateWithoutArtistInput | Enumerable<AlbumCreateWithoutArtistInput>
  connect?: AlbumWhereUniqueInput | Enumerable<AlbumWhereUniqueInput>
}

export type NullableStringFieldUpdateOperationsInput = {
  set?: string | null
}

export type AlbumUpdateManyWithoutArtistInput = {
  create?: AlbumCreateWithoutArtistInput | Enumerable<AlbumCreateWithoutArtistInput>
  connect?: AlbumWhereUniqueInput | Enumerable<AlbumWhereUniqueInput>
  set?: AlbumWhereUniqueInput | Enumerable<AlbumWhereUniqueInput>
  disconnect?: AlbumWhereUniqueInput | Enumerable<AlbumWhereUniqueInput>
  delete?: AlbumWhereUniqueInput | Enumerable<AlbumWhereUniqueInput>
  update?: AlbumUpdateWithWhereUniqueWithoutArtistInput | Enumerable<AlbumUpdateWithWhereUniqueWithoutArtistInput>
  updateMany?: AlbumUpdateManyWithWhereNestedInput | Enumerable<AlbumUpdateManyWithWhereNestedInput>
  deleteMany?: AlbumScalarWhereInput | Enumerable<AlbumScalarWhereInput>
  upsert?: AlbumUpsertWithWhereUniqueWithoutArtistInput | Enumerable<AlbumUpsertWithWhereUniqueWithoutArtistInput>
}

export type EmployeeCreateOneWithoutCustomerInput = {
  create?: EmployeeCreateWithoutCustomerInput
  connect?: EmployeeWhereUniqueInput
}

export type InvoiceCreateManyWithoutCustomerInput = {
  create?: InvoiceCreateWithoutCustomerInput | Enumerable<InvoiceCreateWithoutCustomerInput>
  connect?: InvoiceWhereUniqueInput | Enumerable<InvoiceWhereUniqueInput>
}

export type EmployeeUpdateOneWithoutCustomerInput = {
  create?: EmployeeCreateWithoutCustomerInput
  connect?: EmployeeWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: EmployeeUpdateWithoutCustomerDataInput
  upsert?: EmployeeUpsertWithoutCustomerInput
}

export type InvoiceUpdateManyWithoutCustomerInput = {
  create?: InvoiceCreateWithoutCustomerInput | Enumerable<InvoiceCreateWithoutCustomerInput>
  connect?: InvoiceWhereUniqueInput | Enumerable<InvoiceWhereUniqueInput>
  set?: InvoiceWhereUniqueInput | Enumerable<InvoiceWhereUniqueInput>
  disconnect?: InvoiceWhereUniqueInput | Enumerable<InvoiceWhereUniqueInput>
  delete?: InvoiceWhereUniqueInput | Enumerable<InvoiceWhereUniqueInput>
  update?: InvoiceUpdateWithWhereUniqueWithoutCustomerInput | Enumerable<InvoiceUpdateWithWhereUniqueWithoutCustomerInput>
  updateMany?: InvoiceUpdateManyWithWhereNestedInput | Enumerable<InvoiceUpdateManyWithWhereNestedInput>
  deleteMany?: InvoiceScalarWhereInput | Enumerable<InvoiceScalarWhereInput>
  upsert?: InvoiceUpsertWithWhereUniqueWithoutCustomerInput | Enumerable<InvoiceUpsertWithWhereUniqueWithoutCustomerInput>
}

export type EmployeeCreateOneWithoutOther_EmployeeInput = {
  create?: EmployeeCreateWithoutOther_EmployeeInput
  connect?: EmployeeWhereUniqueInput
}

export type CustomerCreateManyWithoutEmployeeInput = {
  create?: CustomerCreateWithoutEmployeeInput | Enumerable<CustomerCreateWithoutEmployeeInput>
  connect?: CustomerWhereUniqueInput | Enumerable<CustomerWhereUniqueInput>
}

export type EmployeeCreateManyWithoutEmployeeInput = {
  create?: EmployeeCreateWithoutEmployeeInput | Enumerable<EmployeeCreateWithoutEmployeeInput>
  connect?: EmployeeWhereUniqueInput | Enumerable<EmployeeWhereUniqueInput>
}

export type NullableDateTimeFieldUpdateOperationsInput = {
  set?: Date | string | null
}

export type EmployeeUpdateOneWithoutOther_EmployeeInput = {
  create?: EmployeeCreateWithoutOther_EmployeeInput
  connect?: EmployeeWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: EmployeeUpdateWithoutOther_EmployeeDataInput
  upsert?: EmployeeUpsertWithoutOther_EmployeeInput
}

export type CustomerUpdateManyWithoutEmployeeInput = {
  create?: CustomerCreateWithoutEmployeeInput | Enumerable<CustomerCreateWithoutEmployeeInput>
  connect?: CustomerWhereUniqueInput | Enumerable<CustomerWhereUniqueInput>
  set?: CustomerWhereUniqueInput | Enumerable<CustomerWhereUniqueInput>
  disconnect?: CustomerWhereUniqueInput | Enumerable<CustomerWhereUniqueInput>
  delete?: CustomerWhereUniqueInput | Enumerable<CustomerWhereUniqueInput>
  update?: CustomerUpdateWithWhereUniqueWithoutEmployeeInput | Enumerable<CustomerUpdateWithWhereUniqueWithoutEmployeeInput>
  updateMany?: CustomerUpdateManyWithWhereNestedInput | Enumerable<CustomerUpdateManyWithWhereNestedInput>
  deleteMany?: CustomerScalarWhereInput | Enumerable<CustomerScalarWhereInput>
  upsert?: CustomerUpsertWithWhereUniqueWithoutEmployeeInput | Enumerable<CustomerUpsertWithWhereUniqueWithoutEmployeeInput>
}

export type EmployeeUpdateManyWithoutEmployeeInput = {
  create?: EmployeeCreateWithoutEmployeeInput | Enumerable<EmployeeCreateWithoutEmployeeInput>
  connect?: EmployeeWhereUniqueInput | Enumerable<EmployeeWhereUniqueInput>
  set?: EmployeeWhereUniqueInput | Enumerable<EmployeeWhereUniqueInput>
  disconnect?: EmployeeWhereUniqueInput | Enumerable<EmployeeWhereUniqueInput>
  delete?: EmployeeWhereUniqueInput | Enumerable<EmployeeWhereUniqueInput>
  update?: EmployeeUpdateWithWhereUniqueWithoutEmployeeInput | Enumerable<EmployeeUpdateWithWhereUniqueWithoutEmployeeInput>
  updateMany?: EmployeeUpdateManyWithWhereNestedInput | Enumerable<EmployeeUpdateManyWithWhereNestedInput>
  deleteMany?: EmployeeScalarWhereInput | Enumerable<EmployeeScalarWhereInput>
  upsert?: EmployeeUpsertWithWhereUniqueWithoutEmployeeInput | Enumerable<EmployeeUpsertWithWhereUniqueWithoutEmployeeInput>
}

export type TrackCreateManyWithoutGenreInput = {
  create?: TrackCreateWithoutGenreInput | Enumerable<TrackCreateWithoutGenreInput>
  connect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
}

export type TrackUpdateManyWithoutGenreInput = {
  create?: TrackCreateWithoutGenreInput | Enumerable<TrackCreateWithoutGenreInput>
  connect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  set?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  disconnect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  delete?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  update?: TrackUpdateWithWhereUniqueWithoutGenreInput | Enumerable<TrackUpdateWithWhereUniqueWithoutGenreInput>
  updateMany?: TrackUpdateManyWithWhereNestedInput | Enumerable<TrackUpdateManyWithWhereNestedInput>
  deleteMany?: TrackScalarWhereInput | Enumerable<TrackScalarWhereInput>
  upsert?: TrackUpsertWithWhereUniqueWithoutGenreInput | Enumerable<TrackUpsertWithWhereUniqueWithoutGenreInput>
}

export type CustomerCreateOneWithoutInvoiceInput = {
  create?: CustomerCreateWithoutInvoiceInput
  connect?: CustomerWhereUniqueInput
}

export type InvoiceLineCreateManyWithoutInvoiceInput = {
  create?: InvoiceLineCreateWithoutInvoiceInput | Enumerable<InvoiceLineCreateWithoutInvoiceInput>
  connect?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
}

export type DateTimeFieldUpdateOperationsInput = {
  set?: Date | string
}

export type FloatFieldUpdateOperationsInput = {
  set?: number
  increment?: number
  decrement?: number
  multiply?: number
  divide?: number
}

export type CustomerUpdateOneRequiredWithoutInvoiceInput = {
  create?: CustomerCreateWithoutInvoiceInput
  connect?: CustomerWhereUniqueInput
  update?: CustomerUpdateWithoutInvoiceDataInput
  upsert?: CustomerUpsertWithoutInvoiceInput
}

export type InvoiceLineUpdateManyWithoutInvoiceInput = {
  create?: InvoiceLineCreateWithoutInvoiceInput | Enumerable<InvoiceLineCreateWithoutInvoiceInput>
  connect?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  set?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  disconnect?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  delete?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  update?: InvoiceLineUpdateWithWhereUniqueWithoutInvoiceInput | Enumerable<InvoiceLineUpdateWithWhereUniqueWithoutInvoiceInput>
  updateMany?: InvoiceLineUpdateManyWithWhereNestedInput | Enumerable<InvoiceLineUpdateManyWithWhereNestedInput>
  deleteMany?: InvoiceLineScalarWhereInput | Enumerable<InvoiceLineScalarWhereInput>
  upsert?: InvoiceLineUpsertWithWhereUniqueWithoutInvoiceInput | Enumerable<InvoiceLineUpsertWithWhereUniqueWithoutInvoiceInput>
}

export type InvoiceCreateOneWithoutInvoiceLineInput = {
  create?: InvoiceCreateWithoutInvoiceLineInput
  connect?: InvoiceWhereUniqueInput
}

export type TrackCreateOneWithoutInvoiceLineInput = {
  create?: TrackCreateWithoutInvoiceLineInput
  connect?: TrackWhereUniqueInput
}

export type IntFieldUpdateOperationsInput = {
  set?: number
  increment?: number
  decrement?: number
  multiply?: number
  divide?: number
}

export type InvoiceUpdateOneRequiredWithoutInvoiceLineInput = {
  create?: InvoiceCreateWithoutInvoiceLineInput
  connect?: InvoiceWhereUniqueInput
  update?: InvoiceUpdateWithoutInvoiceLineDataInput
  upsert?: InvoiceUpsertWithoutInvoiceLineInput
}

export type TrackUpdateOneRequiredWithoutInvoiceLineInput = {
  create?: TrackCreateWithoutInvoiceLineInput
  connect?: TrackWhereUniqueInput
  update?: TrackUpdateWithoutInvoiceLineDataInput
  upsert?: TrackUpsertWithoutInvoiceLineInput
}

export type TrackCreateManyWithoutMediaTypeInput = {
  create?: TrackCreateWithoutMediaTypeInput | Enumerable<TrackCreateWithoutMediaTypeInput>
  connect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
}

export type TrackUpdateManyWithoutMediaTypeInput = {
  create?: TrackCreateWithoutMediaTypeInput | Enumerable<TrackCreateWithoutMediaTypeInput>
  connect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  set?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  disconnect?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  delete?: TrackWhereUniqueInput | Enumerable<TrackWhereUniqueInput>
  update?: TrackUpdateWithWhereUniqueWithoutMediaTypeInput | Enumerable<TrackUpdateWithWhereUniqueWithoutMediaTypeInput>
  updateMany?: TrackUpdateManyWithWhereNestedInput | Enumerable<TrackUpdateManyWithWhereNestedInput>
  deleteMany?: TrackScalarWhereInput | Enumerable<TrackScalarWhereInput>
  upsert?: TrackUpsertWithWhereUniqueWithoutMediaTypeInput | Enumerable<TrackUpsertWithWhereUniqueWithoutMediaTypeInput>
}

export type PlaylistTrackCreateManyWithoutPlaylistInput = {
  create?: PlaylistTrackCreateWithoutPlaylistInput | Enumerable<PlaylistTrackCreateWithoutPlaylistInput>
  connect?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
}

export type PlaylistTrackUpdateManyWithoutPlaylistInput = {
  create?: PlaylistTrackCreateWithoutPlaylistInput | Enumerable<PlaylistTrackCreateWithoutPlaylistInput>
  connect?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  set?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  disconnect?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  delete?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  update?: PlaylistTrackUpdateWithWhereUniqueWithoutPlaylistInput | Enumerable<PlaylistTrackUpdateWithWhereUniqueWithoutPlaylistInput>
  updateMany?: PlaylistTrackUpdateManyWithWhereNestedInput | Enumerable<PlaylistTrackUpdateManyWithWhereNestedInput>
  deleteMany?: PlaylistTrackScalarWhereInput | Enumerable<PlaylistTrackScalarWhereInput>
  upsert?: PlaylistTrackUpsertWithWhereUniqueWithoutPlaylistInput | Enumerable<PlaylistTrackUpsertWithWhereUniqueWithoutPlaylistInput>
}

export type PlaylistCreateOneWithoutPlaylistTrackInput = {
  create?: PlaylistCreateWithoutPlaylistTrackInput
  connect?: PlaylistWhereUniqueInput
}

export type TrackCreateOneWithoutPlaylistTrackInput = {
  create?: TrackCreateWithoutPlaylistTrackInput
  connect?: TrackWhereUniqueInput
}

export type PlaylistUpdateOneRequiredWithoutPlaylistTrackInput = {
  create?: PlaylistCreateWithoutPlaylistTrackInput
  connect?: PlaylistWhereUniqueInput
  update?: PlaylistUpdateWithoutPlaylistTrackDataInput
  upsert?: PlaylistUpsertWithoutPlaylistTrackInput
}

export type TrackUpdateOneRequiredWithoutPlaylistTrackInput = {
  create?: TrackCreateWithoutPlaylistTrackInput
  connect?: TrackWhereUniqueInput
  update?: TrackUpdateWithoutPlaylistTrackDataInput
  upsert?: TrackUpsertWithoutPlaylistTrackInput
}

export type AlbumCreateOneWithoutTrackInput = {
  create?: AlbumCreateWithoutTrackInput
  connect?: AlbumWhereUniqueInput
}

export type GenreCreateOneWithoutTrackInput = {
  create?: GenreCreateWithoutTrackInput
  connect?: GenreWhereUniqueInput
}

export type MediaTypeCreateOneWithoutTrackInput = {
  create?: MediaTypeCreateWithoutTrackInput
  connect?: MediaTypeWhereUniqueInput
}

export type InvoiceLineCreateManyWithoutTrackInput = {
  create?: InvoiceLineCreateWithoutTrackInput | Enumerable<InvoiceLineCreateWithoutTrackInput>
  connect?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
}

export type PlaylistTrackCreateManyWithoutTrackInput = {
  create?: PlaylistTrackCreateWithoutTrackInput | Enumerable<PlaylistTrackCreateWithoutTrackInput>
  connect?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
}

export type NullableIntFieldUpdateOperationsInput = {
  set?: number | null
  increment?: number
  decrement?: number
  multiply?: number
  divide?: number
}

export type AlbumUpdateOneWithoutTrackInput = {
  create?: AlbumCreateWithoutTrackInput
  connect?: AlbumWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: AlbumUpdateWithoutTrackDataInput
  upsert?: AlbumUpsertWithoutTrackInput
}

export type GenreUpdateOneWithoutTrackInput = {
  create?: GenreCreateWithoutTrackInput
  connect?: GenreWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: GenreUpdateWithoutTrackDataInput
  upsert?: GenreUpsertWithoutTrackInput
}

export type MediaTypeUpdateOneRequiredWithoutTrackInput = {
  create?: MediaTypeCreateWithoutTrackInput
  connect?: MediaTypeWhereUniqueInput
  update?: MediaTypeUpdateWithoutTrackDataInput
  upsert?: MediaTypeUpsertWithoutTrackInput
}

export type InvoiceLineUpdateManyWithoutTrackInput = {
  create?: InvoiceLineCreateWithoutTrackInput | Enumerable<InvoiceLineCreateWithoutTrackInput>
  connect?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  set?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  disconnect?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  delete?: InvoiceLineWhereUniqueInput | Enumerable<InvoiceLineWhereUniqueInput>
  update?: InvoiceLineUpdateWithWhereUniqueWithoutTrackInput | Enumerable<InvoiceLineUpdateWithWhereUniqueWithoutTrackInput>
  updateMany?: InvoiceLineUpdateManyWithWhereNestedInput | Enumerable<InvoiceLineUpdateManyWithWhereNestedInput>
  deleteMany?: InvoiceLineScalarWhereInput | Enumerable<InvoiceLineScalarWhereInput>
  upsert?: InvoiceLineUpsertWithWhereUniqueWithoutTrackInput | Enumerable<InvoiceLineUpsertWithWhereUniqueWithoutTrackInput>
}

export type PlaylistTrackUpdateManyWithoutTrackInput = {
  create?: PlaylistTrackCreateWithoutTrackInput | Enumerable<PlaylistTrackCreateWithoutTrackInput>
  connect?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  set?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  disconnect?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  delete?: PlaylistTrackWhereUniqueInput | Enumerable<PlaylistTrackWhereUniqueInput>
  update?: PlaylistTrackUpdateWithWhereUniqueWithoutTrackInput | Enumerable<PlaylistTrackUpdateWithWhereUniqueWithoutTrackInput>
  updateMany?: PlaylistTrackUpdateManyWithWhereNestedInput | Enumerable<PlaylistTrackUpdateManyWithWhereNestedInput>
  deleteMany?: PlaylistTrackScalarWhereInput | Enumerable<PlaylistTrackScalarWhereInput>
  upsert?: PlaylistTrackUpsertWithWhereUniqueWithoutTrackInput | Enumerable<PlaylistTrackUpsertWithWhereUniqueWithoutTrackInput>
}

export type NestedIntFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntFilter
}

export type NestedStringFilter = {
  equals?: string
  in?: Enumerable<string>
  notIn?: Enumerable<string>
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: string | NestedStringFilter
}

export type NestedStringNullableFilter = {
  equals?: string | null
  in?: Enumerable<string> | null
  notIn?: Enumerable<string> | null
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: string | NestedStringNullableFilter | null
}

export type NestedIntNullableFilter = {
  equals?: number | null
  in?: Enumerable<number> | null
  notIn?: Enumerable<number> | null
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntNullableFilter | null
}

export type NestedDateTimeNullableFilter = {
  equals?: Date | string | null
  in?: Enumerable<Date> | Enumerable<string> | null
  notIn?: Enumerable<Date> | Enumerable<string> | null
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeNullableFilter | null
}

export type NestedDateTimeFilter = {
  equals?: Date | string
  in?: Enumerable<Date> | Enumerable<string>
  notIn?: Enumerable<Date> | Enumerable<string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeFilter
}

export type NestedFloatFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedFloatFilter
}

export type ArtistCreateWithoutAlbumInput = {
  Name?: string | null
}

export type TrackCreateWithoutAlbumInput = {
  Name: string
  Composer?: string | null
  Milliseconds: number
  Bytes?: number | null
  UnitPrice: number
  Genre?: GenreCreateOneWithoutTrackInput
  MediaType: MediaTypeCreateOneWithoutTrackInput
  InvoiceLine?: InvoiceLineCreateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackCreateManyWithoutTrackInput
}

export type ArtistUpdateWithoutAlbumDataInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type ArtistUpsertWithoutAlbumInput = {
  update: ArtistUpdateWithoutAlbumDataInput
  create: ArtistCreateWithoutAlbumInput
}

export type TrackUpdateWithWhereUniqueWithoutAlbumInput = {
  where: TrackWhereUniqueInput
  data: TrackUpdateWithoutAlbumDataInput
}

export type TrackUpdateManyWithWhereNestedInput = {
  where: TrackScalarWhereInput
  data: TrackUpdateManyDataInput
}

export type TrackScalarWhereInput = {
  AND?: TrackScalarWhereInput | Enumerable<TrackScalarWhereInput>
  OR?: TrackScalarWhereInput | Enumerable<TrackScalarWhereInput>
  NOT?: TrackScalarWhereInput | Enumerable<TrackScalarWhereInput>
  TrackId?: IntFilter | number
  Name?: StringFilter | string
  AlbumId?: IntNullableFilter | number | null
  MediaTypeId?: IntFilter | number
  GenreId?: IntNullableFilter | number | null
  Composer?: StringNullableFilter | string | null
  Milliseconds?: IntFilter | number
  Bytes?: IntNullableFilter | number | null
  UnitPrice?: FloatFilter | number
}

export type TrackUpsertWithWhereUniqueWithoutAlbumInput = {
  where: TrackWhereUniqueInput
  update: TrackUpdateWithoutAlbumDataInput
  create: TrackCreateWithoutAlbumInput
}

export type AlbumCreateWithoutArtistInput = {
  Title: string
  Track?: TrackCreateManyWithoutAlbumInput
}

export type AlbumUpdateWithWhereUniqueWithoutArtistInput = {
  where: AlbumWhereUniqueInput
  data: AlbumUpdateWithoutArtistDataInput
}

export type AlbumUpdateManyWithWhereNestedInput = {
  where: AlbumScalarWhereInput
  data: AlbumUpdateManyDataInput
}

export type AlbumScalarWhereInput = {
  AND?: AlbumScalarWhereInput | Enumerable<AlbumScalarWhereInput>
  OR?: AlbumScalarWhereInput | Enumerable<AlbumScalarWhereInput>
  NOT?: AlbumScalarWhereInput | Enumerable<AlbumScalarWhereInput>
  AlbumId?: IntFilter | number
  Title?: StringFilter | string
  ArtistId?: IntFilter | number
}

export type AlbumUpsertWithWhereUniqueWithoutArtistInput = {
  where: AlbumWhereUniqueInput
  update: AlbumUpdateWithoutArtistDataInput
  create: AlbumCreateWithoutArtistInput
}

export type EmployeeCreateWithoutCustomerInput = {
  LastName: string
  FirstName: string
  Title?: string | null
  BirthDate?: Date | string | null
  HireDate?: Date | string | null
  Address?: string | null
  City?: string | null
  State?: string | null
  Country?: string | null
  PostalCode?: string | null
  Phone?: string | null
  Fax?: string | null
  Email?: string | null
  Employee?: EmployeeCreateOneWithoutOther_EmployeeInput
  other_Employee?: EmployeeCreateManyWithoutEmployeeInput
}

export type InvoiceCreateWithoutCustomerInput = {
  InvoiceDate: Date | string
  BillingAddress?: string | null
  BillingCity?: string | null
  BillingState?: string | null
  BillingCountry?: string | null
  BillingPostalCode?: string | null
  Total: number
  InvoiceLine?: InvoiceLineCreateManyWithoutInvoiceInput
}

export type EmployeeUpdateWithoutCustomerDataInput = {
  LastName?: string | StringFieldUpdateOperationsInput
  FirstName?: string | StringFieldUpdateOperationsInput
  Title?: string | NullableStringFieldUpdateOperationsInput | null
  BirthDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  HireDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | NullableStringFieldUpdateOperationsInput | null
  Employee?: EmployeeUpdateOneWithoutOther_EmployeeInput
  other_Employee?: EmployeeUpdateManyWithoutEmployeeInput
}

export type EmployeeUpsertWithoutCustomerInput = {
  update: EmployeeUpdateWithoutCustomerDataInput
  create: EmployeeCreateWithoutCustomerInput
}

export type InvoiceUpdateWithWhereUniqueWithoutCustomerInput = {
  where: InvoiceWhereUniqueInput
  data: InvoiceUpdateWithoutCustomerDataInput
}

export type InvoiceUpdateManyWithWhereNestedInput = {
  where: InvoiceScalarWhereInput
  data: InvoiceUpdateManyDataInput
}

export type InvoiceScalarWhereInput = {
  AND?: InvoiceScalarWhereInput | Enumerable<InvoiceScalarWhereInput>
  OR?: InvoiceScalarWhereInput | Enumerable<InvoiceScalarWhereInput>
  NOT?: InvoiceScalarWhereInput | Enumerable<InvoiceScalarWhereInput>
  InvoiceId?: IntFilter | number
  CustomerId?: IntFilter | number
  InvoiceDate?: DateTimeFilter | Date | string
  BillingAddress?: StringNullableFilter | string | null
  BillingCity?: StringNullableFilter | string | null
  BillingState?: StringNullableFilter | string | null
  BillingCountry?: StringNullableFilter | string | null
  BillingPostalCode?: StringNullableFilter | string | null
  Total?: FloatFilter | number
}

export type InvoiceUpsertWithWhereUniqueWithoutCustomerInput = {
  where: InvoiceWhereUniqueInput
  update: InvoiceUpdateWithoutCustomerDataInput
  create: InvoiceCreateWithoutCustomerInput
}

export type EmployeeCreateWithoutOther_EmployeeInput = {
  LastName: string
  FirstName: string
  Title?: string | null
  BirthDate?: Date | string | null
  HireDate?: Date | string | null
  Address?: string | null
  City?: string | null
  State?: string | null
  Country?: string | null
  PostalCode?: string | null
  Phone?: string | null
  Fax?: string | null
  Email?: string | null
  Employee?: EmployeeCreateOneWithoutOther_EmployeeInput
  Customer?: CustomerCreateManyWithoutEmployeeInput
}

export type CustomerCreateWithoutEmployeeInput = {
  FirstName: string
  LastName: string
  Company?: string | null
  Address?: string | null
  City?: string | null
  State?: string | null
  Country?: string | null
  PostalCode?: string | null
  Phone?: string | null
  Fax?: string | null
  Email: string
  Invoice?: InvoiceCreateManyWithoutCustomerInput
}

export type EmployeeCreateWithoutEmployeeInput = {
  LastName: string
  FirstName: string
  Title?: string | null
  BirthDate?: Date | string | null
  HireDate?: Date | string | null
  Address?: string | null
  City?: string | null
  State?: string | null
  Country?: string | null
  PostalCode?: string | null
  Phone?: string | null
  Fax?: string | null
  Email?: string | null
  Customer?: CustomerCreateManyWithoutEmployeeInput
  other_Employee?: EmployeeCreateManyWithoutEmployeeInput
}

export type EmployeeUpdateWithoutOther_EmployeeDataInput = {
  LastName?: string | StringFieldUpdateOperationsInput
  FirstName?: string | StringFieldUpdateOperationsInput
  Title?: string | NullableStringFieldUpdateOperationsInput | null
  BirthDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  HireDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | NullableStringFieldUpdateOperationsInput | null
  Employee?: EmployeeUpdateOneWithoutOther_EmployeeInput
  Customer?: CustomerUpdateManyWithoutEmployeeInput
}

export type EmployeeUpsertWithoutOther_EmployeeInput = {
  update: EmployeeUpdateWithoutOther_EmployeeDataInput
  create: EmployeeCreateWithoutOther_EmployeeInput
}

export type CustomerUpdateWithWhereUniqueWithoutEmployeeInput = {
  where: CustomerWhereUniqueInput
  data: CustomerUpdateWithoutEmployeeDataInput
}

export type CustomerUpdateManyWithWhereNestedInput = {
  where: CustomerScalarWhereInput
  data: CustomerUpdateManyDataInput
}

export type CustomerScalarWhereInput = {
  AND?: CustomerScalarWhereInput | Enumerable<CustomerScalarWhereInput>
  OR?: CustomerScalarWhereInput | Enumerable<CustomerScalarWhereInput>
  NOT?: CustomerScalarWhereInput | Enumerable<CustomerScalarWhereInput>
  CustomerId?: IntFilter | number
  FirstName?: StringFilter | string
  LastName?: StringFilter | string
  Company?: StringNullableFilter | string | null
  Address?: StringNullableFilter | string | null
  City?: StringNullableFilter | string | null
  State?: StringNullableFilter | string | null
  Country?: StringNullableFilter | string | null
  PostalCode?: StringNullableFilter | string | null
  Phone?: StringNullableFilter | string | null
  Fax?: StringNullableFilter | string | null
  Email?: StringFilter | string
  SupportRepId?: IntNullableFilter | number | null
}

export type CustomerUpsertWithWhereUniqueWithoutEmployeeInput = {
  where: CustomerWhereUniqueInput
  update: CustomerUpdateWithoutEmployeeDataInput
  create: CustomerCreateWithoutEmployeeInput
}

export type EmployeeUpdateWithWhereUniqueWithoutEmployeeInput = {
  where: EmployeeWhereUniqueInput
  data: EmployeeUpdateWithoutEmployeeDataInput
}

export type EmployeeUpdateManyWithWhereNestedInput = {
  where: EmployeeScalarWhereInput
  data: EmployeeUpdateManyDataInput
}

export type EmployeeScalarWhereInput = {
  AND?: EmployeeScalarWhereInput | Enumerable<EmployeeScalarWhereInput>
  OR?: EmployeeScalarWhereInput | Enumerable<EmployeeScalarWhereInput>
  NOT?: EmployeeScalarWhereInput | Enumerable<EmployeeScalarWhereInput>
  EmployeeId?: IntFilter | number
  LastName?: StringFilter | string
  FirstName?: StringFilter | string
  Title?: StringNullableFilter | string | null
  ReportsTo?: IntNullableFilter | number | null
  BirthDate?: DateTimeNullableFilter | Date | string | null
  HireDate?: DateTimeNullableFilter | Date | string | null
  Address?: StringNullableFilter | string | null
  City?: StringNullableFilter | string | null
  State?: StringNullableFilter | string | null
  Country?: StringNullableFilter | string | null
  PostalCode?: StringNullableFilter | string | null
  Phone?: StringNullableFilter | string | null
  Fax?: StringNullableFilter | string | null
  Email?: StringNullableFilter | string | null
}

export type EmployeeUpsertWithWhereUniqueWithoutEmployeeInput = {
  where: EmployeeWhereUniqueInput
  update: EmployeeUpdateWithoutEmployeeDataInput
  create: EmployeeCreateWithoutEmployeeInput
}

export type TrackCreateWithoutGenreInput = {
  Name: string
  Composer?: string | null
  Milliseconds: number
  Bytes?: number | null
  UnitPrice: number
  Album?: AlbumCreateOneWithoutTrackInput
  MediaType: MediaTypeCreateOneWithoutTrackInput
  InvoiceLine?: InvoiceLineCreateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackCreateManyWithoutTrackInput
}

export type TrackUpdateWithWhereUniqueWithoutGenreInput = {
  where: TrackWhereUniqueInput
  data: TrackUpdateWithoutGenreDataInput
}

export type TrackUpsertWithWhereUniqueWithoutGenreInput = {
  where: TrackWhereUniqueInput
  update: TrackUpdateWithoutGenreDataInput
  create: TrackCreateWithoutGenreInput
}

export type CustomerCreateWithoutInvoiceInput = {
  FirstName: string
  LastName: string
  Company?: string | null
  Address?: string | null
  City?: string | null
  State?: string | null
  Country?: string | null
  PostalCode?: string | null
  Phone?: string | null
  Fax?: string | null
  Email: string
  Employee?: EmployeeCreateOneWithoutCustomerInput
}

export type InvoiceLineCreateWithoutInvoiceInput = {
  UnitPrice: number
  Quantity: number
  Track: TrackCreateOneWithoutInvoiceLineInput
}

export type CustomerUpdateWithoutInvoiceDataInput = {
  FirstName?: string | StringFieldUpdateOperationsInput
  LastName?: string | StringFieldUpdateOperationsInput
  Company?: string | NullableStringFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | StringFieldUpdateOperationsInput
  Employee?: EmployeeUpdateOneWithoutCustomerInput
}

export type CustomerUpsertWithoutInvoiceInput = {
  update: CustomerUpdateWithoutInvoiceDataInput
  create: CustomerCreateWithoutInvoiceInput
}

export type InvoiceLineUpdateWithWhereUniqueWithoutInvoiceInput = {
  where: InvoiceLineWhereUniqueInput
  data: InvoiceLineUpdateWithoutInvoiceDataInput
}

export type InvoiceLineUpdateManyWithWhereNestedInput = {
  where: InvoiceLineScalarWhereInput
  data: InvoiceLineUpdateManyDataInput
}

export type InvoiceLineScalarWhereInput = {
  AND?: InvoiceLineScalarWhereInput | Enumerable<InvoiceLineScalarWhereInput>
  OR?: InvoiceLineScalarWhereInput | Enumerable<InvoiceLineScalarWhereInput>
  NOT?: InvoiceLineScalarWhereInput | Enumerable<InvoiceLineScalarWhereInput>
  InvoiceLineId?: IntFilter | number
  InvoiceId?: IntFilter | number
  TrackId?: IntFilter | number
  UnitPrice?: FloatFilter | number
  Quantity?: IntFilter | number
}

export type InvoiceLineUpsertWithWhereUniqueWithoutInvoiceInput = {
  where: InvoiceLineWhereUniqueInput
  update: InvoiceLineUpdateWithoutInvoiceDataInput
  create: InvoiceLineCreateWithoutInvoiceInput
}

export type InvoiceCreateWithoutInvoiceLineInput = {
  InvoiceDate: Date | string
  BillingAddress?: string | null
  BillingCity?: string | null
  BillingState?: string | null
  BillingCountry?: string | null
  BillingPostalCode?: string | null
  Total: number
  Customer: CustomerCreateOneWithoutInvoiceInput
}

export type TrackCreateWithoutInvoiceLineInput = {
  Name: string
  Composer?: string | null
  Milliseconds: number
  Bytes?: number | null
  UnitPrice: number
  Album?: AlbumCreateOneWithoutTrackInput
  Genre?: GenreCreateOneWithoutTrackInput
  MediaType: MediaTypeCreateOneWithoutTrackInput
  PlaylistTrack?: PlaylistTrackCreateManyWithoutTrackInput
}

export type InvoiceUpdateWithoutInvoiceLineDataInput = {
  InvoiceDate?: Date | string | DateTimeFieldUpdateOperationsInput
  BillingAddress?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCity?: string | NullableStringFieldUpdateOperationsInput | null
  BillingState?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCountry?: string | NullableStringFieldUpdateOperationsInput | null
  BillingPostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Total?: number | FloatFieldUpdateOperationsInput
  Customer?: CustomerUpdateOneRequiredWithoutInvoiceInput
}

export type InvoiceUpsertWithoutInvoiceLineInput = {
  update: InvoiceUpdateWithoutInvoiceLineDataInput
  create: InvoiceCreateWithoutInvoiceLineInput
}

export type TrackUpdateWithoutInvoiceLineDataInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Album?: AlbumUpdateOneWithoutTrackInput
  Genre?: GenreUpdateOneWithoutTrackInput
  MediaType?: MediaTypeUpdateOneRequiredWithoutTrackInput
  PlaylistTrack?: PlaylistTrackUpdateManyWithoutTrackInput
}

export type TrackUpsertWithoutInvoiceLineInput = {
  update: TrackUpdateWithoutInvoiceLineDataInput
  create: TrackCreateWithoutInvoiceLineInput
}

export type TrackCreateWithoutMediaTypeInput = {
  Name: string
  Composer?: string | null
  Milliseconds: number
  Bytes?: number | null
  UnitPrice: number
  Album?: AlbumCreateOneWithoutTrackInput
  Genre?: GenreCreateOneWithoutTrackInput
  InvoiceLine?: InvoiceLineCreateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackCreateManyWithoutTrackInput
}

export type TrackUpdateWithWhereUniqueWithoutMediaTypeInput = {
  where: TrackWhereUniqueInput
  data: TrackUpdateWithoutMediaTypeDataInput
}

export type TrackUpsertWithWhereUniqueWithoutMediaTypeInput = {
  where: TrackWhereUniqueInput
  update: TrackUpdateWithoutMediaTypeDataInput
  create: TrackCreateWithoutMediaTypeInput
}

export type PlaylistTrackCreateWithoutPlaylistInput = {
  Track: TrackCreateOneWithoutPlaylistTrackInput
}

export type PlaylistTrackUpdateWithWhereUniqueWithoutPlaylistInput = {
  where: PlaylistTrackWhereUniqueInput
  data: PlaylistTrackUpdateWithoutPlaylistDataInput
}

export type PlaylistTrackUpdateManyWithWhereNestedInput = {
  where: PlaylistTrackScalarWhereInput
  data: PlaylistTrackUpdateManyDataInput
}

export type PlaylistTrackScalarWhereInput = {
  AND?: PlaylistTrackScalarWhereInput | Enumerable<PlaylistTrackScalarWhereInput>
  OR?: PlaylistTrackScalarWhereInput | Enumerable<PlaylistTrackScalarWhereInput>
  NOT?: PlaylistTrackScalarWhereInput | Enumerable<PlaylistTrackScalarWhereInput>
  PlaylistId?: IntFilter | number
  TrackId?: IntFilter | number
}

export type PlaylistTrackUpsertWithWhereUniqueWithoutPlaylistInput = {
  where: PlaylistTrackWhereUniqueInput
  update: PlaylistTrackUpdateWithoutPlaylistDataInput
  create: PlaylistTrackCreateWithoutPlaylistInput
}

export type PlaylistCreateWithoutPlaylistTrackInput = {
  Name?: string | null
}

export type TrackCreateWithoutPlaylistTrackInput = {
  Name: string
  Composer?: string | null
  Milliseconds: number
  Bytes?: number | null
  UnitPrice: number
  Album?: AlbumCreateOneWithoutTrackInput
  Genre?: GenreCreateOneWithoutTrackInput
  MediaType: MediaTypeCreateOneWithoutTrackInput
  InvoiceLine?: InvoiceLineCreateManyWithoutTrackInput
}

export type PlaylistUpdateWithoutPlaylistTrackDataInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type PlaylistUpsertWithoutPlaylistTrackInput = {
  update: PlaylistUpdateWithoutPlaylistTrackDataInput
  create: PlaylistCreateWithoutPlaylistTrackInput
}

export type TrackUpdateWithoutPlaylistTrackDataInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Album?: AlbumUpdateOneWithoutTrackInput
  Genre?: GenreUpdateOneWithoutTrackInput
  MediaType?: MediaTypeUpdateOneRequiredWithoutTrackInput
  InvoiceLine?: InvoiceLineUpdateManyWithoutTrackInput
}

export type TrackUpsertWithoutPlaylistTrackInput = {
  update: TrackUpdateWithoutPlaylistTrackDataInput
  create: TrackCreateWithoutPlaylistTrackInput
}

export type AlbumCreateWithoutTrackInput = {
  Title: string
  Artist: ArtistCreateOneWithoutAlbumInput
}

export type GenreCreateWithoutTrackInput = {
  Name?: string | null
}

export type MediaTypeCreateWithoutTrackInput = {
  Name?: string | null
}

export type InvoiceLineCreateWithoutTrackInput = {
  UnitPrice: number
  Quantity: number
  Invoice: InvoiceCreateOneWithoutInvoiceLineInput
}

export type PlaylistTrackCreateWithoutTrackInput = {
  Playlist: PlaylistCreateOneWithoutPlaylistTrackInput
}

export type AlbumUpdateWithoutTrackDataInput = {
  Title?: string | StringFieldUpdateOperationsInput
  Artist?: ArtistUpdateOneRequiredWithoutAlbumInput
}

export type AlbumUpsertWithoutTrackInput = {
  update: AlbumUpdateWithoutTrackDataInput
  create: AlbumCreateWithoutTrackInput
}

export type GenreUpdateWithoutTrackDataInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type GenreUpsertWithoutTrackInput = {
  update: GenreUpdateWithoutTrackDataInput
  create: GenreCreateWithoutTrackInput
}

export type MediaTypeUpdateWithoutTrackDataInput = {
  Name?: string | NullableStringFieldUpdateOperationsInput | null
}

export type MediaTypeUpsertWithoutTrackInput = {
  update: MediaTypeUpdateWithoutTrackDataInput
  create: MediaTypeCreateWithoutTrackInput
}

export type InvoiceLineUpdateWithWhereUniqueWithoutTrackInput = {
  where: InvoiceLineWhereUniqueInput
  data: InvoiceLineUpdateWithoutTrackDataInput
}

export type InvoiceLineUpsertWithWhereUniqueWithoutTrackInput = {
  where: InvoiceLineWhereUniqueInput
  update: InvoiceLineUpdateWithoutTrackDataInput
  create: InvoiceLineCreateWithoutTrackInput
}

export type PlaylistTrackUpdateWithWhereUniqueWithoutTrackInput = {
  where: PlaylistTrackWhereUniqueInput
  data: PlaylistTrackUpdateWithoutTrackDataInput
}

export type PlaylistTrackUpsertWithWhereUniqueWithoutTrackInput = {
  where: PlaylistTrackWhereUniqueInput
  update: PlaylistTrackUpdateWithoutTrackDataInput
  create: PlaylistTrackCreateWithoutTrackInput
}

export type TrackUpdateWithoutAlbumDataInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Genre?: GenreUpdateOneWithoutTrackInput
  MediaType?: MediaTypeUpdateOneRequiredWithoutTrackInput
  InvoiceLine?: InvoiceLineUpdateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackUpdateManyWithoutTrackInput
}

export type TrackUpdateManyDataInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
}

export type AlbumUpdateWithoutArtistDataInput = {
  Title?: string | StringFieldUpdateOperationsInput
  Track?: TrackUpdateManyWithoutAlbumInput
}

export type AlbumUpdateManyDataInput = {
  Title?: string | StringFieldUpdateOperationsInput
}

export type InvoiceUpdateWithoutCustomerDataInput = {
  InvoiceDate?: Date | string | DateTimeFieldUpdateOperationsInput
  BillingAddress?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCity?: string | NullableStringFieldUpdateOperationsInput | null
  BillingState?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCountry?: string | NullableStringFieldUpdateOperationsInput | null
  BillingPostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Total?: number | FloatFieldUpdateOperationsInput
  InvoiceLine?: InvoiceLineUpdateManyWithoutInvoiceInput
}

export type InvoiceUpdateManyDataInput = {
  InvoiceDate?: Date | string | DateTimeFieldUpdateOperationsInput
  BillingAddress?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCity?: string | NullableStringFieldUpdateOperationsInput | null
  BillingState?: string | NullableStringFieldUpdateOperationsInput | null
  BillingCountry?: string | NullableStringFieldUpdateOperationsInput | null
  BillingPostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Total?: number | FloatFieldUpdateOperationsInput
}

export type CustomerUpdateWithoutEmployeeDataInput = {
  FirstName?: string | StringFieldUpdateOperationsInput
  LastName?: string | StringFieldUpdateOperationsInput
  Company?: string | NullableStringFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | StringFieldUpdateOperationsInput
  Invoice?: InvoiceUpdateManyWithoutCustomerInput
}

export type CustomerUpdateManyDataInput = {
  FirstName?: string | StringFieldUpdateOperationsInput
  LastName?: string | StringFieldUpdateOperationsInput
  Company?: string | NullableStringFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | StringFieldUpdateOperationsInput
}

export type EmployeeUpdateWithoutEmployeeDataInput = {
  LastName?: string | StringFieldUpdateOperationsInput
  FirstName?: string | StringFieldUpdateOperationsInput
  Title?: string | NullableStringFieldUpdateOperationsInput | null
  BirthDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  HireDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | NullableStringFieldUpdateOperationsInput | null
  Customer?: CustomerUpdateManyWithoutEmployeeInput
  other_Employee?: EmployeeUpdateManyWithoutEmployeeInput
}

export type EmployeeUpdateManyDataInput = {
  LastName?: string | StringFieldUpdateOperationsInput
  FirstName?: string | StringFieldUpdateOperationsInput
  Title?: string | NullableStringFieldUpdateOperationsInput | null
  BirthDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  HireDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  Address?: string | NullableStringFieldUpdateOperationsInput | null
  City?: string | NullableStringFieldUpdateOperationsInput | null
  State?: string | NullableStringFieldUpdateOperationsInput | null
  Country?: string | NullableStringFieldUpdateOperationsInput | null
  PostalCode?: string | NullableStringFieldUpdateOperationsInput | null
  Phone?: string | NullableStringFieldUpdateOperationsInput | null
  Fax?: string | NullableStringFieldUpdateOperationsInput | null
  Email?: string | NullableStringFieldUpdateOperationsInput | null
}

export type TrackUpdateWithoutGenreDataInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Album?: AlbumUpdateOneWithoutTrackInput
  MediaType?: MediaTypeUpdateOneRequiredWithoutTrackInput
  InvoiceLine?: InvoiceLineUpdateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackUpdateManyWithoutTrackInput
}

export type InvoiceLineUpdateWithoutInvoiceDataInput = {
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Quantity?: number | IntFieldUpdateOperationsInput
  Track?: TrackUpdateOneRequiredWithoutInvoiceLineInput
}

export type InvoiceLineUpdateManyDataInput = {
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Quantity?: number | IntFieldUpdateOperationsInput
}

export type TrackUpdateWithoutMediaTypeDataInput = {
  Name?: string | StringFieldUpdateOperationsInput
  Composer?: string | NullableStringFieldUpdateOperationsInput | null
  Milliseconds?: number | IntFieldUpdateOperationsInput
  Bytes?: number | NullableIntFieldUpdateOperationsInput | null
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Album?: AlbumUpdateOneWithoutTrackInput
  Genre?: GenreUpdateOneWithoutTrackInput
  InvoiceLine?: InvoiceLineUpdateManyWithoutTrackInput
  PlaylistTrack?: PlaylistTrackUpdateManyWithoutTrackInput
}

export type PlaylistTrackUpdateWithoutPlaylistDataInput = {
  Track?: TrackUpdateOneRequiredWithoutPlaylistTrackInput
}

export type PlaylistTrackUpdateManyDataInput = {

}

export type InvoiceLineUpdateWithoutTrackDataInput = {
  UnitPrice?: number | FloatFieldUpdateOperationsInput
  Quantity?: number | IntFieldUpdateOperationsInput
  Invoice?: InvoiceUpdateOneRequiredWithoutInvoiceLineInput
}

export type PlaylistTrackUpdateWithoutTrackDataInput = {
  Playlist?: PlaylistUpdateOneRequiredWithoutPlaylistTrackInput
}

/**
 * Batch Payload for updateMany & deleteMany
 */

export type BatchPayload = {
  count: number
}

/**
 * DMMF
 */
export declare const dmmf: DMMF.Document;
export {};
