import { getDMMF } from "@prisma/sdk";
import fs from "fs-extra";
import { join } from "path";
import {
  CodeBlockWriter,
  MethodDeclarationStructure,
  Project,
  PropertyDeclarationStructure,
  Scope,
  StatementStructures,
  StructureKind,
  SyntaxKind,
} from "ts-morph";
 
const basedir = join(process.cwd(), "models", "gen");

const run = async () => {
  const schema = fs.readFileSync(
    join(process.cwd(), "prisma", "schema.prisma"),
    "utf-8"
  );
  const dmmf = await getDMMF({ datamodel: schema });

  if (fs.existsSync(basedir)) {
    await fs.remove(basedir);
  }
  await fs.mkdirp(basedir);

  const project = new Project({
    compilerOptions: { outDir: basedir, declaration: true },
  });
  dmmf.datamodel.models.map((e, idx) => {
    const className = camelize(e.name);
    const imports: { [key: string]: StatementStructures } = {};
    let primaryKey = "id";
    const properties: PropertyDeclarationStructure[] = e.fields.map((e) => {
      let fieldType = "any";
      const types = {
        String: {
          type: "string",
          default: `""`,
        },
        Int: {
          type: "number",
          default: `0`,
        },
      };
      let isOptional = !e.isRequired;
      let camelizedType = camelize(e.type);
      const ctype = (types as any)[e.type];
      if (ctype) {
        fieldType = ctype.type;
      }

      if (e.isId) {
        primaryKey = e.name;
      }

      if (e.relationName) {
        imports[e.type] = {
          kind: StructureKind.ImportDeclaration,
          moduleSpecifier: `./${e.type}`,
          namedImports: [camelizedType],
        };
        isOptional = false;
        fieldType = `HasManyClass<${camelizedType}<${className}>, ${className}>`;
      }

      return {
        kind: StructureKind.Property,
        name: e.name,
        hasQuestionToken: isOptional,
        type: fieldType,
        initializer: (writer: CodeBlockWriter) => {
          let value = "null";
          if (ctype) {
            value = ctype.default;
          }
          if (e.relationName) {
            value = `this._hasMany(${camelizedType})`;
          }
          writer.write(value);
        },
      };
    });

    imports["@prisma/generated"] = {
      kind: StructureKind.ImportDeclaration,
      moduleSpecifier: `@prisma/generated`,
      namedImports: [`FindMany${className}Args`],
    };
    properties.unshift({
      kind: StructureKind.Property,
      name: "_tableName",
      scope: Scope.Private,
      type: "string",
      hasQuestionToken: false,
      isStatic: true,
      initializer: (writer: CodeBlockWriter) => {
        writer.write(`"${e.name}"`);
      },
    });
    properties.unshift({
      kind: StructureKind.Property,
      name: "_modelName",
      scope: Scope.Private,
      type: "string",
      hasQuestionToken: false,
      isStatic: true,
      initializer: (writer: CodeBlockWriter) => {
        writer.write(`"${className}"`);
      },
    });
    properties.unshift({
      kind: StructureKind.Property,
      name: "_primaryKey",
      scope: Scope.Private,
      type: "string",
      hasQuestionToken: false,
      isStatic: true,
      initializer: (writer: CodeBlockWriter) => {
        writer.write(`"${primaryKey}"`);
      },
    });

    const methods: MethodDeclarationStructure[] = [
      {
        kind: StructureKind.Method,
        name: "findMany",
        isStatic: true,
        isAsync: true,
        parameters: [
          {
            kind: StructureKind.Parameter,
            name: "q",
            type: `FindMany${className}Args`,
          },
          {
            kind: StructureKind.Parameter,
            name: "featureName",
            type: `string`,
            initializer: `"public"`,
          },
        ],
        returnType: `${className}[]`,
        statements: `return await super.findMany(q, featureName);`,
      },
    ];

    const modelClass: StatementStructures = {
      kind: StructureKind.Class,
      isExported: true,
      name: `${className}<T extends Model = any>`,
      properties,
      methods,
      extends: "Model<T>",
    };
    const source = project.createSourceFile(`${e.name}.ts`, {
      statements: [
        {
          kind: StructureKind.ImportDeclaration,
          moduleSpecifier: "core",
          namedImports: ["Model", "HasManyClass"],
        },
        ...Object.values(imports),
        modelClass,
      ],
    });
  });
  await project.emit();
};

run();
function camelize(str: string) {
  return str
    .replace(/_/gi, " ")
    .replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
      return word.toUpperCase();
    })
    .replace(/\s+/g, "");
}
